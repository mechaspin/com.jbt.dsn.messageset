local packagePath = package.path
package.path = '../org.openjaus.system.cpp/?.lua;' .. packagePath

local oj = require("buildtools.openjaus")
local this = require("mechaspin")

oj.createSolution(this.Name, this.Projects)

return projects