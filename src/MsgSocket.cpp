/*
 * MsgSocket.cpp
 *
 *  Created on: Feb 2, 2018
 *      Author: danny
 */

#include "MsgSocket.h"
#include "system/Packet.h"
#include <algorithm>
#include <string>

using namespace openjaus;
using namespace mechaspin;

#define RECV_PACKETSIZE 1048576 // 1 MB

MsgSocket::MsgSocket(const std::string &interfaceName, uint16 port) :
	socket(),
	recvQueue(),
	sendQueue(),
	recvThread(),
	sendThread(),
	mutex(),
	callback(NULL)
{
	system::NetworkInterface interface = loadInterface(interfaceName);
	socket.open(interface.getAddress(), port);

	recvThread.setThreadFunction(THREAD_METHOD(MsgSocket, recvThreadMethod), this);
    sendThread.setThreadFunction(THREAD_METHOD(MsgSocket, sendThreadMethod), this);
}

MsgSocket::~MsgSocket()
{
	if (recvThread.isRunning())
	{
		this->stop();
	}
}

void MsgSocket::run()
{
	recvThread.create();
	sendThread.create();
}

void MsgSocket::stop()
{
	recvThread.join();
	sendThread.join();
}

bool MsgSocket::joinMulticastGroup(const std::string &groupAddress)
{

	return socket.joinGroup(system::InetAddress::getByName(groupAddress));
}

bool MsgSocket::sendMessage(model::Message *msg)
{
	// Send Queue now owns the msg object
	msg->setSource(this->socket.getIpAddress());
	msg->setSourcePort(this->socket.getPort());
	return sendQueue.push(msg);
}

model::Message *MsgSocket::getMessage()
{
	return recvQueue.pop();
}

bool MsgSocket::hasMessagesWaiting() const
{
	return !recvQueue.isEmpty();
}

bool MsgSocket::clearQueue()
{
	if (recvThread.isRunning())
	{
		mutex.lock();
		while (!recvQueue.isEmpty())
		{
			openjaus::model::Message *msg = recvQueue.pop();
			delete msg;
		}
		mutex.unlock();
		return true;
	}
	return false;
}

int MsgSocket::getQueueSize() const
{
	return recvQueue.getSize();
}


void *MsgSocket::recvThreadMethod()
{
	system::Time timeout;
	timeout.setMicroseconds(250000);
	socket.setTimeout(timeout);

	system::Packet* packet = new system::Packet(RECV_PACKETSIZE);
	while (recvThread.isRunning())
	{
		if (!socket.receive(*packet))
		{
			// Did not receive any packet data so try again
			continue;
		}

		if (packet->getPort() == socket.getPort() && socket.isLocalAddress(packet->getAddress()))
		{
			// Ignore packets sent from this interface
			//            LOG_OJ_DEBUG("Ignoring Packet From Self: " << packet->getAddress());
			continue;
		}

		// Convert Packet into Message
		model::Message *msg = new model::Message();
		msg->setSource(packet->getAddress());
		msg->setSourcePort(packet->getPort());
		msg->from(packet->getReader());

		// [DK] Manually change things up
		packet->getReader().reset();
		uint32 magicNumber;
		uint8 version;
		uint32 commandId;
		packet->getReader().unpack(magicNumber);
		packet->getReader().unpack(version);
		packet->getReader().unpack(commandId);
		msg->setId(commandId);
		packet->getReader().reset();

		mutex.lock();
		{
			// Push packet into Queue
			this->recvQueue.push(msg);
		}
		mutex.unlock();

		// Notify Callback Function
		if (callback)
		{
			this->callback->execute();
		}
	}

	if (packet != NULL)
	{
		delete packet;
	}

	return NULL;
}

void *MsgSocket::sendThreadMethod()
{
	while (sendThread.isRunning())
	{
		model::Message *msg = sendQueue.pop();
		if (msg == NULL)
		{
			sendQueue.timedWait(system::Condition::DEFAULT_WAIT_MSEC);
			continue;
		}

		sendMessageInternal(msg);
		delete msg;
	}
	return NULL;
}

void MsgSocket::sendMessageInternal(model::Message* msg)
{
	try
	{
		system::Packet packet(msg->length());
		packet.setAddress(msg->getDestination());
		packet.setPort(msg->getDestinationPort());


		msg->to(packet.getWriter());
		//std::cout << packet.toString() << std::endl;
		socket.send(packet);
	}
	catch (openjaus::system::Exception& exp)
	{
		std::cout << exp.toString() << std::endl;
		system::Logger::log(exp);
	}
}

void MsgSocket::addMessageCallback(MsgSocket::Callback* callback)
{
	this->callback = callback;
}

void MsgSocket::addMessageCallback(void(*callbackFunc)(void *))
{
	MsgSocket::Callback* callback = new MsgSocket::Callback(callbackFunc);
	this->addMessageCallback(callback);
}

void MsgSocket::addMessageCallback(void(*callbackFunc)(void *object, void *), void *object)
{
	MsgSocket::Callback* callback = new MsgSocket::Callback(callbackFunc, object);
	this->addMessageCallback(callback);
}

MsgSocket::Callback::Callback(void(*callback)(void *)) :
	object(NULL),
	funcCallback(callback),
	objectCallback(NULL)
{
	if (this->funcCallback == NULL)
	{
		THROW_EXCEPTION("Invalid parameter. Provided callback method cannot be NULL");
	}
}

MsgSocket::Callback::Callback(void(*callback)(void *object, void *), void *object) :
	object(object),
	funcCallback(NULL),
	objectCallback(callback)
{
	if (this->objectCallback == NULL)
	{
		THROW_EXCEPTION("Invalid parameter. Provided callback method cannot be NULL");
	}

	if (this->object == NULL)
	{
		THROW_EXCEPTION("Invalid parameter. Provided object cannot be NULL");
	}
}

MsgSocket::Callback::Callback(const MsgSocket::Callback& other) :
	object(other.object),
	funcCallback(other.funcCallback),
	objectCallback(other.objectCallback)
{
	// Intentionally left blank
}

MsgSocket::Callback& MsgSocket::Callback::operator=(const MsgSocket::Callback& other)
{
	return *this;
}

void MsgSocket::Callback::execute(void)
{
	if (funcCallback != NULL)
	{
		funcCallback(NULL);
	}
	else if (objectCallback != NULL)
	{
		objectCallback(object, NULL);
	}
	else
	{
		LOG_EXCEPTION("Tried to execute an Events::Callback that was not properly set!");
	}
}

system::NetworkInterface MsgSocket::loadInterface(const std::string &interfaceName)
{
	system::NetworkInterface output;

    // NMJ: If "Auto" is listed we will just use Auto.
    // Having Auto in addition to other interfaces doesn't make sense.
    if (interfaceName.compare("Auto") == 0)
    {
        return system::NetworkInterface::anyInterface();
    }

	bool interfaceFound = false;

	std::vector<system::NetworkInterface> availableInterfaces = system::Socket::lookupInterfaces();
	std::vector<system::NetworkInterface>::iterator jt;
	for (jt = availableInterfaces.begin(); jt != availableInterfaces.end(); jt++)
	{
		system::NetworkInterface& interface = *jt;
		if (interfaceName.compare(interface.getName()) == 0)
		{
			output = interface;
			interfaceFound = true;
			break;
		}
	}

	if (!interfaceFound)
	{
		THROW_EXCEPTION("Interface " << interfaceName << " is not valid.");
		RETURN_AFTER_EXCEPTION(output);
	}

    return output;
}




