/**
\file Message.h

\par Copyright
Copyright (c) 2010-2016, OpenJAUS, LLC
All rights reserved.

This file is part of the OpenJAUS Software Development Kit (SDK). This
software is distributed under one of two licenses, the OpenJAUS SDK
Commercial End User License Agreement or the OpenJAUS SDK Non-Commercial
End User License Agreement. The appropriate licensing details were included
in with your developer credentials and software download. See the LICENSE
file included with this software for full details.
 
THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LCC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
- [2015-08] - Version 4.3 Release
- [2014-12] - Version 4.2 Release
- [2014-01] - Version 4.1 Release
- [2011-06] - Version 4.0 Release

*/

#include "model/Message.h"
#include <sstream>
#include <iostream>
#include <iomanip>

namespace openjaus
{
namespace model
{

Message::Message() : 
	payload(NULL)
{
}

Message::Message(Message *message) :
	payload(NULL)
{
	if(!message)
	{
		THROW_EXCEPTION("Attempted to construct a message from a NULL pointer");
	}
}

Message::~Message()
{
	payload = NULL;
}

// Class Methods
uint64 Message::to(system::BufferWriter& dst) const
{
//	// Start of user code for method to:
//	if(this->length() > dst->remainingBytes())
//	{
//		THROW_EXCEPTION("Destination Buffer Too Small. Not sufficient room to pack Message Field");
//	}
//
//	system::Buffer *sendBuffer = dynamic_cast<system::Buffer*>(payload);
//	if(!sendBuffer)
//	{
//		THROW_EXCEPTION("Message class trying to pack NULL payload buffer");
//	}
//
//
//	int initialIndex = sendBuffer->containedBytes();
//	sendBuffer->reset();
//
//	int result = dst->pack(*sendBuffer);
//
//	// Restore to initial state (not necessarily reset)
//	sendBuffer->reset();
//	sendBuffer->increment(initialIndex);
//
//	return result;
//    // End of user code
    
    if (this->length() > dst.bytesRemaining())
    {
        THROW_EXCEPTION("Destination Buffer Too Small. Not sufficient room to pack Message");
    }
    
    system::Buffer* sendBuffer = dynamic_cast<system::Buffer*>(payload);
    if (sendBuffer == NULL)
    {
        THROW_EXCEPTION("Message class trying to pack NULL payload buffer");
    }
    
    uint64 result = 0;
    result += dst.pack(*sendBuffer);
    return result;
}


uint64 Message::from(system::BufferReader& src)
{
    // We peek at the id for state machine transition routing
    // The id must remain in the payload buffer
    uint16 tempId;
    src.peek(tempId);
    this->id = tempId;

    if (payload && dynamic_cast<Message*>(payload) != this)
    {
        delete payload;
    }
    payload = new system::Buffer(src.bytesRemaining());
    
    uint64 result = 0;
    result += src.unpack(*payload);
    return result;
}

uint64 Message::length() const
{
	// Return length of payload buffer
	return payload? payload->length() : 0;
}

system::Transportable* Message::getPayload() const
{
	// Start of user code for accessor getPayload:

	return payload;
	// End of user code
}

bool Message::setPayload(system::Transportable* payload)
{
	// Start of user code for accessor setPayload:
	if (this->payload != NULL)
	{
		delete this->payload;
	}

	if (payload == NULL)
	{
		this->payload = NULL;
		return true;
	}

	system::Buffer* newBuffer = new system::Buffer(payload->length());
	system::BufferWriter& writer = newBuffer->getWriter();
	writer.clear();
	payload->to(writer);

	this->payload = newBuffer;

	return true;
	// End of user code
}

std::string Message::toString() const
{	
	std::ostringstream oss;
	oss << std::showbase << std::uppercase << std::hex << std::setw(4) << static_cast<uint16>(this->id);
	return oss.str();
}

std::ostream& operator<<(std::ostream& output, const Message& object)
{
    output << object.toString();
    return output;
}

std::ostream& operator<<(std::ostream& output, const Message* object)
{
    output << object->toString();
    return output;
}

system::InetAddress Message::getSource() const
{
	return this->source;
}

void Message::setSource(system::InetAddress ipAddress)
{
	this->source = ipAddress;
}

system::InetAddress Message::getDestination() const
{
	return this->destination;
}

void Message::setDestination(system::InetAddress ipAddress)
{
	this->destination = ipAddress;
}

uint16 Message::getSourcePort() const
{
	return this->sourcePort;
}

void Message::setSourcePort(uint16 port)
{
	this->sourcePort = port;
}

uint16 Message::getDestinationPort() const
{
	return this->destinationPort;
}

void Message::setDestinationPort(uint16 port)
{
	this->destinationPort = port;
}




} // namespace model
} // namespace openjaus

