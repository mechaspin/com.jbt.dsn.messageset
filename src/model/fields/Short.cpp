/**
\file Short.h

\par Copyright
Copyright (c) 2010-2016, OpenJAUS, LLC
All rights reserved.

This file is part of the OpenJAUS Software Development Kit (SDK). This
software is distributed under one of two licenses, the OpenJAUS SDK
Commercial End User License Agreement or the OpenJAUS SDK Non-Commercial
End User License Agreement. The appropriate licensing details were included
in with your developer credentials and software download. See the LICENSE
file included with this software for full details.
 
THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LCC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
- [2015-08] - Version 4.3 Release
- [2014-12] - Version 4.2 Release
- [2014-01] - Version 4.1 Release
- [2011-06] - Version 4.0 Release

*/

#include "model/fields/Short.h"
#include <sstream>
// Start of user code for additional includes
// End of user code

namespace openjaus
{
namespace model
{
namespace fields
{

// Start of user code for default constructor:
Short::Short() :
		value(0)
{
}
// End of user code

// Start of user code for default destructor:
Short::~Short()
{
}
// End of user code
int16_t Short::getValue() const
{
	// Start of user code for accessor getValue:
	
	return value;
	// End of user code
}

bool Short::setValue(int16_t value)
{
	// Start of user code for accessor setValue:
	this->value = value;
	return true;
	// End of user code
}




// Class Methods

uint64 Short::to(system::BufferWriter& dst) const
{
	// Start of user code for method to:
	return dst.pack(value);
	// End of user code
}

uint64 Short::from(system::BufferReader& src)
{
	// Start of user code for method from:
	return src.unpack(value);
	// End of user code
}

uint64 Short::length() const
{
	// Start of user code for method length:
	return sizeof(int16_t);
	// End of user code
}


std::string Short::toString() const
{	
	// Start of user code for toString
	std::ostringstream oss;
	oss << "";
	return oss.str();
	// End of user code
}

std::ostream& operator<<(std::ostream& output, const Short& object)
{
    output << object.toString();
    return output;
}

std::ostream& operator<<(std::ostream& output, const Short* object)
{
    output << object->toString();
    return output;
}
// Start of user code for additional methods
std::string Short::toXml(unsigned char level) const
{
	std::ostringstream prefix;
	for(unsigned char i = 0; i < level; i++)
	{
		prefix << "\t";
	}

	std::ostringstream oss;
	oss << prefix.str() << "<Short name=\"" << this->name << "\" value=\"" << this->value << "\" />\n";
	return oss.str();
}
// End of user code

} // namespace fields
} // namespace model
} // namespace openjaus

