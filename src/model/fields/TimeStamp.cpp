/**
\file TimeStamp.h

\par Copyright
Copyright (c) 2010-2016, OpenJAUS, LLC
All rights reserved.

This file is part of the OpenJAUS Software Development Kit (SDK). This
software is distributed under one of two licenses, the OpenJAUS SDK
Commercial End User License Agreement or the OpenJAUS SDK Non-Commercial
End User License Agreement. The appropriate licensing details were included
in with your developer credentials and software download. See the LICENSE
file included with this software for full details.
 
THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LCC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
- [2015-08] - Version 4.3 Release
- [2014-12] - Version 4.2 Release
- [2014-01] - Version 4.1 Release
- [2011-06] - Version 4.0 Release

*/

#include "model/fields/TimeStamp.h"
#include <sstream>
// Start of user code for additional includes
#include <time.h>

#define MILLISECONDS_PER_DAY    86400000
#define MILLISECONDS_PER_HOUR    3600000
#define MILLISECONDS_PER_MIN       60000
#define MILLISECONDS_PER_SEC        1000

// End of user code

namespace openjaus
{
namespace model
{
namespace fields
{

// Start of user code for default constructor:
TimeStamp::TimeStamp() :
		day(DAY_MIN_VALUE),
		hour(HOUR_MIN_VALUE),
		minutes(MINUTE_MIN_VALUE),
		seconds(SECOND_MIN_VALUE),
		milliseconds(MILLISECOND_MIN_VALUE)
{
}
// End of user code

// Start of user code for default destructor:
TimeStamp::~TimeStamp()
{
}
// End of user code
uint8_t TimeStamp::getDay() const
{
	// Start of user code for accessor getDay:
	
	return day;
	// End of user code
}

bool TimeStamp::setDay(uint8_t day)
{
	// Start of user code for accessor setDay:
	if(day < DAY_MIN_VALUE || day > DAY_MAX_VALUE)
	{
		//< \todo: Throw error?
		return false;
	}

	this->day = day;
	return true;
	// End of user code
}

uint8_t TimeStamp::getHour() const
{
	// Start of user code for accessor getHour:
	
	return hour;
	// End of user code
}

bool TimeStamp::setHour(uint8_t hour)
{
	// Start of user code for accessor setHour:
	if(hour < HOUR_MIN_VALUE || hour > HOUR_MAX_VALUE)
	{
		//< \todo: Throw error?
		return false;
	}

	this->hour = hour;
	return true;
	// End of user code
}

uint8_t TimeStamp::getMinutes() const
{
	// Start of user code for accessor getMinutes:
	
	return minutes;
	// End of user code
}

bool TimeStamp::setMinutes(uint8_t minutes)
{
	// Start of user code for accessor setMinutes:
	if(minutes < MINUTE_MIN_VALUE || minutes > MINUTE_MAX_VALUE)
	{
		//< \todo: Throw error?
		return false;
	}

	this->minutes = minutes;
	return true;
	// End of user code
}

uint8_t TimeStamp::getSeconds() const
{
	// Start of user code for accessor getSeconds:
	
	return seconds;
	// End of user code
}

bool TimeStamp::setSeconds(uint8_t seconds)
{
	// Start of user code for accessor setSeconds:
	if(seconds < SECOND_MIN_VALUE || seconds > SECOND_MAX_VALUE)
	{
		//< \todo: Throw error?
		return false;
	}

	this->seconds = seconds;
	return true;
	// End of user code
}

uint16_t TimeStamp::getMilliseconds() const
{
	// Start of user code for accessor getMilliseconds:
	
	return milliseconds;
	// End of user code
}

bool TimeStamp::setMilliseconds(uint16_t milliseconds)
{
	// Start of user code for accessor setMilliseconds:
	if(milliseconds < MILLISECOND_MIN_VALUE || milliseconds > MILLISECOND_MAX_VALUE)
	{
		//< \todo: Throw error?
		return false;
	}

	this->milliseconds = milliseconds;
	return true;
	// End of user code
}



// Class Methods
std::string TimeStamp::toXml(uint8_t level) const
{
	// Start of user code for method toXml:
	std::ostringstream prefix;
	for(unsigned char i = 0; i < level; i++)
	{
		prefix << "\t";
	}

	std::ostringstream oss;
	oss << prefix.str() << "<TimeStamp name=\"" << this->name << "\">\n";
	oss << prefix.str() << "\t" << "<stringValue>"<< this->toString() << "</stringValue>\n";
	oss << prefix.str() << "\t" << "<intValue>" << toIntegerValue() << "</intValue>\n";
	oss << prefix.str() << "\t" << "<fields>\n";
	oss << prefix.str() << "\t\t" << "<BitFieldRange name=\"Milliseconds\" value=\"" << (uint16)getMilliseconds() << "\" />\n";
	oss << prefix.str() << "\t\t" << "<BitFieldRange name=\"Seconds\" value=\"" << (uint16)getSeconds() << "\" />\n";
	oss << prefix.str() << "\t\t" << "<BitFieldRange name=\"Minutes\" value=\"" << (uint16)getMinutes() << "\" />\n";
	oss << prefix.str() << "\t\t" << "<BitFieldRange name=\"Hour\" value=\"" << (uint16)getHour() << "\" />\n";
	oss << prefix.str() << "\t\t" << "<BitFieldRange name=\"Day\" value=\"" << (uint16)getDay() << "\" />\n";
	oss << prefix.str() << "\t" << "</fields>\n";
	oss << prefix.str() << "</TimeStamp>\n";
	return oss.str();
	// End of user code
}


void TimeStamp::setCurrentTime()
{
	// Start of user code for method setCurrentTime:
	// System time in seconds & microseconds
	setTime(system::Time::getTime());
	// End of user code
}


std::string TimeStamp::toString(std::string format) const
{
	// Start of user code for method toString:
	std::string result = "";
	char buffer[1024] = {0};

	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = gmtime(&rawtime);

	// Day
	// tm_mday = day of the month (1-31)
	// JAUS day = day of the month (1-31)
	timeinfo->tm_mday = getDay();

	// Hour
	// tm_hour = hours since midnight (0-23)
	// JAUS Hour = hours since midnight (0-23)
	timeinfo->tm_hour = getHour();

	// Minute
	// tm_min = minutes after the hour (0-59)
	// JAUS Minutes = minutes after the hour (0-59)
	timeinfo->tm_min = getMinutes();

	// Second
	// tm_sec = seconds after the minute (0-59)
	// JAUS Seconds = seconds after the minute (0-59)
	timeinfo->tm_sec = getSeconds();

	size_t retCount = strftime(buffer, 1024, format.c_str(), timeinfo);
	if(retCount < 1024)
	{
		result = buffer;
	}

	return result;
	// End of user code
}


uint32_t TimeStamp::toIntegerValue() const
{
	// Start of user code for method toIntegerValue:
	uint32_t intValue = 0;

	intValue |= ((this->milliseconds & MILLISECOND_BIT_MASK) << MILLISECOND_START_BIT);
	intValue |= ((this->seconds & SECOND_BIT_MASK) << SECOND_START_BIT);
	intValue |= ((this->minutes & MINUTE_BIT_MASK) << MINUTE_START_BIT);
	intValue |= ((this->hour & HOUR_BIT_MASK) << HOUR_START_BIT);
	intValue |= ((this->day & DAY_BIT_MASK) << DAY_START_BIT);

	return intValue;
	// End of user code
}


void TimeStamp::fromIntegerValue(uint32_t intValue)
{
	// Start of user code for method fromIntegerValue:
	this->milliseconds = (intValue >> (MILLISECOND_START_BIT)) & MILLISECOND_BIT_MASK;
	this->seconds = (intValue >> (SECOND_START_BIT)) & SECOND_BIT_MASK;
	this->minutes = (intValue >> (MINUTE_START_BIT)) & MINUTE_BIT_MASK;
	this->hour = (intValue >> (HOUR_START_BIT)) & HOUR_BIT_MASK;
	this->day = (intValue >> (DAY_START_BIT)) & DAY_BIT_MASK;
	// End of user code
}


void TimeStamp::copy(TimeStamp &source)
{
	// Start of user code for method copy:
	this->setName(source.getName());

	setMilliseconds(source.getMilliseconds());
	setSeconds(source.getSeconds());
	setMinutes(source.getMinutes());
	setHour(source.getHour());
	setDay(source.getDay());
	// End of user code
}


TimeStamp TimeStamp::addHours(int32_t hours) const
{
	// Start of user code for method addHours:
	TimeStamp result;

	// Get a struct tm representing this timestamp
	struct tm base = this->convertToTm();

	// Add hours and normalize
	base.tm_hour += hours;
	mktime( &base );

	// Set the result Timestamp data (milliseconds comes from current (this) TimeStamp)
	result.setTime( base.tm_mday, base.tm_hour, base.tm_min, base.tm_sec, this->milliseconds );

	return result;
	// End of user code
}


TimeStamp TimeStamp::addDays(int32_t days) const
{
	// Start of user code for method addDays:
	TimeStamp result;

	struct tm base = this->convertToTm();

	// Add days and normalize
	base.tm_mday += days;
	mktime( &base );

	// Set the result TimeStamp data (milliseconds comes from current (this) TimeStamp)
	result.setTime( base.tm_mday, base.tm_hour, base.tm_min, base.tm_sec, this->milliseconds );

	return result;
	// End of user code
}


TimeStamp TimeStamp::addMinutes(int32_t minutes) const
{
	// Start of user code for method addMinutes:
	TimeStamp result;

	struct tm base = this->convertToTm();

	// Add minutes and normalize
	base.tm_min += minutes;
	mktime( &base );

	// Set the result TimeStamp data (milliseconds comes from current (this) Timestamp)
	result.setTime( base.tm_mday, base.tm_hour, base.tm_min, base.tm_sec, this->milliseconds );

	return result;
	// End of user code
}


TimeStamp TimeStamp::addSeconds(int32_t seconds) const
{
	// Start of user code for method addSeconds:
	TimeStamp result;

	struct tm base = this->convertToTm();

	// Add seconds and normalize
	base.tm_sec += seconds;
	mktime( &base );

	// Set the result TimeStamp data (milliseconds comes from current (this) Timestamp)
	result.setTime( base.tm_mday, base.tm_hour, base.tm_min, base.tm_sec, this->milliseconds );

	return result;
	// End of user code
}


TimeStamp TimeStamp::addMilliseconds(int32_t milliseconds) const
{
	// Start of user code for method addMilliseconds:
	TimeStamp result;

	struct tm base = this->convertToTm();

	// Attempting to get around negative number weirdness w.r.t integer division/modulus
	int32_t secondsToAdd;
	if ( milliseconds < 0 )
	{
		secondsToAdd = -1 * ((-1 * milliseconds) / 1000);
	}
	else
	{
		secondsToAdd  = milliseconds / 1000;
	}
	int32_t mSecondsToAdd = milliseconds - (secondsToAdd * 1000) + this->milliseconds;

	// Handled here to head off overflow problems (e.g. passing in MAX_INT when current msec = 999)
	if ( mSecondsToAdd > 1000 )
	{
		secondsToAdd  += 1;
		mSecondsToAdd -= 1000;
	}

	// Add seconds and normalize
	base.tm_sec += secondsToAdd;
	mktime( &base );

	// Set the result TimeStamp data (milliseconds comes earlier calculation)
	result.setTime( base.tm_mday, base.tm_hour, base.tm_min, base.tm_sec, mSecondsToAdd );

	return result;
	// End of user code
}


bool TimeStamp::setTime(int8_t days, int8_t hours, int8_t minutes, int8_t seconds, int16_t milliseconds)
{
	// Start of user code for method setTime:
	bool result = false;

	result = ( (this->setDay(days)                 ) &&
			   (this->setHour(hours)               ) &&
			   (this->setMinutes(minutes)          ) &&
			   (this->setSeconds(seconds)          ) &&
			   (this->setMilliseconds(milliseconds)) );

	return result;
	// End of user code
}



uint64 TimeStamp::to(system::BufferWriter& dst) const
{
	// Start of user code for method to:
	uint32 intValue = toIntegerValue();
	return dst.pack(intValue);
	// End of user code
}

uint64 TimeStamp::from(system::BufferReader& src)
{
	// Start of user code for method from:
	int byteSize = 0;
	uint32 intValue = 0;
	byteSize = src.unpack(intValue);

	this->fromIntegerValue(intValue);

	return byteSize;
	// End of user code
}

uint64 TimeStamp::length() const
{
	// Start of user code for method length:
	return sizeof(uint32);
	// End of user code
}


std::string TimeStamp::toString() const
{	
	// Start of user code for toString
	return this->toString("%X");
	// End of user code
}

std::ostream& operator<<(std::ostream& output, const TimeStamp& object)
{
    output << object.toString();
    return output;
}

std::ostream& operator<<(std::ostream& output, const TimeStamp* object)
{
    output << object->toString();
    return output;
}
// Start of user code for additional methods

struct tm TimeStamp::convertToTm( ) const
{
	struct tm result;

	// Fill in struct tm
	result.tm_year = 100; // Avoids potential problems with pre-epoch years
	result.tm_mon = 0;
	result.tm_mday = this->day;
	result.tm_hour = this->hour;
	result.tm_min = this->minutes;
	result.tm_sec = this->seconds;

	return result;
}

TimeStamp TimeStamp::till( TimeStamp &futureTime ) const
{
	TimeStamp result;

	// Convert to milliseconds for easy comparison
	int now = ( (this->day     * MILLISECONDS_PER_DAY  ) +
				(this->hour    * MILLISECONDS_PER_HOUR ) +
				(this->minutes * MILLISECONDS_PER_MIN  ) +
				(this->seconds * MILLISECONDS_PER_SEC  ) +
				(this->milliseconds      ) );
	int future = ( (futureTime.day     * MILLISECONDS_PER_DAY  ) +
				   (futureTime.hour    * MILLISECONDS_PER_HOUR ) +
		    	   (futureTime.minutes * MILLISECONDS_PER_MIN  ) +
		    	   (futureTime.seconds * MILLISECONDS_PER_SEC  ) +
		    	   (futureTime.milliseconds      ) );

	if( future > now )
	{
		// Convert back to TimeStamp
		int resultMilliseconds = future - now;
		result.day = resultMilliseconds / MILLISECONDS_PER_DAY;
		resultMilliseconds -= (result.day * MILLISECONDS_PER_DAY);
		result.hour = resultMilliseconds / MILLISECONDS_PER_HOUR;
		resultMilliseconds -= (result.hour * MILLISECONDS_PER_HOUR);
		result.minutes = resultMilliseconds / MILLISECONDS_PER_MIN;
		resultMilliseconds -= (result.minutes * MILLISECONDS_PER_MIN);
		result.seconds = resultMilliseconds / MILLISECONDS_PER_SEC;
		resultMilliseconds -= (result.seconds * MILLISECONDS_PER_SEC);
		result.milliseconds = resultMilliseconds;
	}

	return result;
}

void TimeStamp::setTime(const openjaus::system::Time& time)
{
    // Convert to struct tm in UTC time
    time_t epochTime = time.getSeconds();
    struct tm *utcTm;
    utcTm = gmtime(&epochTime);

    // Day
    // tm_mday = day of the month (1-31)
    // JAUS day = day of the month (1-31)
    this->setDay(utcTm->tm_mday);

    // Hour
    // tm_hour = hours since midnight (0-23)
    // JAUS Hour = hours since midnight (0-23)
    this->setHour(utcTm->tm_hour);

    // Minute
    // tm_min = minutes after the hour (0-59)
    // JAUS Minutes = minutes after the hour (0-59)
    this->setMinutes(utcTm->tm_min);

    // Seconds
    // tm_sec = seconds after the minute (0-59)
    // JAUS Seconds = seconds after the minute (0-59)
    this->setSeconds(utcTm->tm_sec);

    // Milliseconds
    // JAUS Milliseconds = seconds after the minute (0-999)
    this->setMilliseconds(time.getMicroseconds() / 1000);
}

// End of user code

} // namespace fields
} // namespace model
} // namespace openjaus

