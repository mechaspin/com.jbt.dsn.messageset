/**
\file DateStamp.h

\par Copyright
Copyright (c) 2010-2016, OpenJAUS, LLC
All rights reserved.

This file is part of the OpenJAUS Software Development Kit (SDK). This
software is distributed under one of two licenses, the OpenJAUS SDK
Commercial End User License Agreement or the OpenJAUS SDK Non-Commercial
End User License Agreement. The appropriate licensing details were included
in with your developer credentials and software download. See the LICENSE
file included with this software for full details.
 
THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LCC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
- [2015-08] - Version 4.3 Release
- [2014-12] - Version 4.2 Release
- [2014-01] - Version 4.1 Release
- [2011-06] - Version 4.0 Release

*/

#include "model/fields/DateStamp.h"
#include <sstream>
// Start of user code for additional includes
#include <time.h>
// End of user code

namespace openjaus
{
namespace model
{
namespace fields
{

// Start of user code for default constructor:
DateStamp::DateStamp() :
		day(DAY_MIN_VALUE),
		month(MONTH_MIN_VALUE),
		year(YEAR_MIN_VALUE)
{
}
// End of user code

// Start of user code for default destructor:
DateStamp::~DateStamp()
{
}
// End of user code
uint8_t DateStamp::getDay() const
{
	// Start of user code for accessor getDay:
	
	return day;
	// End of user code
}

bool DateStamp::setDay(uint8_t day)
{
	// Start of user code for accessor setDay:
	if(day >= DAY_MIN_VALUE && day <= DAY_MAX_VALUE)
	{
		this->day = day;
		return true;
	}
	return false;
	// End of user code
}

uint8_t DateStamp::getMonth() const
{
	// Start of user code for accessor getMonth:
	
	return month;
	// End of user code
}

bool DateStamp::setMonth(uint8_t month)
{
	// Start of user code for accessor setMonth:
	if(month >= MONTH_MIN_VALUE && month <= MONTH_MAX_VALUE)
	{
		this->month = month;
		return true;
	}
	return false;
	// End of user code
}

uint16_t DateStamp::getYear() const
{
	// Start of user code for accessor getYear:
	
	return year;
	// End of user code
}

bool DateStamp::setYear(uint16_t year)
{
	// Start of user code for accessor setYear:
	// Note: There is an offset in this data of YEAR_MIN_VALUE, the min and max values are expressed at 2000 / 2167 respectively
	// but only the number of years since 2000 are actually transmitted across the wire
	if(year >= (YEAR_MIN_VALUE - YEAR_MIN_VALUE) && year <= (YEAR_MAX_VALUE - YEAR_MIN_VALUE))
	{
		this->year = year;
		return true;
	}
	return false;
	// End of user code
}



// Class Methods
std::string DateStamp::toXml(uint8_t level) const
{
	// Start of user code for method toXml:
	std::ostringstream prefix;
	for(unsigned char i = 0; i < level; i++)
	{
		prefix << "\t";
	}

	std::ostringstream oss;
	oss << prefix.str() << "<DateStamp BitField name=\"" << this->name << "\">\n";
	oss << prefix.str() << "\t" << "<stringValue>" << toString("%x") << "</stringValue>\n";
	oss << prefix.str() << "\t" << "<intValue>" << toIntegerValue() << "</intValue>\n";
	oss << prefix.str() << "\t" << "<fields>\n";
	oss << prefix.str() << "\t" << "<BitFieldRange name=\"Year\" value=\"" << (uint16)getYear() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldRange name=\"Month\" value=\"" << (uint16)getMonth() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldRange name=\"Day\" value=\"" << (uint16)getDay() << "\" />\n";
	oss << prefix.str() << "\t" << "</fields>\n";
	oss << prefix.str() << "</BitField>\n";
	return oss.str();

	// End of user code
}


void DateStamp::setCurrentDate()
{
	// Start of user code for method setCurrentDate:
	// System time in seconds & microseconds
	setDate(system::Time::getTime());
	// End of user code
}


std::string DateStamp::toString(std::string format) const
{
	// Start of user code for method toString:
	std::string result = "";
	char buffer[1024] = {0};

	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = gmtime(&rawtime);

	// Day
	// tm_mday = day of the month (1-31)
	// JAUS day = day of the month (1-31)
	timeinfo->tm_mday = getDay();

	// Month
	// tm_mon = month since Jan (0-11)
	// JAUS Month = month of the year (1-12)
	timeinfo->tm_mon = getMonth()-1;

	// Year
	// tm_year = years since 1900
	// JAUS Year = year since 2000
	timeinfo->tm_year = getYear() + 100;

	size_t retCount = strftime(buffer, 1024, format.c_str(), timeinfo);
	if(retCount < 1024)
	{
		result = buffer;
	}

	return result;
	// End of user code
}


uint16_t DateStamp::toIntegerValue() const
{
	// Start of user code for method toIntegerValue:
	uint16_t intValue = 0;

	intValue |= ((this->day & DAY_BIT_MASK) << DAY_START_BIT);
	intValue |= ((this->month & MONTH_BIT_MASK) << MONTH_START_BIT);
	intValue |= ((this->year & YEAR_BIT_MASK) << YEAR_START_BIT);

	return intValue;
	// End of user code
}


void DateStamp::fromIntegerValue(uint16_t intValue)
{
	// Start of user code for method fromIntegerValue:
	this->day = (intValue >> (DAY_START_BIT)) & DAY_BIT_MASK;
	this->month = (intValue >> (MONTH_START_BIT)) & MONTH_BIT_MASK;
	this->year = (intValue >> (YEAR_START_BIT)) & YEAR_BIT_MASK;
	// End of user code
}


void DateStamp::copy(DateStamp &source)
{
	// Start of user code for method copy:
	this->setName(source.getName());

	setDay(source.getDay());
	setMonth(source.getMonth());
	setYear(source.getYear());
	// End of user code
}


bool DateStamp::setDate(int16_t year, int8_t month, int8_t day)
{
	// Start of user code for method setDate:
	bool result = false;

	result = ( (this->setYear(year)  ) &&
			   (this->setMonth(month)) &&
			   (this->setDay(day)    ) );

	return result;
	// End of user code
}


DateStamp DateStamp::addYears(int32_t years) const
{
	// Start of user code for method addYears:
	DateStamp result;

	struct tm base = this->convertToTm();

	// Add years and normalize
	base.tm_year += years;
	mktime( &base );

	// Set the result DateStamp data (some conversion required)
	result.setDate( base.tm_year + 100, base.tm_mon + 1, base.tm_mday );

	return result;
	// End of user code
}


DateStamp DateStamp::addMonths(int32_t months) const
{
	// Start of user code for method addMonths:
	DateStamp result;

	struct tm base = this->convertToTm();

	// Add months and normalize
	base.tm_mon += months;
	mktime( &base );

	// Set the result DateStamp data (some conversion required)
	result.setDate( base.tm_year, base.tm_mon + 1, base.tm_mday );

	return result;
	// End of user code
}


DateStamp DateStamp::addDays(int32_t days) const
{
	// Start of user code for method addDays:
	DateStamp result;

	struct tm base = this->convertToTm();

	// Add days and normalize
	base.tm_mday += days;
	mktime( &base );

	// Set the result DateStamp data (some conversion required)
	result.setDate( base.tm_year, base.tm_mon + 1, base.tm_mday );

	return result;
	// End of user code
}



uint64 DateStamp::to(system::BufferWriter& dst) const
{
	// Start of user code for method to:
	uint16 intValue = toIntegerValue();
	return dst.pack(intValue);
	// End of user code
}

uint64 DateStamp::from(system::BufferReader&  src)
{
	// Start of user code for method from:
    uint64 byteSize = 0;
	
	uint16 intValue = 0;	
	byteSize = src.unpack(intValue);
	this->fromIntegerValue(intValue);

	return byteSize;
	// End of user code
}

uint64 DateStamp::length() const
{
	// Start of user code for method length:
	return sizeof(uint16_t);
	// End of user code
}


std::string DateStamp::toString() const
{	
	// Start of user code for toString
	return this->toString("%x");
	// End of user code
}

std::ostream& operator<<(std::ostream& output, const DateStamp& object)
{
    output << object.toString();
    return output;
}

std::ostream& operator<<(std::ostream& output, const DateStamp* object)
{
    output << object->toString();
    return output;
}
// Start of user code for additional methods

struct tm DateStamp::convertToTm( ) const
{
	struct tm result;

	// Fill in struct tm
	result.tm_year = this->year - 100;
	result.tm_mon = this->month - 1;
	result.tm_mday = this->day;
	result.tm_hour = 0;
	result.tm_min = 0;
	result.tm_sec = 0;

	return result;
}

void DateStamp::setDate(const openjaus::system::Time& time)
{
    // Convert to struct tm in UTC time
    time_t epochTime = time.getSeconds();
    struct tm *utcTm;
    utcTm = gmtime(&epochTime);

    // Date
    // tm_mday = day of the month (1-31)
    // JAUS day = day of the month (1-31)
    this->setDay(utcTm->tm_mday);

    // Month
    // tm_mon = month since Jan (0-11)
    // JAUS Month = month of the year (1-12)
    this->setMonth(utcTm->tm_mon+1);

    // Year
    // tm_year = years since 1900
    // JAUS Year = year since 2000
    this->setYear(utcTm->tm_year - 100);
}

// End of user code

} // namespace fields
} // namespace model
} // namespace openjaus

