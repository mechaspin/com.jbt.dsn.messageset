
/**
\file ErrorCorrections.cpp

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/

#include "jbt/Triggers/ErrorCorrections.h"
// Start of user code for additional includes
// End of user code for additional includes

namespace mechaspin
{


ErrorCorrections::ErrorCorrections() : 
	model::Message(),
	magicNumber(),
	versionNumber(),
	commandId(),
	dataLength(),
	lateralOffsetError_m(),
	depthOffsetError_m(),
	headingError_rad(),
	correctionCode(),
	secondaryData1(),
	secondaryData2(),
	status(),
	globalErrorX_m(),
	globalErrorY_m(),
	globalErrorHeading_rad()
{
	this->id = ErrorCorrections::ID; // Initialize id member


	magicNumber.setName("MagicNumber");
	magicNumber.setValue(0x204E5344);

	versionNumber.setName("VersionNumber");
	versionNumber.setValue(1);

	commandId.setName("CommandId");
	commandId.setValue(0);

	dataLength.setName("DataLength");
	dataLength.setValue(0);

	lateralOffsetError_m.setName("LateralOffsetError");
	lateralOffsetError_m.setValue(0);

	depthOffsetError_m.setName("DepthOffsetError");
	depthOffsetError_m.setValue(0);

	headingError_rad.setName("HeadingError");
	headingError_rad.setValue(0);

	correctionCode.setName("CorrectionCode");
	correctionCode.setValue(0);

	secondaryData1.setName("SecondaryData1");
	secondaryData1.setValue(0);

	secondaryData2.setName("SecondaryData2");
	secondaryData2.setValue(0);

	status.setName("Status");
	// Nothing

	globalErrorX_m.setName("GlobalErrorX");
	globalErrorX_m.setValue(0);

	globalErrorY_m.setName("GlobalErrorY");
	globalErrorY_m.setValue(0);

	globalErrorHeading_rad.setName("GlobalErrorHeading");
	globalErrorHeading_rad.setValue(0);

}

ErrorCorrections::ErrorCorrections(model::Message *message) :
	model::Message(message),
	magicNumber(),
	versionNumber(),
	commandId(),
	dataLength(),
	lateralOffsetError_m(),
	depthOffsetError_m(),
	headingError_rad(),
	correctionCode(),
	secondaryData1(),
	secondaryData2(),
	status(),
	globalErrorX_m(),
	globalErrorY_m(),
	globalErrorHeading_rad()
{
	this->id = ErrorCorrections::ID; // Initialize id member


	magicNumber.setName("MagicNumber");
	magicNumber.setValue(0x204E5344);

	versionNumber.setName("VersionNumber");
	versionNumber.setValue(1);

	commandId.setName("CommandId");
	commandId.setValue(0);

	dataLength.setName("DataLength");
	dataLength.setValue(0);

	lateralOffsetError_m.setName("LateralOffsetError");
	lateralOffsetError_m.setValue(0);

	depthOffsetError_m.setName("DepthOffsetError");
	depthOffsetError_m.setValue(0);

	headingError_rad.setName("HeadingError");
	headingError_rad.setValue(0);

	correctionCode.setName("CorrectionCode");
	correctionCode.setValue(0);

	secondaryData1.setName("SecondaryData1");
	secondaryData1.setValue(0);

	secondaryData2.setName("SecondaryData2");
	secondaryData2.setValue(0);

	status.setName("Status");
	// Nothing

	globalErrorX_m.setName("GlobalErrorX");
	globalErrorX_m.setValue(0);

	globalErrorY_m.setName("GlobalErrorY");
	globalErrorY_m.setValue(0);

	globalErrorHeading_rad.setName("GlobalErrorHeading");
	globalErrorHeading_rad.setValue(0);


	system::Buffer* payloadBuffer = dynamic_cast<system::Buffer*>(message->getPayload());
	if (payloadBuffer != NULL)
	{
	    system::BufferReader& reader = payloadBuffer->getReader();
	    reader.reset(); 
		this->from(reader);
		// payloadBuffer->reset();
	}
}

ErrorCorrections::~ErrorCorrections()
{

}


uint32_t ErrorCorrections::getMagicNumber(void)
{
	return this->magicNumber.getValue();
}

bool ErrorCorrections::setMagicNumber(uint32_t value)
{
	return this->magicNumber.setValue(value);
}

int8_t ErrorCorrections::getVersionNumber(void)
{
	return this->versionNumber.getValue();
}

bool ErrorCorrections::setVersionNumber(int8_t value)
{
	return this->versionNumber.setValue(value);
}

uint32_t ErrorCorrections::getCommandId(void)
{
	return this->commandId.getValue();
}

bool ErrorCorrections::setCommandId(uint32_t value)
{
	return this->commandId.setValue(value);
}

uint32_t ErrorCorrections::getDataLength(void)
{
	return this->dataLength.getValue();
}

bool ErrorCorrections::setDataLength(uint32_t value)
{
	return this->dataLength.setValue(value);
}

float ErrorCorrections::getLateralOffsetError_m(void)
{
	return this->lateralOffsetError_m.getValue();
}

bool ErrorCorrections::setLateralOffsetError_m(float value)
{
	return this->lateralOffsetError_m.setValue(value);
}

float ErrorCorrections::getDepthOffsetError_m(void)
{
	return this->depthOffsetError_m.getValue();
}

bool ErrorCorrections::setDepthOffsetError_m(float value)
{
	return this->depthOffsetError_m.setValue(value);
}

float ErrorCorrections::getHeadingError_rad(void)
{
	return this->headingError_rad.getValue();
}

bool ErrorCorrections::setHeadingError_rad(float value)
{
	return this->headingError_rad.setValue(value);
}

int8_t ErrorCorrections::getCorrectionCode(void)
{
	return this->correctionCode.getValue();
}

bool ErrorCorrections::setCorrectionCode(int8_t value)
{
	return this->correctionCode.setValue(value);
}

int8_t ErrorCorrections::getSecondaryData1(void)
{
	return this->secondaryData1.getValue();
}

bool ErrorCorrections::setSecondaryData1(int8_t value)
{
	return this->secondaryData1.setValue(value);
}

int8_t ErrorCorrections::getSecondaryData2(void)
{
	return this->secondaryData2.getValue();
}

bool ErrorCorrections::setSecondaryData2(int8_t value)
{
	return this->secondaryData2.setValue(value);
}

StatusBitField& ErrorCorrections::getStatus(void)
{
	return this->status;
}

float ErrorCorrections::getGlobalErrorX_m(void)
{
	return this->globalErrorX_m.getValue();
}

bool ErrorCorrections::setGlobalErrorX_m(float value)
{
	return this->globalErrorX_m.setValue(value);
}

float ErrorCorrections::getGlobalErrorY_m(void)
{
	return this->globalErrorY_m.getValue();
}

bool ErrorCorrections::setGlobalErrorY_m(float value)
{
	return this->globalErrorY_m.setValue(value);
}

float ErrorCorrections::getGlobalErrorHeading_rad(void)
{
	return this->globalErrorHeading_rad.getValue();
}

bool ErrorCorrections::setGlobalErrorHeading_rad(float value)
{
	return this->globalErrorHeading_rad.setValue(value);
}

uint64 ErrorCorrections::to(system::BufferWriter& dst) const
{
	uint64 byteSize = 0;
	byteSize += dst.pack(magicNumber);
	byteSize += dst.pack(versionNumber);
	byteSize += dst.pack(this->id);
	byteSize += dst.pack(static_cast<uint32>(this->length()-13));
	byteSize += dst.pack(lateralOffsetError_m);
	byteSize += dst.pack(depthOffsetError_m);
	byteSize += dst.pack(headingError_rad);
	byteSize += dst.pack(correctionCode);
	byteSize += dst.pack(secondaryData1);
	byteSize += dst.pack(secondaryData2);
	byteSize += dst.pack(status);
	byteSize += dst.pack(globalErrorX_m);
	byteSize += dst.pack(globalErrorY_m);
	byteSize += dst.pack(globalErrorHeading_rad);
	return byteSize;
}

uint64 ErrorCorrections::from(system::BufferReader& src)
{
	uint64 byteSize = 0;
	byteSize += src.unpack(magicNumber);
	byteSize += src.unpack(versionNumber);
	byteSize += src.unpack(this->id);
	byteSize += src.unpack(dataLength);
	byteSize += src.unpack(lateralOffsetError_m);
	byteSize += src.unpack(depthOffsetError_m);
	byteSize += src.unpack(headingError_rad);
	byteSize += src.unpack(correctionCode);
	byteSize += src.unpack(secondaryData1);
	byteSize += src.unpack(secondaryData2);
	byteSize += src.unpack(status);
	byteSize += src.unpack(globalErrorX_m);
	byteSize += src.unpack(globalErrorY_m);
	byteSize += src.unpack(globalErrorHeading_rad);
	return byteSize;
}

uint64 ErrorCorrections::length(void) const
{
	uint64 length = 0;
	length += magicNumber.length(); // magicNumber
	length += versionNumber.length(); // versionNumber
	length += commandId.length(); // commandId
	length += dataLength.length(); // dataLength
	length += lateralOffsetError_m.length(); // lateralOffsetError_m
	length += depthOffsetError_m.length(); // depthOffsetError_m
	length += headingError_rad.length(); // headingError_rad
	length += correctionCode.length(); // correctionCode
	length += secondaryData1.length(); // secondaryData1
	length += secondaryData2.length(); // secondaryData2
	length += status.length(); // status
	length += globalErrorX_m.length(); // globalErrorX_m
	length += globalErrorY_m.length(); // globalErrorY_m
	length += globalErrorHeading_rad.length(); // globalErrorHeading_rad
	return length;
}

std::string ErrorCorrections::toString() const
{
	return std::string("ErrorCorrections [0xAACC]");
}

std::ostream& operator<<(std::ostream& output, const ErrorCorrections& object)
{
    output << object.toString();
    return output;
}

std::ostream& operator<<(std::ostream& output, const ErrorCorrections* object)
{
    output << object->toString();
    return output;
}

std::string ErrorCorrections::toXml(unsigned char ojIndentLevel) const
{
	std::ostringstream prefix;
	for(unsigned char i = 0; i < ojIndentLevel; i++)
	{
		prefix << "\t";
	}

	std::ostringstream oss;
	oss << prefix.str() << "<Message name=\"ErrorCorrections\"";
	oss << " id=\"0xAACC\" >\n";
	oss << magicNumber.toXml(ojIndentLevel+1); // magicNumber
	oss << versionNumber.toXml(ojIndentLevel+1); // versionNumber
	oss << commandId.toXml(ojIndentLevel+1); // commandId
	oss << dataLength.toXml(ojIndentLevel+1); // dataLength
	oss << lateralOffsetError_m.toXml(ojIndentLevel+1); // lateralOffsetError_m
	oss << depthOffsetError_m.toXml(ojIndentLevel+1); // depthOffsetError_m
	oss << headingError_rad.toXml(ojIndentLevel+1); // headingError_rad
	oss << correctionCode.toXml(ojIndentLevel+1); // correctionCode
	oss << secondaryData1.toXml(ojIndentLevel+1); // secondaryData1
	oss << secondaryData2.toXml(ojIndentLevel+1); // secondaryData2
	oss << status.toXml(ojIndentLevel+1); // status
	oss << globalErrorX_m.toXml(ojIndentLevel+1); // globalErrorX_m
	oss << globalErrorY_m.toXml(ojIndentLevel+1); // globalErrorY_m
	oss << globalErrorHeading_rad.toXml(ojIndentLevel+1); // globalErrorHeading_rad
	oss << prefix.str() << "</Message>\n";
	return oss.str();
}


// Start of user code for additional methods
// End of user code for additional methods

} // namespace jbt



