
/**
\file Debug.cpp

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/

#include "jbt/Triggers/Debug.h"
// Start of user code for additional includes
// End of user code for additional includes

namespace mechaspin
{


Debug::Debug() : 
	model::Message(),
	magicNumber(),
	versionNumber(),
	commandId(),
	dataLength(),
	debugMessage()
{
	this->id = Debug::ID; // Initialize id member


	magicNumber.setName("MagicNumber");
	magicNumber.setValue(0x204E5344);

	versionNumber.setName("VersionNumber");
	versionNumber.setValue(1);

	commandId.setName("CommandId");
	commandId.setValue(0);

	dataLength.setName("DataLength");
	dataLength.setValue(0);

	debugMessage.setName("DebugMessage");
	debugMessage.setSizeType(model::fields::UNSIGNED_SHORT);

}

Debug::Debug(model::Message *message) :
	model::Message(message),
	magicNumber(),
	versionNumber(),
	commandId(),
	dataLength(),
	debugMessage()
{
	this->id = Debug::ID; // Initialize id member


	magicNumber.setName("MagicNumber");
	magicNumber.setValue(0x204E5344);

	versionNumber.setName("VersionNumber");
	versionNumber.setValue(1);

	commandId.setName("CommandId");
	commandId.setValue(0);

	dataLength.setName("DataLength");
	dataLength.setValue(0);

	debugMessage.setName("DebugMessage");
	debugMessage.setSizeType(model::fields::UNSIGNED_SHORT);


	system::Buffer* payloadBuffer = dynamic_cast<system::Buffer*>(message->getPayload());
	if (payloadBuffer != NULL)
	{
	    system::BufferReader& reader = payloadBuffer->getReader();
	    reader.reset(); 
		this->from(reader);
		// payloadBuffer->reset();
	}
}

Debug::~Debug()
{

}


uint32_t Debug::getMagicNumber(void)
{
	return this->magicNumber.getValue();
}

bool Debug::setMagicNumber(uint32_t value)
{
	return this->magicNumber.setValue(value);
}

int8_t Debug::getVersionNumber(void)
{
	return this->versionNumber.getValue();
}

bool Debug::setVersionNumber(int8_t value)
{
	return this->versionNumber.setValue(value);
}

uint32_t Debug::getCommandId(void)
{
	return this->commandId.getValue();
}

bool Debug::setCommandId(uint32_t value)
{
	return this->commandId.setValue(value);
}

uint32_t Debug::getDataLength(void)
{
	return this->dataLength.getValue();
}

bool Debug::setDataLength(uint32_t value)
{
	return this->dataLength.setValue(value);
}

std::string Debug::getDebugMessage(void)
{
	return this->debugMessage.getValue();
}

bool Debug::setDebugMessage(std::string value)
{
	return this->debugMessage.setValue(value);
}

uint64 Debug::to(system::BufferWriter& dst) const
{
	uint64 byteSize = 0;
	byteSize += dst.pack(magicNumber);
	byteSize += dst.pack(versionNumber);
	byteSize += dst.pack(this->id);
	byteSize += dst.pack(static_cast<uint32>(this->length()-13));
	byteSize += dst.pack(debugMessage);
	return byteSize;
}

uint64 Debug::from(system::BufferReader& src)
{
	uint64 byteSize = 0;
	byteSize += src.unpack(magicNumber);
	byteSize += src.unpack(versionNumber);
	byteSize += src.unpack(this->id);
	byteSize += src.unpack(dataLength);
	byteSize += src.unpack(debugMessage);
	return byteSize;
}

uint64 Debug::length(void) const
{
	uint64 length = 0;
	length += magicNumber.length(); // magicNumber
	length += versionNumber.length(); // versionNumber
	length += commandId.length(); // commandId
	length += dataLength.length(); // dataLength
	length += debugMessage.length(); // debugMessage
	return length;
}

std::string Debug::toString() const
{
	return std::string("Debug [0xDB03]");
}

std::ostream& operator<<(std::ostream& output, const Debug& object)
{
    output << object.toString();
    return output;
}

std::ostream& operator<<(std::ostream& output, const Debug* object)
{
    output << object->toString();
    return output;
}

std::string Debug::toXml(unsigned char ojIndentLevel) const
{
	std::ostringstream prefix;
	for(unsigned char i = 0; i < ojIndentLevel; i++)
	{
		prefix << "\t";
	}

	std::ostringstream oss;
	oss << prefix.str() << "<Message name=\"Debug\"";
	oss << " id=\"0xDB03\" >\n";
	oss << magicNumber.toXml(ojIndentLevel+1); // magicNumber
	oss << versionNumber.toXml(ojIndentLevel+1); // versionNumber
	oss << commandId.toXml(ojIndentLevel+1); // commandId
	oss << dataLength.toXml(ojIndentLevel+1); // dataLength
	oss << debugMessage.toXml(ojIndentLevel+1); // debugMessage
	oss << prefix.str() << "</Message>\n";
	return oss.str();
}


// Start of user code for additional methods
// End of user code for additional methods

} // namespace jbt



