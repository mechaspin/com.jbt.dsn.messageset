
/**
\file Configuration.cpp

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/

#include "jbt/Triggers/Configuration.h"
// Start of user code for additional includes
// End of user code for additional includes

namespace mechaspin
{


Configuration::Configuration() : 
	model::Message(),
	magicNumber(),
	versionNumber(),
	commandId(),
	dataLength(),
	scanUseInterval(),
	numberScans(),
	numberActiveToStart(),
	onlyUseSensedEdgeLength(),
	resetLaneBoundaries(),
	alignToDetectedScans(),
	updateOnlyIfGood(),
	useInitialLaneCompromise(),
	laneMismatchTolerance_m(),
	laneAngularMismatchTolerance_deg(),
	minLaneLength_m(),
	initialLaneMinMatch_percent(),
	initialLateralTolerance_m(),
	initialHeadingTolerance_deg(),
	initialLateralTolerance2_m(),
	initialLaneCompromiseMaxAllowedError_m(),
	maxLateralDeviation_m(),
	maxHeadingDeviation_deg(),
	lateralErrorMaxStepDelta_m(),
	yawErrorMaxStepDelta_deg()
{
	this->id = Configuration::ID; // Initialize id member


	magicNumber.setName("MagicNumber");
	magicNumber.setValue(0x204E5344);

	versionNumber.setName("VersionNumber");
	versionNumber.setValue(1);

	commandId.setName("CommandId");
	commandId.setValue(0);

	dataLength.setName("DataLength");
	dataLength.setValue(0);

	scanUseInterval.setName("ScanUseInterval");
	scanUseInterval.setValue(0);

	numberScans.setName("NumberScans");
	numberScans.setValue(0);

	numberActiveToStart.setName("NumberActiveToStart");
	numberActiveToStart.setValue(0);

	onlyUseSensedEdgeLength.setName("OnlyUseSensedEdgeLength");
	onlyUseSensedEdgeLength.setValue(0);

	resetLaneBoundaries.setName("ResetLaneBoundaries");
	resetLaneBoundaries.setValue(0);

	alignToDetectedScans.setName("AlignToDetectedScans");
	alignToDetectedScans.setValue(0);

	updateOnlyIfGood.setName("UpdateOnlyIfGood");
	updateOnlyIfGood.setValue(0);

	useInitialLaneCompromise.setName("UseInitialLaneCompromise");
	useInitialLaneCompromise.setValue(0);

	laneMismatchTolerance_m.setName("LaneMismatchTolerance");
	laneMismatchTolerance_m.setValue(0);

	laneAngularMismatchTolerance_deg.setName("LaneAngularMismatchTolerance");
	laneAngularMismatchTolerance_deg.setValue(0);

	minLaneLength_m.setName("MinLaneLength");
	minLaneLength_m.setValue(0);

	initialLaneMinMatch_percent.setName("InitialLaneMinMatch");
	initialLaneMinMatch_percent.setValue(0);

	initialLateralTolerance_m.setName("InitialLateralTolerance");
	initialLateralTolerance_m.setValue(0);

	initialHeadingTolerance_deg.setName("InitialHeadingTolerance");
	initialHeadingTolerance_deg.setValue(0);

	initialLateralTolerance2_m.setName("InitialLateralTolerance2");
	initialLateralTolerance2_m.setValue(0);

	initialLaneCompromiseMaxAllowedError_m.setName("InitialLaneCompromiseMaxAllowedError");
	initialLaneCompromiseMaxAllowedError_m.setValue(0);

	maxLateralDeviation_m.setName("MaxLateralDeviation");
	maxLateralDeviation_m.setValue(0);

	maxHeadingDeviation_deg.setName("MaxHeadingDeviation");
	maxHeadingDeviation_deg.setValue(0);

	lateralErrorMaxStepDelta_m.setName("LateralErrorMaxStepDelta");
	lateralErrorMaxStepDelta_m.setValue(0);

	yawErrorMaxStepDelta_deg.setName("YawErrorMaxStepDelta");
	yawErrorMaxStepDelta_deg.setValue(0);

}

Configuration::Configuration(model::Message *message) :
	model::Message(message),
	magicNumber(),
	versionNumber(),
	commandId(),
	dataLength(),
	scanUseInterval(),
	numberScans(),
	numberActiveToStart(),
	onlyUseSensedEdgeLength(),
	resetLaneBoundaries(),
	alignToDetectedScans(),
	updateOnlyIfGood(),
	useInitialLaneCompromise(),
	laneMismatchTolerance_m(),
	laneAngularMismatchTolerance_deg(),
	minLaneLength_m(),
	initialLaneMinMatch_percent(),
	initialLateralTolerance_m(),
	initialHeadingTolerance_deg(),
	initialLateralTolerance2_m(),
	initialLaneCompromiseMaxAllowedError_m(),
	maxLateralDeviation_m(),
	maxHeadingDeviation_deg(),
	lateralErrorMaxStepDelta_m(),
	yawErrorMaxStepDelta_deg()
{
	this->id = Configuration::ID; // Initialize id member


	magicNumber.setName("MagicNumber");
	magicNumber.setValue(0x204E5344);

	versionNumber.setName("VersionNumber");
	versionNumber.setValue(1);

	commandId.setName("CommandId");
	commandId.setValue(0);

	dataLength.setName("DataLength");
	dataLength.setValue(0);

	scanUseInterval.setName("ScanUseInterval");
	scanUseInterval.setValue(0);

	numberScans.setName("NumberScans");
	numberScans.setValue(0);

	numberActiveToStart.setName("NumberActiveToStart");
	numberActiveToStart.setValue(0);

	onlyUseSensedEdgeLength.setName("OnlyUseSensedEdgeLength");
	onlyUseSensedEdgeLength.setValue(0);

	resetLaneBoundaries.setName("ResetLaneBoundaries");
	resetLaneBoundaries.setValue(0);

	alignToDetectedScans.setName("AlignToDetectedScans");
	alignToDetectedScans.setValue(0);

	updateOnlyIfGood.setName("UpdateOnlyIfGood");
	updateOnlyIfGood.setValue(0);

	useInitialLaneCompromise.setName("UseInitialLaneCompromise");
	useInitialLaneCompromise.setValue(0);

	laneMismatchTolerance_m.setName("LaneMismatchTolerance");
	laneMismatchTolerance_m.setValue(0);

	laneAngularMismatchTolerance_deg.setName("LaneAngularMismatchTolerance");
	laneAngularMismatchTolerance_deg.setValue(0);

	minLaneLength_m.setName("MinLaneLength");
	minLaneLength_m.setValue(0);

	initialLaneMinMatch_percent.setName("InitialLaneMinMatch");
	initialLaneMinMatch_percent.setValue(0);

	initialLateralTolerance_m.setName("InitialLateralTolerance");
	initialLateralTolerance_m.setValue(0);

	initialHeadingTolerance_deg.setName("InitialHeadingTolerance");
	initialHeadingTolerance_deg.setValue(0);

	initialLateralTolerance2_m.setName("InitialLateralTolerance2");
	initialLateralTolerance2_m.setValue(0);

	initialLaneCompromiseMaxAllowedError_m.setName("InitialLaneCompromiseMaxAllowedError");
	initialLaneCompromiseMaxAllowedError_m.setValue(0);

	maxLateralDeviation_m.setName("MaxLateralDeviation");
	maxLateralDeviation_m.setValue(0);

	maxHeadingDeviation_deg.setName("MaxHeadingDeviation");
	maxHeadingDeviation_deg.setValue(0);

	lateralErrorMaxStepDelta_m.setName("LateralErrorMaxStepDelta");
	lateralErrorMaxStepDelta_m.setValue(0);

	yawErrorMaxStepDelta_deg.setName("YawErrorMaxStepDelta");
	yawErrorMaxStepDelta_deg.setValue(0);


	system::Buffer* payloadBuffer = dynamic_cast<system::Buffer*>(message->getPayload());
	if (payloadBuffer != NULL)
	{
	    system::BufferReader& reader = payloadBuffer->getReader();
	    reader.reset(); 
		this->from(reader);
		// payloadBuffer->reset();
	}
}

Configuration::~Configuration()
{

}


uint32_t Configuration::getMagicNumber(void)
{
	return this->magicNumber.getValue();
}

bool Configuration::setMagicNumber(uint32_t value)
{
	return this->magicNumber.setValue(value);
}

int8_t Configuration::getVersionNumber(void)
{
	return this->versionNumber.getValue();
}

bool Configuration::setVersionNumber(int8_t value)
{
	return this->versionNumber.setValue(value);
}

uint32_t Configuration::getCommandId(void)
{
	return this->commandId.getValue();
}

bool Configuration::setCommandId(uint32_t value)
{
	return this->commandId.setValue(value);
}

uint32_t Configuration::getDataLength(void)
{
	return this->dataLength.getValue();
}

bool Configuration::setDataLength(uint32_t value)
{
	return this->dataLength.setValue(value);
}

int8_t Configuration::getScanUseInterval(void)
{
	return this->scanUseInterval.getValue();
}

bool Configuration::setScanUseInterval(int8_t value)
{
	return this->scanUseInterval.setValue(value);
}

int8_t Configuration::getNumberScans(void)
{
	return this->numberScans.getValue();
}

bool Configuration::setNumberScans(int8_t value)
{
	return this->numberScans.setValue(value);
}

int8_t Configuration::getNumberActiveToStart(void)
{
	return this->numberActiveToStart.getValue();
}

bool Configuration::setNumberActiveToStart(int8_t value)
{
	return this->numberActiveToStart.setValue(value);
}

int8_t Configuration::getOnlyUseSensedEdgeLength(void)
{
	return this->onlyUseSensedEdgeLength.getValue();
}

bool Configuration::setOnlyUseSensedEdgeLength(int8_t value)
{
	return this->onlyUseSensedEdgeLength.setValue(value);
}

int8_t Configuration::getResetLaneBoundaries(void)
{
	return this->resetLaneBoundaries.getValue();
}

bool Configuration::setResetLaneBoundaries(int8_t value)
{
	return this->resetLaneBoundaries.setValue(value);
}

int8_t Configuration::getAlignToDetectedScans(void)
{
	return this->alignToDetectedScans.getValue();
}

bool Configuration::setAlignToDetectedScans(int8_t value)
{
	return this->alignToDetectedScans.setValue(value);
}

int8_t Configuration::getUpdateOnlyIfGood(void)
{
	return this->updateOnlyIfGood.getValue();
}

bool Configuration::setUpdateOnlyIfGood(int8_t value)
{
	return this->updateOnlyIfGood.setValue(value);
}

int8_t Configuration::getUseInitialLaneCompromise(void)
{
	return this->useInitialLaneCompromise.getValue();
}

bool Configuration::setUseInitialLaneCompromise(int8_t value)
{
	return this->useInitialLaneCompromise.setValue(value);
}

float Configuration::getLaneMismatchTolerance_m(void)
{
	return this->laneMismatchTolerance_m.getValue();
}

bool Configuration::setLaneMismatchTolerance_m(float value)
{
	return this->laneMismatchTolerance_m.setValue(value);
}

float Configuration::getLaneAngularMismatchTolerance_deg(void)
{
	return this->laneAngularMismatchTolerance_deg.getValue();
}

bool Configuration::setLaneAngularMismatchTolerance_deg(float value)
{
	return this->laneAngularMismatchTolerance_deg.setValue(value);
}

float Configuration::getMinLaneLength_m(void)
{
	return this->minLaneLength_m.getValue();
}

bool Configuration::setMinLaneLength_m(float value)
{
	return this->minLaneLength_m.setValue(value);
}

float Configuration::getInitialLaneMinMatch_percent(void)
{
	return this->initialLaneMinMatch_percent.getValue();
}

bool Configuration::setInitialLaneMinMatch_percent(float value)
{
	return this->initialLaneMinMatch_percent.setValue(value);
}

float Configuration::getInitialLateralTolerance_m(void)
{
	return this->initialLateralTolerance_m.getValue();
}

bool Configuration::setInitialLateralTolerance_m(float value)
{
	return this->initialLateralTolerance_m.setValue(value);
}

float Configuration::getInitialHeadingTolerance_deg(void)
{
	return this->initialHeadingTolerance_deg.getValue();
}

bool Configuration::setInitialHeadingTolerance_deg(float value)
{
	return this->initialHeadingTolerance_deg.setValue(value);
}

float Configuration::getInitialLateralTolerance2_m(void)
{
	return this->initialLateralTolerance2_m.getValue();
}

bool Configuration::setInitialLateralTolerance2_m(float value)
{
	return this->initialLateralTolerance2_m.setValue(value);
}

float Configuration::getInitialLaneCompromiseMaxAllowedError_m(void)
{
	return this->initialLaneCompromiseMaxAllowedError_m.getValue();
}

bool Configuration::setInitialLaneCompromiseMaxAllowedError_m(float value)
{
	return this->initialLaneCompromiseMaxAllowedError_m.setValue(value);
}

float Configuration::getMaxLateralDeviation_m(void)
{
	return this->maxLateralDeviation_m.getValue();
}

bool Configuration::setMaxLateralDeviation_m(float value)
{
	return this->maxLateralDeviation_m.setValue(value);
}

float Configuration::getMaxHeadingDeviation_deg(void)
{
	return this->maxHeadingDeviation_deg.getValue();
}

bool Configuration::setMaxHeadingDeviation_deg(float value)
{
	return this->maxHeadingDeviation_deg.setValue(value);
}

float Configuration::getLateralErrorMaxStepDelta_m(void)
{
	return this->lateralErrorMaxStepDelta_m.getValue();
}

bool Configuration::setLateralErrorMaxStepDelta_m(float value)
{
	return this->lateralErrorMaxStepDelta_m.setValue(value);
}

float Configuration::getYawErrorMaxStepDelta_deg(void)
{
	return this->yawErrorMaxStepDelta_deg.getValue();
}

bool Configuration::setYawErrorMaxStepDelta_deg(float value)
{
	return this->yawErrorMaxStepDelta_deg.setValue(value);
}

uint64 Configuration::to(system::BufferWriter& dst) const
{
	uint64 byteSize = 0;
	byteSize += dst.pack(magicNumber);
	byteSize += dst.pack(versionNumber);
	byteSize += dst.pack(this->id);
	byteSize += dst.pack(static_cast<uint32>(this->length()-13));
	byteSize += dst.pack(scanUseInterval);
	byteSize += dst.pack(numberScans);
	byteSize += dst.pack(numberActiveToStart);
	byteSize += dst.pack(onlyUseSensedEdgeLength);
	byteSize += dst.pack(resetLaneBoundaries);
	byteSize += dst.pack(alignToDetectedScans);
	byteSize += dst.pack(updateOnlyIfGood);
	byteSize += dst.pack(useInitialLaneCompromise);
	byteSize += dst.pack(laneMismatchTolerance_m);
	byteSize += dst.pack(laneAngularMismatchTolerance_deg);
	byteSize += dst.pack(minLaneLength_m);
	byteSize += dst.pack(initialLaneMinMatch_percent);
	byteSize += dst.pack(initialLateralTolerance_m);
	byteSize += dst.pack(initialHeadingTolerance_deg);
	byteSize += dst.pack(initialLateralTolerance2_m);
	byteSize += dst.pack(initialLaneCompromiseMaxAllowedError_m);
	byteSize += dst.pack(maxLateralDeviation_m);
	byteSize += dst.pack(maxHeadingDeviation_deg);
	byteSize += dst.pack(lateralErrorMaxStepDelta_m);
	byteSize += dst.pack(yawErrorMaxStepDelta_deg);
	return byteSize;
}

uint64 Configuration::from(system::BufferReader& src)
{
	uint64 byteSize = 0;
	byteSize += src.unpack(magicNumber);
	byteSize += src.unpack(versionNumber);
	byteSize += src.unpack(this->id);
	byteSize += src.unpack(dataLength);
	byteSize += src.unpack(scanUseInterval);
	byteSize += src.unpack(numberScans);
	byteSize += src.unpack(numberActiveToStart);
	byteSize += src.unpack(onlyUseSensedEdgeLength);
	byteSize += src.unpack(resetLaneBoundaries);
	byteSize += src.unpack(alignToDetectedScans);
	byteSize += src.unpack(updateOnlyIfGood);
	byteSize += src.unpack(useInitialLaneCompromise);
	byteSize += src.unpack(laneMismatchTolerance_m);
	byteSize += src.unpack(laneAngularMismatchTolerance_deg);
	byteSize += src.unpack(minLaneLength_m);
	byteSize += src.unpack(initialLaneMinMatch_percent);
	byteSize += src.unpack(initialLateralTolerance_m);
	byteSize += src.unpack(initialHeadingTolerance_deg);
	byteSize += src.unpack(initialLateralTolerance2_m);
	byteSize += src.unpack(initialLaneCompromiseMaxAllowedError_m);
	byteSize += src.unpack(maxLateralDeviation_m);
	byteSize += src.unpack(maxHeadingDeviation_deg);
	byteSize += src.unpack(lateralErrorMaxStepDelta_m);
	byteSize += src.unpack(yawErrorMaxStepDelta_deg);
	return byteSize;
}

uint64 Configuration::length(void) const
{
	uint64 length = 0;
	length += magicNumber.length(); // magicNumber
	length += versionNumber.length(); // versionNumber
	length += commandId.length(); // commandId
	length += dataLength.length(); // dataLength
	length += scanUseInterval.length(); // scanUseInterval
	length += numberScans.length(); // numberScans
	length += numberActiveToStart.length(); // numberActiveToStart
	length += onlyUseSensedEdgeLength.length(); // onlyUseSensedEdgeLength
	length += resetLaneBoundaries.length(); // resetLaneBoundaries
	length += alignToDetectedScans.length(); // alignToDetectedScans
	length += updateOnlyIfGood.length(); // updateOnlyIfGood
	length += useInitialLaneCompromise.length(); // useInitialLaneCompromise
	length += laneMismatchTolerance_m.length(); // laneMismatchTolerance_m
	length += laneAngularMismatchTolerance_deg.length(); // laneAngularMismatchTolerance_deg
	length += minLaneLength_m.length(); // minLaneLength_m
	length += initialLaneMinMatch_percent.length(); // initialLaneMinMatch_percent
	length += initialLateralTolerance_m.length(); // initialLateralTolerance_m
	length += initialHeadingTolerance_deg.length(); // initialHeadingTolerance_deg
	length += initialLateralTolerance2_m.length(); // initialLateralTolerance2_m
	length += initialLaneCompromiseMaxAllowedError_m.length(); // initialLaneCompromiseMaxAllowedError_m
	length += maxLateralDeviation_m.length(); // maxLateralDeviation_m
	length += maxHeadingDeviation_deg.length(); // maxHeadingDeviation_deg
	length += lateralErrorMaxStepDelta_m.length(); // lateralErrorMaxStepDelta_m
	length += yawErrorMaxStepDelta_deg.length(); // yawErrorMaxStepDelta_deg
	return length;
}

std::string Configuration::toString() const
{
	return std::string("Configuration [0xAAF5]");
}

std::ostream& operator<<(std::ostream& output, const Configuration& object)
{
    output << object.toString();
    return output;
}

std::ostream& operator<<(std::ostream& output, const Configuration* object)
{
    output << object->toString();
    return output;
}

std::string Configuration::toXml(unsigned char ojIndentLevel) const
{
	std::ostringstream prefix;
	for(unsigned char i = 0; i < ojIndentLevel; i++)
	{
		prefix << "\t";
	}

	std::ostringstream oss;
	oss << prefix.str() << "<Message name=\"Configuration\"";
	oss << " id=\"0xAAF5\" >\n";
	oss << magicNumber.toXml(ojIndentLevel+1); // magicNumber
	oss << versionNumber.toXml(ojIndentLevel+1); // versionNumber
	oss << commandId.toXml(ojIndentLevel+1); // commandId
	oss << dataLength.toXml(ojIndentLevel+1); // dataLength
	oss << scanUseInterval.toXml(ojIndentLevel+1); // scanUseInterval
	oss << numberScans.toXml(ojIndentLevel+1); // numberScans
	oss << numberActiveToStart.toXml(ojIndentLevel+1); // numberActiveToStart
	oss << onlyUseSensedEdgeLength.toXml(ojIndentLevel+1); // onlyUseSensedEdgeLength
	oss << resetLaneBoundaries.toXml(ojIndentLevel+1); // resetLaneBoundaries
	oss << alignToDetectedScans.toXml(ojIndentLevel+1); // alignToDetectedScans
	oss << updateOnlyIfGood.toXml(ojIndentLevel+1); // updateOnlyIfGood
	oss << useInitialLaneCompromise.toXml(ojIndentLevel+1); // useInitialLaneCompromise
	oss << laneMismatchTolerance_m.toXml(ojIndentLevel+1); // laneMismatchTolerance_m
	oss << laneAngularMismatchTolerance_deg.toXml(ojIndentLevel+1); // laneAngularMismatchTolerance_deg
	oss << minLaneLength_m.toXml(ojIndentLevel+1); // minLaneLength_m
	oss << initialLaneMinMatch_percent.toXml(ojIndentLevel+1); // initialLaneMinMatch_percent
	oss << initialLateralTolerance_m.toXml(ojIndentLevel+1); // initialLateralTolerance_m
	oss << initialHeadingTolerance_deg.toXml(ojIndentLevel+1); // initialHeadingTolerance_deg
	oss << initialLateralTolerance2_m.toXml(ojIndentLevel+1); // initialLateralTolerance2_m
	oss << initialLaneCompromiseMaxAllowedError_m.toXml(ojIndentLevel+1); // initialLaneCompromiseMaxAllowedError_m
	oss << maxLateralDeviation_m.toXml(ojIndentLevel+1); // maxLateralDeviation_m
	oss << maxHeadingDeviation_deg.toXml(ojIndentLevel+1); // maxHeadingDeviation_deg
	oss << lateralErrorMaxStepDelta_m.toXml(ojIndentLevel+1); // lateralErrorMaxStepDelta_m
	oss << yawErrorMaxStepDelta_deg.toXml(ojIndentLevel+1); // yawErrorMaxStepDelta_deg
	oss << prefix.str() << "</Message>\n";
	return oss.str();
}


// Start of user code for additional methods
// End of user code for additional methods

} // namespace jbt



