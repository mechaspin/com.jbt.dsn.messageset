
/**
\file StatusBitField.cpp

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/

#include <system.h>
#include <model/fields.h>
#include "jbt/Triggers/Fields/StatusBitField.h"
// Start of user code for additional includes:
// End of user code

namespace mechaspin
{

StatusBitField::StatusBitField() : 
	allOk(),
	pathHeadingErrorTooLarge(),
	pathLateralErrorTooLarge(),
	insufficientData(),
	laneFitHeadingError(),
	laneFitLateralError(),
	initialCompromiseErrorTooLarge(),
	laneConfidenceTooLow(),
	noHostInformation(),
	notConfiguredError()
{
	// Initialize BitFieldRange Members
}

StatusBitField::~StatusBitField()
{

}

bool StatusBitField::setAllOk(bool value)
{
	this->allOk = value;
	return true;
}

bool StatusBitField::getAllOk(void) const
{
	return this->allOk;
}


bool StatusBitField::setPathHeadingErrorTooLarge(bool value)
{
	this->pathHeadingErrorTooLarge = value;
	return true;
}

bool StatusBitField::getPathHeadingErrorTooLarge(void) const
{
	return this->pathHeadingErrorTooLarge;
}


bool StatusBitField::setPathLateralErrorTooLarge(bool value)
{
	this->pathLateralErrorTooLarge = value;
	return true;
}

bool StatusBitField::getPathLateralErrorTooLarge(void) const
{
	return this->pathLateralErrorTooLarge;
}


bool StatusBitField::setInsufficientData(bool value)
{
	this->insufficientData = value;
	return true;
}

bool StatusBitField::getInsufficientData(void) const
{
	return this->insufficientData;
}


bool StatusBitField::setLaneFitHeadingError(bool value)
{
	this->laneFitHeadingError = value;
	return true;
}

bool StatusBitField::getLaneFitHeadingError(void) const
{
	return this->laneFitHeadingError;
}


bool StatusBitField::setLaneFitLateralError(bool value)
{
	this->laneFitLateralError = value;
	return true;
}

bool StatusBitField::getLaneFitLateralError(void) const
{
	return this->laneFitLateralError;
}


bool StatusBitField::setInitialCompromiseErrorTooLarge(bool value)
{
	this->initialCompromiseErrorTooLarge = value;
	return true;
}

bool StatusBitField::getInitialCompromiseErrorTooLarge(void) const
{
	return this->initialCompromiseErrorTooLarge;
}


bool StatusBitField::setLaneConfidenceTooLow(bool value)
{
	this->laneConfidenceTooLow = value;
	return true;
}

bool StatusBitField::getLaneConfidenceTooLow(void) const
{
	return this->laneConfidenceTooLow;
}


bool StatusBitField::setNoHostInformation(bool value)
{
	this->noHostInformation = value;
	return true;
}

bool StatusBitField::getNoHostInformation(void) const
{
	return this->noHostInformation;
}


bool StatusBitField::setNotConfiguredError(bool value)
{
	this->notConfiguredError = value;
	return true;
}

bool StatusBitField::getNotConfiguredError(void) const
{
	return this->notConfiguredError;
}



uint32_t StatusBitField::getIntegerValue(void) const
{
	uint32_t intValue = 0;

	intValue |= ((this->allOk & StatusBitField::ALLOK_BIT_MASK) << StatusBitField::ALLOK_START_BIT);
	intValue |= ((this->pathHeadingErrorTooLarge & StatusBitField::PATHHEADINGERRORTOOLARGE_BIT_MASK) << StatusBitField::PATHHEADINGERRORTOOLARGE_START_BIT);
	intValue |= ((this->pathLateralErrorTooLarge & StatusBitField::PATHLATERALERRORTOOLARGE_BIT_MASK) << StatusBitField::PATHLATERALERRORTOOLARGE_START_BIT);
	intValue |= ((this->insufficientData & StatusBitField::INSUFFICIENTDATA_BIT_MASK) << StatusBitField::INSUFFICIENTDATA_START_BIT);
	intValue |= ((this->laneFitHeadingError & StatusBitField::LANEFITHEADINGERROR_BIT_MASK) << StatusBitField::LANEFITHEADINGERROR_START_BIT);
	intValue |= ((this->laneFitLateralError & StatusBitField::LANEFITLATERALERROR_BIT_MASK) << StatusBitField::LANEFITLATERALERROR_START_BIT);
	intValue |= ((this->initialCompromiseErrorTooLarge & StatusBitField::INITIALCOMPROMISEERRORTOOLARGE_BIT_MASK) << StatusBitField::INITIALCOMPROMISEERRORTOOLARGE_START_BIT);
	intValue |= ((this->laneConfidenceTooLow & StatusBitField::LANECONFIDENCETOOLOW_BIT_MASK) << StatusBitField::LANECONFIDENCETOOLOW_START_BIT);
	intValue |= ((this->noHostInformation & StatusBitField::NOHOSTINFORMATION_BIT_MASK) << StatusBitField::NOHOSTINFORMATION_START_BIT);
	intValue |= ((this->notConfiguredError & StatusBitField::NOTCONFIGUREDERROR_BIT_MASK) << StatusBitField::NOTCONFIGUREDERROR_START_BIT);

	return intValue;
}

void StatusBitField::setIntegerValue(uint32_t intValue)
{
	this->allOk = static_cast<bool>((intValue >> (StatusBitField::ALLOK_START_BIT)) & StatusBitField::ALLOK_BIT_MASK);
	this->pathHeadingErrorTooLarge = static_cast<bool>((intValue >> (StatusBitField::PATHHEADINGERRORTOOLARGE_START_BIT)) & StatusBitField::PATHHEADINGERRORTOOLARGE_BIT_MASK);
	this->pathLateralErrorTooLarge = static_cast<bool>((intValue >> (StatusBitField::PATHLATERALERRORTOOLARGE_START_BIT)) & StatusBitField::PATHLATERALERRORTOOLARGE_BIT_MASK);
	this->insufficientData = static_cast<bool>((intValue >> (StatusBitField::INSUFFICIENTDATA_START_BIT)) & StatusBitField::INSUFFICIENTDATA_BIT_MASK);
	this->laneFitHeadingError = static_cast<bool>((intValue >> (StatusBitField::LANEFITHEADINGERROR_START_BIT)) & StatusBitField::LANEFITHEADINGERROR_BIT_MASK);
	this->laneFitLateralError = static_cast<bool>((intValue >> (StatusBitField::LANEFITLATERALERROR_START_BIT)) & StatusBitField::LANEFITLATERALERROR_BIT_MASK);
	this->initialCompromiseErrorTooLarge = static_cast<bool>((intValue >> (StatusBitField::INITIALCOMPROMISEERRORTOOLARGE_START_BIT)) & StatusBitField::INITIALCOMPROMISEERRORTOOLARGE_BIT_MASK);
	this->laneConfidenceTooLow = static_cast<bool>((intValue >> (StatusBitField::LANECONFIDENCETOOLOW_START_BIT)) & StatusBitField::LANECONFIDENCETOOLOW_BIT_MASK);
	this->noHostInformation = static_cast<bool>((intValue >> (StatusBitField::NOHOSTINFORMATION_START_BIT)) & StatusBitField::NOHOSTINFORMATION_BIT_MASK);
	this->notConfiguredError = static_cast<bool>((intValue >> (StatusBitField::NOTCONFIGUREDERROR_START_BIT)) & StatusBitField::NOTCONFIGUREDERROR_BIT_MASK);
}

uint64 StatusBitField::to(system::BufferWriter& dst) const
{
	uint32_t intValue = getIntegerValue();
	return dst.pack(intValue);
}

uint64 StatusBitField::from(system::BufferReader& src)
{
	uint64 byteSize = 0;
	uint32_t intValue = 0;
	byteSize = src.unpack(intValue);

	this->setIntegerValue(intValue);

	return byteSize;
}

uint64 StatusBitField::length(void) const
{
	return sizeof(uint32_t);
}

void StatusBitField::copy(StatusBitField& source)
{
	this->setName(source.getName());

	setAllOk(source.getAllOk());
	setPathHeadingErrorTooLarge(source.getPathHeadingErrorTooLarge());
	setPathLateralErrorTooLarge(source.getPathLateralErrorTooLarge());
	setInsufficientData(source.getInsufficientData());
	setLaneFitHeadingError(source.getLaneFitHeadingError());
	setLaneFitLateralError(source.getLaneFitLateralError());
	setInitialCompromiseErrorTooLarge(source.getInitialCompromiseErrorTooLarge());
	setLaneConfidenceTooLow(source.getLaneConfidenceTooLow());
	setNoHostInformation(source.getNoHostInformation());
	setNotConfiguredError(source.getNotConfiguredError());

}

std::string StatusBitField::toXml(unsigned char ojIndentLevel) const
{
	uint32_t intValue = getIntegerValue();

	std::ostringstream prefix;
	for(unsigned char i = 0; i < ojIndentLevel; i++)
	{
		prefix << "\t";
	}

	std::ostringstream oss;
	oss << prefix.str() << "<BitField name=\"" << this->name << "\">\n";
	oss << prefix.str() << "\t" << "<intValue>" << intValue << "</intValue>\n";
	oss << prefix.str() << "\t" << "<fields>\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"AllOk\" value=\"" << getAllOk() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"PathHeadingErrorTooLarge\" value=\"" << getPathHeadingErrorTooLarge() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"PathLateralErrorTooLarge\" value=\"" << getPathLateralErrorTooLarge() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"InsufficientData\" value=\"" << getInsufficientData() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"LaneFitHeadingError\" value=\"" << getLaneFitHeadingError() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"LaneFitLateralError\" value=\"" << getLaneFitLateralError() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"InitialCompromiseErrorTooLarge\" value=\"" << getInitialCompromiseErrorTooLarge() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"LaneConfidenceTooLow\" value=\"" << getLaneConfidenceTooLow() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"NoHostInformation\" value=\"" << getNoHostInformation() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"NotConfiguredError\" value=\"" << getNotConfiguredError() << "\" />\n";
	oss << prefix.str() << "\t" << "</fields>\n";
	oss << prefix.str() << "</BitField>\n";
	return oss.str();
}

// Start of user code for additional methods:
// End of user code

} // namespace jbt


