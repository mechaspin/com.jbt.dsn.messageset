
/**
\file DebugModeEnumeration.cpp

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/

#include <system.h>
#include <model/fields.h>
#include "jbt/Triggers/Fields/DebugModeEnumeration.h"

namespace mechaspin
{

DebugModeEnumeration::DebugModeEnumeration() :
	value(DebugModeEnumeration::DEBUGMODEINACTIVE)
{
}

DebugModeEnumeration::DebugModeEnumeration(DebugModeEnumeration::DebugModeEnum value) :
	value(value)
{
}

DebugModeEnumeration::~DebugModeEnumeration()
{

}

DebugModeEnumeration::DebugModeEnum DebugModeEnumeration::getValue(void) const
{
	return this->value;
}

void DebugModeEnumeration::setValue(DebugModeEnumeration::DebugModeEnum value)
{
	this->value = value;
}

uint64 DebugModeEnumeration::to(system::BufferWriter& dst) const
{
	return dst.pack(static_cast<uint8_t>(this->getValue()));
}

uint64 DebugModeEnumeration::from(system::BufferReader& src)
{
	uint64 sizeBytes = 0;
	uint8_t intValue;
	sizeBytes = src.unpack(intValue);
	this->setValue(static_cast<DebugModeEnumeration::DebugModeEnum>(intValue));
	return sizeBytes;
}

uint64 DebugModeEnumeration::length(void) const
{
	return sizeof(uint8_t);
}

std::string DebugModeEnumeration::toXml(unsigned char ojIndentLevel) const
{
	std::ostringstream prefix;
	for(unsigned char i = 0; i < ojIndentLevel; i++)
	{
		prefix << "\t";
	}

	std::ostringstream oss;
	oss << prefix.str() << "<Enumeration name=\"DebugMode\" intValue=\"" << this->getValue() << "\" strValue=\"" << this->toString() << "\" />\n";
	return oss.str();
}

std::string DebugModeEnumeration::toString(DebugModeEnumeration::DebugModeEnum value) 
{
	switch(value)
	{
		case DebugModeEnumeration::DEBUGMODEINACTIVE:
			return "DebugModeInactive";
		case DebugModeEnumeration::DEBUGMODEACTIVE:
			return "DebugModeActive";
		default:
			return "Unknown";
	}
}

std::string DebugModeEnumeration::toString() const
{
	return toString(this->value);
}

} // namespace jbt


