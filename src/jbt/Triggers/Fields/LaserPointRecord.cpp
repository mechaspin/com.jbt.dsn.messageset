/**
\file LaserPointRecord.cpp

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/


#include <system.h>
#include <model/fields.h>
#include "jbt/Triggers/Fields/LaserPointRecord.h"
// Start of user code for additional includes
// End of user code for additional includes


namespace mechaspin
{

LaserPointRecord::LaserPointRecord():
	x_m(),
	y_m()
{

	x_m.setName("x");
	x_m.setValue(0);

	y_m.setName("y");
	y_m.setValue(0);

}

LaserPointRecord::LaserPointRecord(const LaserPointRecord &source)
{

	this->copy(const_cast<LaserPointRecord&>(source));
}

LaserPointRecord::~LaserPointRecord()
{

}


float LaserPointRecord::getX_m(void)
{
	return this->x_m.getValue();
}

bool LaserPointRecord::setX_m(float value)
{
	return this->x_m.setValue(value);
}

float LaserPointRecord::getY_m(void)
{
	return this->y_m.getValue();
}

bool LaserPointRecord::setY_m(float value)
{
	return this->y_m.setValue(value);
}

uint64 LaserPointRecord::to(system::BufferWriter& dst) const
{
	uint64 byteSize = 0;
	byteSize += dst.pack(x_m);
	byteSize += dst.pack(y_m);
	return byteSize;
}

uint64 LaserPointRecord::from(system::BufferReader& src)
{
	uint64 byteSize = 0;
	byteSize += src.unpack(x_m);
	byteSize += src.unpack(y_m);
	return byteSize;
}

uint64 LaserPointRecord::length(void) const
{
	uint64 length = 0;
	length += x_m.length(); // x_m
	length += y_m.length(); // y_m
	return length;
}

std::string LaserPointRecord::toXml(unsigned char ojIndentLevel) const
{
	std::ostringstream prefix;
	for(unsigned char i = 0; i < ojIndentLevel; i++)
	{
		prefix << "\t";
	}

	std::ostringstream oss;
	oss << prefix.str() << "<Record type=\"LaserPointRecord\">\n";
	oss << x_m.toXml(ojIndentLevel+1); // x_m
	oss << y_m.toXml(ojIndentLevel+1); // y_m
	oss << prefix.str() << "</Record>\n";
	return oss.str();
}



void LaserPointRecord::copy(LaserPointRecord& source)
{
	this->setName(source.getName());
	
	this->x_m.setName("x");
	this->x_m.setValue(source.getX_m()); 
 
	this->y_m.setName("y");
	this->y_m.setValue(source.getY_m()); 
 
}

// Start of user code for additional members
// End of user code for additional members

} // namespace jbt


