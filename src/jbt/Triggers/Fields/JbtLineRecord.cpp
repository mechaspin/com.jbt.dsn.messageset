/**
\file JbtLineRecord.cpp

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/


#include <system.h>
#include <model/fields.h>
#include "jbt/Triggers/Fields/JbtLineRecord.h"
// Start of user code for additional includes
// End of user code for additional includes


namespace mechaspin
{

JbtLineRecord::JbtLineRecord():
	startPointX_m(),
	startPointY_m(),
	endPointX_m(),
	endPointY_m()
{

	startPointX_m.setName("StartPointX");
	startPointX_m.setValue(0);

	startPointY_m.setName("StartPointY");
	startPointY_m.setValue(0);

	endPointX_m.setName("EndPointX");
	endPointX_m.setValue(0);

	endPointY_m.setName("EndPointY");
	endPointY_m.setValue(0);

}

JbtLineRecord::JbtLineRecord(const JbtLineRecord &source)
{

	this->copy(const_cast<JbtLineRecord&>(source));
}

JbtLineRecord::~JbtLineRecord()
{

}


float JbtLineRecord::getStartPointX_m(void)
{
	return this->startPointX_m.getValue();
}

bool JbtLineRecord::setStartPointX_m(float value)
{
	return this->startPointX_m.setValue(value);
}

float JbtLineRecord::getStartPointY_m(void)
{
	return this->startPointY_m.getValue();
}

bool JbtLineRecord::setStartPointY_m(float value)
{
	return this->startPointY_m.setValue(value);
}

float JbtLineRecord::getEndPointX_m(void)
{
	return this->endPointX_m.getValue();
}

bool JbtLineRecord::setEndPointX_m(float value)
{
	return this->endPointX_m.setValue(value);
}

float JbtLineRecord::getEndPointY_m(void)
{
	return this->endPointY_m.getValue();
}

bool JbtLineRecord::setEndPointY_m(float value)
{
	return this->endPointY_m.setValue(value);
}

uint64 JbtLineRecord::to(system::BufferWriter& dst) const
{
	uint64 byteSize = 0;
	byteSize += dst.pack(startPointX_m);
	byteSize += dst.pack(startPointY_m);
	byteSize += dst.pack(endPointX_m);
	byteSize += dst.pack(endPointY_m);
	return byteSize;
}

uint64 JbtLineRecord::from(system::BufferReader& src)
{
	uint64 byteSize = 0;
	byteSize += src.unpack(startPointX_m);
	byteSize += src.unpack(startPointY_m);
	byteSize += src.unpack(endPointX_m);
	byteSize += src.unpack(endPointY_m);
	return byteSize;
}

uint64 JbtLineRecord::length(void) const
{
	uint64 length = 0;
	length += startPointX_m.length(); // startPointX_m
	length += startPointY_m.length(); // startPointY_m
	length += endPointX_m.length(); // endPointX_m
	length += endPointY_m.length(); // endPointY_m
	return length;
}

std::string JbtLineRecord::toXml(unsigned char ojIndentLevel) const
{
	std::ostringstream prefix;
	for(unsigned char i = 0; i < ojIndentLevel; i++)
	{
		prefix << "\t";
	}

	std::ostringstream oss;
	oss << prefix.str() << "<Record type=\"JbtLineRecord\">\n";
	oss << startPointX_m.toXml(ojIndentLevel+1); // startPointX_m
	oss << startPointY_m.toXml(ojIndentLevel+1); // startPointY_m
	oss << endPointX_m.toXml(ojIndentLevel+1); // endPointX_m
	oss << endPointY_m.toXml(ojIndentLevel+1); // endPointY_m
	oss << prefix.str() << "</Record>\n";
	return oss.str();
}



void JbtLineRecord::copy(JbtLineRecord& source)
{
	this->setName(source.getName());
	
	this->startPointX_m.setName("StartPointX");
	this->startPointX_m.setValue(source.getStartPointX_m()); 
 
	this->startPointY_m.setName("StartPointY");
	this->startPointY_m.setValue(source.getStartPointY_m()); 
 
	this->endPointX_m.setName("EndPointX");
	this->endPointX_m.setValue(source.getEndPointX_m()); 
 
	this->endPointY_m.setName("EndPointY");
	this->endPointY_m.setValue(source.getEndPointY_m()); 
 
}

// Start of user code for additional members
// End of user code for additional members

} // namespace mechaspin

