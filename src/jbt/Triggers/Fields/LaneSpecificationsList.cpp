/**
\file LaneSpecificationsList.cpp

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/

#include <system.h>
#include <model/fields.h>
#include "jbt/Triggers/Fields/LaneSpecificationsList.h"

namespace mechaspin
{

LaneSpecificationsList::LaneSpecificationsList() : laneSpecifications()
{
}

LaneSpecificationsList::~LaneSpecificationsList()
{

}

void LaneSpecificationsList::copy(LaneSpecificationsList& source)
{
	this->setName(source.getName());

	this->laneSpecifications = source.getLaneSpecifications();
}

uint64 LaneSpecificationsList::to(system::BufferWriter& dst) const
{
	uint64 result = 0;
	result += dst.pack(static_cast<uint8_t>(this->laneSpecifications.size())); // Count
	
	for (std::size_t i = 0; i < this->laneSpecifications.size(); i++) // Elements
	{
		result += dst.pack(this->laneSpecifications.at(i)); 
	}
	return result;
}

uint64 LaneSpecificationsList::from(system::BufferReader& src)
{
	uint64 result = 0;
	uint8_t count;

	result += src.unpack(count); // Count
	this->laneSpecifications.clear();
	this->laneSpecifications.reserve(count);
	
	for (std::size_t i = 0; i < count; i++) // Elements
	{
		LaneSpecificationsRecord temp;
		// Nothing to Init
		result += src.unpack(temp);
		this->laneSpecifications.push_back(temp);
	}
	return result;
}

uint64 LaneSpecificationsList::length(void) const
{
	uint64 result = 0;
	result += sizeof(uint8_t); // Count
	for (std::size_t i = 0; i < this->laneSpecifications.size(); i++) // Elements
	{
		result += this->laneSpecifications[i].length(); 
	}
	return result;
}

std::string LaneSpecificationsList::toXml(unsigned char ojIndentLevel) const
{
	std::ostringstream prefix;
	for(unsigned char i = 0; i < ojIndentLevel; i++)
	{
		prefix << "\t";
	}

	std::ostringstream oss;
	oss << prefix.str() << "<List name=\"" << this->name << "\">\n";
	oss << prefix.str() << "\t" << "<count>" << this->laneSpecifications.size() << "</count>\n";
	
	for(std::size_t i = 0; i < this->laneSpecifications.size(); i++) // Elements
	{
		oss << this->laneSpecifications.at(i).toXml(ojIndentLevel + 1);
	}
	oss << prefix.str() << "</List>\n";
	return oss.str();
}


std::vector<LaneSpecificationsRecord>& LaneSpecificationsList::getLaneSpecifications()
{
	return this->laneSpecifications;
}

void LaneSpecificationsList::add(LaneSpecificationsRecord value)
{
	this->laneSpecifications.push_back(value);
}

bool LaneSpecificationsList::empty()
{
	return this->laneSpecifications.empty();
}

void LaneSpecificationsList::clear()
{
	this->laneSpecifications.clear();
}

void LaneSpecificationsList::remove(int index)
{
	this->laneSpecifications.erase(this->laneSpecifications.begin() + index);
}

bool LaneSpecificationsList::set(std::size_t index, LaneSpecificationsRecord value)
{
	this->laneSpecifications[index] = value;
	return true;
}

LaneSpecificationsRecord& LaneSpecificationsList::get(std::size_t index)
{
	return this->laneSpecifications.at(index);
}

std::size_t LaneSpecificationsList::size()
{
	return this->laneSpecifications.size();
}

} // namespace jbt


