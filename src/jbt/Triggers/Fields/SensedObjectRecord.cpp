/**
\file SensedObjectRecord.cpp

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/


#include <system.h>
#include <model/fields.h>
#include "jbt/Triggers/Fields/SensedObjectRecord.h"
// Start of user code for additional includes
// End of user code for additional includes


namespace mechaspin
{

SensedObjectRecord::SensedObjectRecord():
	infoFlag(),
	referenceLine(),
	sensedLine(),
	lateralOffsetError_m(),
	longitudinalOffsetError_m(),
	headingError_rad()
{

	infoFlag.setName("InfoFlag");
	// Nothing

	referenceLine.setName("ReferenceLine");
	// Nothing to Init

	sensedLine.setName("SensedLine");
	// Nothing to Init

	lateralOffsetError_m.setName("LateralOffsetError");
	lateralOffsetError_m.setValue(0);

	longitudinalOffsetError_m.setName("LongitudinalOffsetError");
	longitudinalOffsetError_m.setValue(0);

	headingError_rad.setName("HeadingError");
	headingError_rad.setValue(0);

}

SensedObjectRecord::SensedObjectRecord(const SensedObjectRecord &source)
{

	this->copy(const_cast<SensedObjectRecord&>(source));
}

SensedObjectRecord::~SensedObjectRecord()
{

}


InfoFlagBitField& SensedObjectRecord::getInfoFlag(void)
{
	return this->infoFlag;
}

JbtLineRecord& SensedObjectRecord::getReferenceLine(void)
{
	return this->referenceLine;
}

JbtLineRecord& SensedObjectRecord::getSensedLine(void)
{
	return this->sensedLine;
}

float SensedObjectRecord::getLateralOffsetError_m(void)
{
	return this->lateralOffsetError_m.getValue();
}

bool SensedObjectRecord::setLateralOffsetError_m(float value)
{
	return this->lateralOffsetError_m.setValue(value);
}

float SensedObjectRecord::getLongitudinalOffsetError_m(void)
{
	return this->longitudinalOffsetError_m.getValue();
}

bool SensedObjectRecord::setLongitudinalOffsetError_m(float value)
{
	return this->longitudinalOffsetError_m.setValue(value);
}

float SensedObjectRecord::getHeadingError_rad(void)
{
	return this->headingError_rad.getValue();
}

bool SensedObjectRecord::setHeadingError_rad(float value)
{
	return this->headingError_rad.setValue(value);
}

uint64 SensedObjectRecord::to(system::BufferWriter& dst) const
{
	uint64 byteSize = 0;
	byteSize += dst.pack(infoFlag);
	byteSize += dst.pack(referenceLine);
	byteSize += dst.pack(sensedLine);
	byteSize += dst.pack(lateralOffsetError_m);
	byteSize += dst.pack(longitudinalOffsetError_m);
	byteSize += dst.pack(headingError_rad);
	return byteSize;
}

uint64 SensedObjectRecord::from(system::BufferReader& src)
{
	uint64 byteSize = 0;
	byteSize += src.unpack(infoFlag);
	byteSize += src.unpack(referenceLine);
	byteSize += src.unpack(sensedLine);
	byteSize += src.unpack(lateralOffsetError_m);
	byteSize += src.unpack(longitudinalOffsetError_m);
	byteSize += src.unpack(headingError_rad);
	return byteSize;
}

uint64 SensedObjectRecord::length(void) const
{
	uint64 length = 0;
	length += infoFlag.length(); // infoFlag
	length += referenceLine.length(); // referenceLine
	length += sensedLine.length(); // sensedLine
	length += lateralOffsetError_m.length(); // lateralOffsetError_m
	length += longitudinalOffsetError_m.length(); // longitudinalOffsetError_m
	length += headingError_rad.length(); // headingError_rad
	return length;
}

std::string SensedObjectRecord::toXml(unsigned char ojIndentLevel) const
{
	std::ostringstream prefix;
	for(unsigned char i = 0; i < ojIndentLevel; i++)
	{
		prefix << "\t";
	}

	std::ostringstream oss;
	oss << prefix.str() << "<Record type=\"SensedObjectRecord\">\n";
	oss << infoFlag.toXml(ojIndentLevel+1); // infoFlag
	oss << referenceLine.toXml(ojIndentLevel+1); // referenceLine
	oss << sensedLine.toXml(ojIndentLevel+1); // sensedLine
	oss << lateralOffsetError_m.toXml(ojIndentLevel+1); // lateralOffsetError_m
	oss << longitudinalOffsetError_m.toXml(ojIndentLevel+1); // longitudinalOffsetError_m
	oss << headingError_rad.toXml(ojIndentLevel+1); // headingError_rad
	oss << prefix.str() << "</Record>\n";
	return oss.str();
}



void SensedObjectRecord::copy(SensedObjectRecord& source)
{
	this->setName(source.getName());
	
	this->infoFlag.copy(source.getInfoFlag()); 
 
	this->referenceLine.copy(source.getReferenceLine()); 
 
	this->sensedLine.copy(source.getSensedLine()); 
 
	this->lateralOffsetError_m.setName("LateralOffsetError");
	this->lateralOffsetError_m.setValue(source.getLateralOffsetError_m()); 
 
	this->longitudinalOffsetError_m.setName("LongitudinalOffsetError");
	this->longitudinalOffsetError_m.setValue(source.getLongitudinalOffsetError_m()); 
 
	this->headingError_rad.setName("HeadingError");
	this->headingError_rad.setValue(source.getHeadingError_rad()); 
 
}

// Start of user code for additional members
// End of user code for additional members

} // namespace jbt


