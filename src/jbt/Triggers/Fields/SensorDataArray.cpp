/**
\file SensorDataArray.cpp

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/

#include <system.h>
#include <model/fields.h>
#include "jbt/Triggers/Fields/SensorDataArray.h"

namespace mechaspin
{

SensorDataArray::SensorDataArray() : laserPoint()
{
}

SensorDataArray::~SensorDataArray()
{

}

void SensorDataArray::copy(SensorDataArray& source)
{
	this->setName(source.getName());

	this->laserPoint = source.getLaserPoint();
}

uint64 SensorDataArray::to(system::BufferWriter& dst) const
{
	uint64 result = 0;
	result += dst.pack(static_cast<uint16_t>(this->laserPoint.size())); // Count
	
	for (std::size_t i = 0; i < this->laserPoint.size(); i++) // Elements
	{
		result += dst.pack(this->laserPoint.at(i)); 
	}
	return result;
}

uint64 SensorDataArray::from(system::BufferReader& src)
{
	uint64 result = 0;
	uint16_t count;

	result += src.unpack(count); // Count
	this->laserPoint.clear();
	this->laserPoint.reserve(count);
	
	for (std::size_t i = 0; i < count; i++) // Elements
	{
		LaserPointRecord temp;
		// Nothing to Init
		result += src.unpack(temp);
		this->laserPoint.push_back(temp);
	}
	return result;
}

uint64 SensorDataArray::length(void) const
{
	uint64 result = 0;
	result += sizeof(uint16_t); // Count
	for (std::size_t i = 0; i < this->laserPoint.size(); i++) // Elements
	{
		result += this->laserPoint[i].length(); 
	}
	return result;
}

std::string SensorDataArray::toXml(unsigned char ojIndentLevel) const
{
	std::ostringstream prefix;
	for(unsigned char i = 0; i < ojIndentLevel; i++)
	{
		prefix << "\t";
	}

	std::ostringstream oss;
	oss << prefix.str() << "<List name=\"" << this->name << "\">\n";
	oss << prefix.str() << "\t" << "<count>" << this->laserPoint.size() << "</count>\n";
	
	for(std::size_t i = 0; i < this->laserPoint.size(); i++) // Elements
	{
		oss << this->laserPoint.at(i).toXml(ojIndentLevel + 1);
	}
	oss << prefix.str() << "</List>\n";
	return oss.str();
}


std::vector<LaserPointRecord>& SensorDataArray::getLaserPoint()
{
	return this->laserPoint;
}

void SensorDataArray::add(LaserPointRecord value)
{
	this->laserPoint.push_back(value);
}

bool SensorDataArray::empty()
{
	return this->laserPoint.empty();
}

void SensorDataArray::clear()
{
	this->laserPoint.clear();
}

void SensorDataArray::remove(int index)
{
	this->laserPoint.erase(this->laserPoint.begin() + index);
}

bool SensorDataArray::set(std::size_t index, LaserPointRecord value)
{
	this->laserPoint[index] = value;
	return true;
}

LaserPointRecord& SensorDataArray::get(std::size_t index)
{
	return this->laserPoint.at(index);
}

std::size_t SensorDataArray::size()
{
	return this->laserPoint.size();
}

} // namespace jbt


