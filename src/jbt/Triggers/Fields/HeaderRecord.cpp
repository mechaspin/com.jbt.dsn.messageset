/**
\file HeaderRecord.cpp

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/


#include <system.h>
#include <model/fields.h>
#include "jbt/Triggers/Fields/HeaderRecord.h"
// Start of user code for additional includes
// End of user code for additional includes


namespace mechaspin
{

HeaderRecord::HeaderRecord():
	magicNumber(),
	versionNumber(),
	commandId(),
	dataLength()
{

	magicNumber.setName("MagicNumber");
	magicNumber.setValue(0);

	versionNumber.setName("VersionNumber");
	versionNumber.setValue(0);

	commandId.setName("CommandId");
	commandId.setValue(0);

	dataLength.setName("DataLength");
	dataLength.setValue(0);

}

HeaderRecord::HeaderRecord(const HeaderRecord &source)
{

	this->copy(const_cast<HeaderRecord&>(source));
}

HeaderRecord::~HeaderRecord()
{

}


uint32_t HeaderRecord::getMagicNumber(void)
{
	return this->magicNumber.getValue();
}

bool HeaderRecord::setMagicNumber(uint32_t value)
{
	return this->magicNumber.setValue(value);
}

int8_t HeaderRecord::getVersionNumber(void)
{
	return this->versionNumber.getValue();
}

bool HeaderRecord::setVersionNumber(int8_t value)
{
	return this->versionNumber.setValue(value);
}

uint32_t HeaderRecord::getCommandId(void)
{
	return this->commandId.getValue();
}

bool HeaderRecord::setCommandId(uint32_t value)
{
	return this->commandId.setValue(value);
}

uint32_t HeaderRecord::getDataLength(void)
{
	return this->dataLength.getValue();
}

bool HeaderRecord::setDataLength(uint32_t value)
{
	return this->dataLength.setValue(value);
}

uint64 HeaderRecord::to(system::BufferWriter& dst) const
{
	uint64 byteSize = 0;
	byteSize += dst.pack(magicNumber);
	byteSize += dst.pack(versionNumber);
	byteSize += dst.pack(commandId);
	byteSize += dst.pack(dataLength);
	return byteSize;
}

uint64 HeaderRecord::from(system::BufferReader& src)
{
	uint64 byteSize = 0;
	byteSize += src.unpack(magicNumber);
	byteSize += src.unpack(versionNumber);
	byteSize += src.unpack(commandId);
	byteSize += src.unpack(dataLength);
	return byteSize;
}

uint64 HeaderRecord::length(void) const
{
	uint64 length = 0;
	length += magicNumber.length(); // magicNumber
	length += versionNumber.length(); // versionNumber
	length += commandId.length(); // commandId
	length += dataLength.length(); // dataLength
	return length;
}

std::string HeaderRecord::toXml(unsigned char ojIndentLevel) const
{
	std::ostringstream prefix;
	for(unsigned char i = 0; i < ojIndentLevel; i++)
	{
		prefix << "\t";
	}

	std::ostringstream oss;
	oss << prefix.str() << "<Record type=\"HeaderRecord\">\n";
	oss << magicNumber.toXml(ojIndentLevel+1); // magicNumber
	oss << versionNumber.toXml(ojIndentLevel+1); // versionNumber
	oss << commandId.toXml(ojIndentLevel+1); // commandId
	oss << dataLength.toXml(ojIndentLevel+1); // dataLength
	oss << prefix.str() << "</Record>\n";
	return oss.str();
}



void HeaderRecord::copy(HeaderRecord& source)
{
	this->setName(source.getName());
	
	this->magicNumber.setName("MagicNumber");
	this->magicNumber.setValue(source.getMagicNumber()); 
 
	this->versionNumber.setName("VersionNumber");
	this->versionNumber.setValue(source.getVersionNumber()); 
 
	this->commandId.setName("CommandId");
	this->commandId.setValue(source.getCommandId()); 
 
	this->dataLength.setName("DataLength");
	this->dataLength.setValue(source.getDataLength()); 
 
}

// Start of user code for additional members
// End of user code for additional members

} // namespace mechaspin

