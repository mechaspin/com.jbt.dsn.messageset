
/**
\file InfoFlagBitField.cpp

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/

#include <system.h>
#include <model/fields.h>
#include "jbt/Triggers/Fields/InfoFlagBitField.h"
// Start of user code for additional includes:
// End of user code

namespace mechaspin
{

InfoFlagBitField::InfoFlagBitField() : 
	bit0(),
	bit1(),
	bit2(),
	bit3(),
	bit4(),
	bit5(),
	bit6(),
	bit7(),
	bit8(),
	bit9(),
	bit10(),
	bit11(),
	bit12(),
	bit13(),
	bit14(),
	bit15()
{
	// Initialize BitFieldRange Members
}

InfoFlagBitField::~InfoFlagBitField()
{

}

bool InfoFlagBitField::setBit0(bool value)
{
	this->bit0 = value;
	return true;
}

bool InfoFlagBitField::getBit0(void) const
{
	return this->bit0;
}


bool InfoFlagBitField::setBit1(bool value)
{
	this->bit1 = value;
	return true;
}

bool InfoFlagBitField::getBit1(void) const
{
	return this->bit1;
}


bool InfoFlagBitField::setBit2(bool value)
{
	this->bit2 = value;
	return true;
}

bool InfoFlagBitField::getBit2(void) const
{
	return this->bit2;
}


bool InfoFlagBitField::setBit3(bool value)
{
	this->bit3 = value;
	return true;
}

bool InfoFlagBitField::getBit3(void) const
{
	return this->bit3;
}


bool InfoFlagBitField::setBit4(bool value)
{
	this->bit4 = value;
	return true;
}

bool InfoFlagBitField::getBit4(void) const
{
	return this->bit4;
}


bool InfoFlagBitField::setBit5(bool value)
{
	this->bit5 = value;
	return true;
}

bool InfoFlagBitField::getBit5(void) const
{
	return this->bit5;
}


bool InfoFlagBitField::setBit6(bool value)
{
	this->bit6 = value;
	return true;
}

bool InfoFlagBitField::getBit6(void) const
{
	return this->bit6;
}


bool InfoFlagBitField::setBit7(bool value)
{
	this->bit7 = value;
	return true;
}

bool InfoFlagBitField::getBit7(void) const
{
	return this->bit7;
}


bool InfoFlagBitField::setBit8(bool value)
{
	this->bit8 = value;
	return true;
}

bool InfoFlagBitField::getBit8(void) const
{
	return this->bit8;
}


bool InfoFlagBitField::setBit9(bool value)
{
	this->bit9 = value;
	return true;
}

bool InfoFlagBitField::getBit9(void) const
{
	return this->bit9;
}


bool InfoFlagBitField::setBit10(bool value)
{
	this->bit10 = value;
	return true;
}

bool InfoFlagBitField::getBit10(void) const
{
	return this->bit10;
}


bool InfoFlagBitField::setBit11(bool value)
{
	this->bit11 = value;
	return true;
}

bool InfoFlagBitField::getBit11(void) const
{
	return this->bit11;
}


bool InfoFlagBitField::setBit12(bool value)
{
	this->bit12 = value;
	return true;
}

bool InfoFlagBitField::getBit12(void) const
{
	return this->bit12;
}


bool InfoFlagBitField::setBit13(bool value)
{
	this->bit13 = value;
	return true;
}

bool InfoFlagBitField::getBit13(void) const
{
	return this->bit13;
}


bool InfoFlagBitField::setBit14(bool value)
{
	this->bit14 = value;
	return true;
}

bool InfoFlagBitField::getBit14(void) const
{
	return this->bit14;
}


bool InfoFlagBitField::setBit15(bool value)
{
	this->bit15 = value;
	return true;
}

bool InfoFlagBitField::getBit15(void) const
{
	return this->bit15;
}



uint16_t InfoFlagBitField::getIntegerValue(void) const
{
	uint16_t intValue = 0;

	intValue |= ((this->bit0 & InfoFlagBitField::BIT0_BIT_MASK) << InfoFlagBitField::BIT0_START_BIT);
	intValue |= ((this->bit1 & InfoFlagBitField::BIT1_BIT_MASK) << InfoFlagBitField::BIT1_START_BIT);
	intValue |= ((this->bit2 & InfoFlagBitField::BIT2_BIT_MASK) << InfoFlagBitField::BIT2_START_BIT);
	intValue |= ((this->bit3 & InfoFlagBitField::BIT3_BIT_MASK) << InfoFlagBitField::BIT3_START_BIT);
	intValue |= ((this->bit4 & InfoFlagBitField::BIT4_BIT_MASK) << InfoFlagBitField::BIT4_START_BIT);
	intValue |= ((this->bit5 & InfoFlagBitField::BIT5_BIT_MASK) << InfoFlagBitField::BIT5_START_BIT);
	intValue |= ((this->bit6 & InfoFlagBitField::BIT6_BIT_MASK) << InfoFlagBitField::BIT6_START_BIT);
	intValue |= ((this->bit7 & InfoFlagBitField::BIT7_BIT_MASK) << InfoFlagBitField::BIT7_START_BIT);
	intValue |= ((this->bit8 & InfoFlagBitField::BIT8_BIT_MASK) << InfoFlagBitField::BIT8_START_BIT);
	intValue |= ((this->bit9 & InfoFlagBitField::BIT9_BIT_MASK) << InfoFlagBitField::BIT9_START_BIT);
	intValue |= ((this->bit10 & InfoFlagBitField::BIT10_BIT_MASK) << InfoFlagBitField::BIT10_START_BIT);
	intValue |= ((this->bit11 & InfoFlagBitField::BIT11_BIT_MASK) << InfoFlagBitField::BIT11_START_BIT);
	intValue |= ((this->bit12 & InfoFlagBitField::BIT12_BIT_MASK) << InfoFlagBitField::BIT12_START_BIT);
	intValue |= ((this->bit13 & InfoFlagBitField::BIT13_BIT_MASK) << InfoFlagBitField::BIT13_START_BIT);
	intValue |= ((this->bit14 & InfoFlagBitField::BIT14_BIT_MASK) << InfoFlagBitField::BIT14_START_BIT);
	intValue |= ((this->bit15 & InfoFlagBitField::BIT15_BIT_MASK) << InfoFlagBitField::BIT15_START_BIT);

	return intValue;
}

void InfoFlagBitField::setIntegerValue(uint16_t intValue)
{
	this->bit0 = static_cast<bool>((intValue >> (InfoFlagBitField::BIT0_START_BIT)) & InfoFlagBitField::BIT0_BIT_MASK);
	this->bit1 = static_cast<bool>((intValue >> (InfoFlagBitField::BIT1_START_BIT)) & InfoFlagBitField::BIT1_BIT_MASK);
	this->bit2 = static_cast<bool>((intValue >> (InfoFlagBitField::BIT2_START_BIT)) & InfoFlagBitField::BIT2_BIT_MASK);
	this->bit3 = static_cast<bool>((intValue >> (InfoFlagBitField::BIT3_START_BIT)) & InfoFlagBitField::BIT3_BIT_MASK);
	this->bit4 = static_cast<bool>((intValue >> (InfoFlagBitField::BIT4_START_BIT)) & InfoFlagBitField::BIT4_BIT_MASK);
	this->bit5 = static_cast<bool>((intValue >> (InfoFlagBitField::BIT5_START_BIT)) & InfoFlagBitField::BIT5_BIT_MASK);
	this->bit6 = static_cast<bool>((intValue >> (InfoFlagBitField::BIT6_START_BIT)) & InfoFlagBitField::BIT6_BIT_MASK);
	this->bit7 = static_cast<bool>((intValue >> (InfoFlagBitField::BIT7_START_BIT)) & InfoFlagBitField::BIT7_BIT_MASK);
	this->bit8 = static_cast<bool>((intValue >> (InfoFlagBitField::BIT8_START_BIT)) & InfoFlagBitField::BIT8_BIT_MASK);
	this->bit9 = static_cast<bool>((intValue >> (InfoFlagBitField::BIT9_START_BIT)) & InfoFlagBitField::BIT9_BIT_MASK);
	this->bit10 = static_cast<bool>((intValue >> (InfoFlagBitField::BIT10_START_BIT)) & InfoFlagBitField::BIT10_BIT_MASK);
	this->bit11 = static_cast<bool>((intValue >> (InfoFlagBitField::BIT11_START_BIT)) & InfoFlagBitField::BIT11_BIT_MASK);
	this->bit12 = static_cast<bool>((intValue >> (InfoFlagBitField::BIT12_START_BIT)) & InfoFlagBitField::BIT12_BIT_MASK);
	this->bit13 = static_cast<bool>((intValue >> (InfoFlagBitField::BIT13_START_BIT)) & InfoFlagBitField::BIT13_BIT_MASK);
	this->bit14 = static_cast<bool>((intValue >> (InfoFlagBitField::BIT14_START_BIT)) & InfoFlagBitField::BIT14_BIT_MASK);
	this->bit15 = static_cast<bool>((intValue >> (InfoFlagBitField::BIT15_START_BIT)) & InfoFlagBitField::BIT15_BIT_MASK);
}

uint64 InfoFlagBitField::to(system::BufferWriter& dst) const
{
	uint16_t intValue = getIntegerValue();
	return dst.pack(intValue);
}

uint64 InfoFlagBitField::from(system::BufferReader& src)
{
	uint64 byteSize = 0;
	uint16_t intValue = 0;
	byteSize = src.unpack(intValue);

	this->setIntegerValue(intValue);

	return byteSize;
}

uint64 InfoFlagBitField::length(void) const
{
	return sizeof(uint16_t);
}

void InfoFlagBitField::copy(InfoFlagBitField& source)
{
	this->setName(source.getName());

	setBit0(source.getBit0());
	setBit1(source.getBit1());
	setBit2(source.getBit2());
	setBit3(source.getBit3());
	setBit4(source.getBit4());
	setBit5(source.getBit5());
	setBit6(source.getBit6());
	setBit7(source.getBit7());
	setBit8(source.getBit8());
	setBit9(source.getBit9());
	setBit10(source.getBit10());
	setBit11(source.getBit11());
	setBit12(source.getBit12());
	setBit13(source.getBit13());
	setBit14(source.getBit14());
	setBit15(source.getBit15());

}

std::string InfoFlagBitField::toXml(unsigned char ojIndentLevel) const
{
	uint16_t intValue = getIntegerValue();

	std::ostringstream prefix;
	for(unsigned char i = 0; i < ojIndentLevel; i++)
	{
		prefix << "\t";
	}

	std::ostringstream oss;
	oss << prefix.str() << "<BitField name=\"" << this->name << "\">\n";
	oss << prefix.str() << "\t" << "<intValue>" << intValue << "</intValue>\n";
	oss << prefix.str() << "\t" << "<fields>\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"bit0\" value=\"" << getBit0() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"bit1\" value=\"" << getBit1() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"bit2\" value=\"" << getBit2() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"bit3\" value=\"" << getBit3() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"bit4\" value=\"" << getBit4() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"bit5\" value=\"" << getBit5() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"bit6\" value=\"" << getBit6() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"bit7\" value=\"" << getBit7() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"bit8\" value=\"" << getBit8() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"bit9\" value=\"" << getBit9() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"bit10\" value=\"" << getBit10() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"bit11\" value=\"" << getBit11() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"bit12\" value=\"" << getBit12() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"bit13\" value=\"" << getBit13() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"bit14\" value=\"" << getBit14() << "\" />\n";
	oss << prefix.str() << "\t" << "<BitFieldEnumeration name=\"bit15\" value=\"" << getBit15() << "\" />\n";
	oss << prefix.str() << "\t" << "</fields>\n";
	oss << prefix.str() << "</BitField>\n";
	return oss.str();
}

// Start of user code for additional methods:
// End of user code

} // namespace jbt


