
/**
\file InitialLaneCompromiseEnumeration.cpp

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/

#include <system.h>
#include <model/fields.h>
#include "jbt/Triggers/Fields/InitialLaneCompromiseEnumeration.h"

namespace mechaspin
{

InitialLaneCompromiseEnumeration::InitialLaneCompromiseEnumeration() :
	value(static_cast<InitialLaneCompromiseEnumeration::InitialLaneCompromiseEnum>(0))
{
}

InitialLaneCompromiseEnumeration::InitialLaneCompromiseEnumeration(InitialLaneCompromiseEnumeration::InitialLaneCompromiseEnum value) :
	value(value)
{
}

InitialLaneCompromiseEnumeration::~InitialLaneCompromiseEnumeration()
{

}

InitialLaneCompromiseEnumeration::InitialLaneCompromiseEnum InitialLaneCompromiseEnumeration::getValue(void) const
{
	return this->value;
}

void InitialLaneCompromiseEnumeration::setValue(InitialLaneCompromiseEnumeration::InitialLaneCompromiseEnum value)
{
	this->value = value;
}

uint64 InitialLaneCompromiseEnumeration::to(system::BufferWriter& dst) const
{
	return dst.pack(static_cast<uint8_t>(this->getValue()));
}

uint64 InitialLaneCompromiseEnumeration::from(system::BufferReader& src)
{
	uint64 sizeBytes = 0;
	uint8_t intValue;
	sizeBytes = src.unpack(intValue);
	this->setValue(static_cast<InitialLaneCompromiseEnumeration::InitialLaneCompromiseEnum>(intValue));
	return sizeBytes;
}

uint64 InitialLaneCompromiseEnumeration::length(void) const
{
	return sizeof(uint8_t);
}

std::string InitialLaneCompromiseEnumeration::toXml(unsigned char ojIndentLevel) const
{
	std::ostringstream prefix;
	for(unsigned char i = 0; i < ojIndentLevel; i++)
	{
		prefix << "\t";
	}

	std::ostringstream oss;
	oss << prefix.str() << "<Enumeration name=\"InitialLaneCompromise\" intValue=\"" << this->getValue() << "\" strValue=\"" << this->toString() << "\" />\n";
	return oss.str();
}

std::string InitialLaneCompromiseEnumeration::toString(InitialLaneCompromiseEnumeration::InitialLaneCompromiseEnum value) 
{
	switch(value)
	{
		case InitialLaneCompromiseEnumeration::DO_NOT_USE:
			return "DO_NOT_USE";
		case InitialLaneCompromiseEnumeration::USE:
			return "USE";
		default:
			return "Unknown";
	}
}

std::string InitialLaneCompromiseEnumeration::toString() const
{
	return toString(this->value);
}

} // namespace jbt


