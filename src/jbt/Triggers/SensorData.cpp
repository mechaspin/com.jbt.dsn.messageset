
/**
\file SensorData.cpp

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/

#include "jbt/Triggers/SensorData.h"
// Start of user code for additional includes
// End of user code for additional includes

namespace mechaspin
{


SensorData::SensorData() : 
	model::Message(),
	magicNumber(),
	versionNumber(),
	commandId(),
	dataLength(),
	timestamp_us(),
	laserId(),
	sensorData()
{
	this->id = SensorData::ID; // Initialize id member


	magicNumber.setName("MagicNumber");
	magicNumber.setValue(0x204E5344);

	versionNumber.setName("VersionNumber");
	versionNumber.setValue(1);

	commandId.setName("CommandId");
	commandId.setValue(0);

	dataLength.setName("DataLength");
	dataLength.setValue(0);

	timestamp_us.setName("Timestamp_us");
	timestamp_us.setValue(0);

	laserId.setName("LaserId");
	// Nothing to init

	sensorData.setName("SensorData");
	// Nothing to Init

}

SensorData::SensorData(model::Message *message) :
	model::Message(message),
	magicNumber(),
	versionNumber(),
	commandId(),
	dataLength(),
	timestamp_us(),
	laserId(),
	sensorData()
{
	this->id = SensorData::ID; // Initialize id member


	magicNumber.setName("MagicNumber");
	magicNumber.setValue(0x204E5344);

	versionNumber.setName("VersionNumber");
	versionNumber.setValue(1);

	commandId.setName("CommandId");
	commandId.setValue(0);

	dataLength.setName("DataLength");
	dataLength.setValue(0);

	timestamp_us.setName("Timestamp_us");
	timestamp_us.setValue(0);

	laserId.setName("LaserId");
	// Nothing to init

	sensorData.setName("SensorData");
	// Nothing to Init


	system::Buffer* payloadBuffer = dynamic_cast<system::Buffer*>(message->getPayload());
	if (payloadBuffer != NULL)
	{
	    system::BufferReader& reader = payloadBuffer->getReader();
	    reader.reset(); 
		this->from(reader);
		// payloadBuffer->reset();
	}
}

SensorData::~SensorData()
{

}


uint32_t SensorData::getMagicNumber(void)
{
	return this->magicNumber.getValue();
}

bool SensorData::setMagicNumber(uint32_t value)
{
	return this->magicNumber.setValue(value);
}

int8_t SensorData::getVersionNumber(void)
{
	return this->versionNumber.getValue();
}

bool SensorData::setVersionNumber(int8_t value)
{
	return this->versionNumber.setValue(value);
}

uint32_t SensorData::getCommandId(void)
{
	return this->commandId.getValue();
}

bool SensorData::setCommandId(uint32_t value)
{
	return this->commandId.setValue(value);
}

uint32_t SensorData::getDataLength(void)
{
	return this->dataLength.getValue();
}

bool SensorData::setDataLength(uint32_t value)
{
	return this->dataLength.setValue(value);
}

uint64_t SensorData::getTimestamp_us(void)
{
	return this->timestamp_us.getValue();
}

bool SensorData::setTimestamp_us(uint64_t value)
{
	return this->timestamp_us.setValue(value);
}

LaserIdEnumeration::LaserIdEnum SensorData::getLaserId(void)
{
	return this->laserId.getValue();
}

bool SensorData::setLaserId(LaserIdEnumeration::LaserIdEnum value)
{
	this->laserId.setValue(value);
	return true;
}

std::string SensorData::getLaserIdToString(void)
{
	return this->laserId.toString();
}

SensorDataArray& SensorData::getSensorData(void)
{
	return this->sensorData;
}

uint64 SensorData::to(system::BufferWriter& dst) const
{
	uint64 byteSize = 0;
	byteSize += dst.pack(magicNumber);
	byteSize += dst.pack(versionNumber);
	byteSize += dst.pack(this->id);
	byteSize += dst.pack(static_cast<uint32>(this->length()-13));
	byteSize += dst.pack(timestamp_us);
	byteSize += dst.pack(laserId);
	byteSize += dst.pack(sensorData);
	return byteSize;
}

uint64 SensorData::from(system::BufferReader& src)
{
	uint64 byteSize = 0;
	byteSize += src.unpack(magicNumber);
	byteSize += src.unpack(versionNumber);
	byteSize += src.unpack(this->id);
	byteSize += src.unpack(dataLength);
	byteSize += src.unpack(timestamp_us);
	byteSize += src.unpack(laserId);
	byteSize += src.unpack(sensorData);
	return byteSize;
}

uint64 SensorData::length(void) const
{
	uint64 length = 0;
	length += magicNumber.length(); // magicNumber
	length += versionNumber.length(); // versionNumber
	length += commandId.length(); // commandId
	length += dataLength.length(); // dataLength
	length += timestamp_us.length(); // timestamp_us
	length += laserId.length(); // laserId
	length += sensorData.length(); // sensorData
	return length;
}

std::string SensorData::toString() const
{
	return std::string("SensorData [0xAADD]");
}

std::ostream& operator<<(std::ostream& output, const SensorData& object)
{
    output << object.toString();
    return output;
}

std::ostream& operator<<(std::ostream& output, const SensorData* object)
{
    output << object->toString();
    return output;
}

std::string SensorData::toXml(unsigned char ojIndentLevel) const
{
	std::ostringstream prefix;
	for(unsigned char i = 0; i < ojIndentLevel; i++)
	{
		prefix << "\t";
	}

	std::ostringstream oss;
	oss << prefix.str() << "<Message name=\"SensorData\"";
	oss << " id=\"0xAADD\" >\n";
	oss << magicNumber.toXml(ojIndentLevel+1); // magicNumber
	oss << versionNumber.toXml(ojIndentLevel+1); // versionNumber
	oss << commandId.toXml(ojIndentLevel+1); // commandId
	oss << dataLength.toXml(ojIndentLevel+1); // dataLength
	oss << timestamp_us.toXml(ojIndentLevel+1); // timestamp_us
	oss << laserId.toXml(ojIndentLevel+1); // laserId
	oss << sensorData.toXml(ojIndentLevel+1); // sensorData
	oss << prefix.str() << "</Message>\n";
	return oss.str();
}


// Start of user code for additional methods
// End of user code for additional methods

} // namespace jbt



