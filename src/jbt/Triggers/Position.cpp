
/**
\file Position.cpp

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/

#include "jbt/Triggers/Position.h"
// Start of user code for additional includes
// End of user code for additional includes

namespace mechaspin
{


Position::Position() : 
	model::Message(),
	magicNumber(),
	versionNumber(),
	commandId(),
	dataLength(),
	timestamp_us(),
	dsnActive(),
	deadReckoningX_m(),
	deadReckoningY_m(),
	heading_rad(),
	navX_m(),
	navY_m(),
	navHeading_rad()
{
	this->id = Position::ID; // Initialize id member


	magicNumber.setName("MagicNumber");
	magicNumber.setValue(0x204E5344);

	versionNumber.setName("VersionNumber");
	versionNumber.setValue(1);

	commandId.setName("CommandId");
	commandId.setValue(0);

	dataLength.setName("DataLength");
	dataLength.setValue(0);

	timestamp_us.setName("Timestamp_us");
	timestamp_us.setValue(0);

	dsnActive.setName("DsnActive");
	// Nothing to init

	deadReckoningX_m.setName("DeadReckoningX");
	deadReckoningX_m.setValue(0);

	deadReckoningY_m.setName("DeadReckoningY");
	deadReckoningY_m.setValue(0);

	heading_rad.setName("Heading");
	heading_rad.setValue(0);

	navX_m.setName("NavX");
	navX_m.setValue(0);

	navY_m.setName("NavY");
	navY_m.setValue(0);

	navHeading_rad.setName("NavHeading");
	navHeading_rad.setValue(0);

}

Position::Position(model::Message *message) :
	model::Message(message),
	magicNumber(),
	versionNumber(),
	commandId(),
	dataLength(),
	timestamp_us(),
	dsnActive(),
	deadReckoningX_m(),
	deadReckoningY_m(),
	heading_rad(),
	navX_m(),
	navY_m(),
	navHeading_rad()
{
	this->id = Position::ID; // Initialize id member


	magicNumber.setName("MagicNumber");
	magicNumber.setValue(0x204E5344);

	versionNumber.setName("VersionNumber");
	versionNumber.setValue(1);

	commandId.setName("CommandId");
	commandId.setValue(0);

	dataLength.setName("DataLength");
	dataLength.setValue(0);

	timestamp_us.setName("Timestamp_us");
	timestamp_us.setValue(0);

	dsnActive.setName("DsnActive");
	// Nothing to init

	deadReckoningX_m.setName("DeadReckoningX");
	deadReckoningX_m.setValue(0);

	deadReckoningY_m.setName("DeadReckoningY");
	deadReckoningY_m.setValue(0);

	heading_rad.setName("Heading");
	heading_rad.setValue(0);

	navX_m.setName("NavX");
	navX_m.setValue(0);

	navY_m.setName("NavY");
	navY_m.setValue(0);

	navHeading_rad.setName("NavHeading");
	navHeading_rad.setValue(0);


	system::Buffer* payloadBuffer = dynamic_cast<system::Buffer*>(message->getPayload());
	if (payloadBuffer != NULL)
	{
	    system::BufferReader& reader = payloadBuffer->getReader();
	    reader.reset(); 
		this->from(reader);
		// payloadBuffer->reset();
	}
}

Position::~Position()
{

}


uint32_t Position::getMagicNumber(void)
{
	return this->magicNumber.getValue();
}

bool Position::setMagicNumber(uint32_t value)
{
	return this->magicNumber.setValue(value);
}

int8_t Position::getVersionNumber(void)
{
	return this->versionNumber.getValue();
}

bool Position::setVersionNumber(int8_t value)
{
	return this->versionNumber.setValue(value);
}

uint32_t Position::getCommandId(void)
{
	return this->commandId.getValue();
}

bool Position::setCommandId(uint32_t value)
{
	return this->commandId.setValue(value);
}

uint32_t Position::getDataLength(void)
{
	return this->dataLength.getValue();
}

bool Position::setDataLength(uint32_t value)
{
	return this->dataLength.setValue(value);
}

uint64_t Position::getTimestamp_us(void)
{
	return this->timestamp_us.getValue();
}

bool Position::setTimestamp_us(uint64_t value)
{
	return this->timestamp_us.setValue(value);
}

DsnActiveEnumeration::DsnActiveEnum Position::getDsnActive(void)
{
	return this->dsnActive.getValue();
}

bool Position::setDsnActive(DsnActiveEnumeration::DsnActiveEnum value)
{
	this->dsnActive.setValue(value);
	return true;
}

std::string Position::getDsnActiveToString(void)
{
	return this->dsnActive.toString();
}

float Position::getDeadReckoningX_m(void)
{
	return this->deadReckoningX_m.getValue();
}

bool Position::setDeadReckoningX_m(float value)
{
	return this->deadReckoningX_m.setValue(value);
}

float Position::getDeadReckoningY_m(void)
{
	return this->deadReckoningY_m.getValue();
}

bool Position::setDeadReckoningY_m(float value)
{
	return this->deadReckoningY_m.setValue(value);
}

float Position::getHeading_rad(void)
{
	return this->heading_rad.getValue();
}

bool Position::setHeading_rad(float value)
{
	return this->heading_rad.setValue(value);
}

float Position::getNavX_m(void)
{
	return this->navX_m.getValue();
}

bool Position::setNavX_m(float value)
{
	return this->navX_m.setValue(value);
}

float Position::getNavY_m(void)
{
	return this->navY_m.getValue();
}

bool Position::setNavY_m(float value)
{
	return this->navY_m.setValue(value);
}

float Position::getNavHeading_rad(void)
{
	return this->navHeading_rad.getValue();
}

bool Position::setNavHeading_rad(float value)
{
	return this->navHeading_rad.setValue(value);
}

uint64 Position::to(system::BufferWriter& dst) const
{
	uint64 byteSize = 0;
	byteSize += dst.pack(magicNumber);
	byteSize += dst.pack(versionNumber);
	byteSize += dst.pack(this->id);
	byteSize += dst.pack(static_cast<uint32>(this->length()-13));
	byteSize += dst.pack(timestamp_us);
	byteSize += dst.pack(dsnActive);
	byteSize += dst.pack(deadReckoningX_m);
	byteSize += dst.pack(deadReckoningY_m);
	byteSize += dst.pack(heading_rad);
	byteSize += dst.pack(navX_m);
	byteSize += dst.pack(navY_m);
	byteSize += dst.pack(navHeading_rad);

	return byteSize;
}

uint64 Position::from(system::BufferReader& src)
{
	uint64 byteSize = 0;
	byteSize += src.unpack(magicNumber);
	byteSize += src.unpack(versionNumber);
	byteSize += src.unpack(this->id);
	byteSize += src.unpack(dataLength);
	byteSize += src.unpack(timestamp_us);
	byteSize += src.unpack(dsnActive);
	byteSize += src.unpack(deadReckoningX_m);
	byteSize += src.unpack(deadReckoningY_m);
	byteSize += src.unpack(heading_rad);
	byteSize += src.unpack(navX_m);
	byteSize += src.unpack(navY_m);
	byteSize += src.unpack(navHeading_rad);
	return byteSize;
}

uint64 Position::length(void) const
{
	uint64 length = 0;
	length += magicNumber.length(); // magicNumber
	length += versionNumber.length(); // versionNumber
	length += commandId.length(); // commandId
	length += dataLength.length(); // dataLength
	length += timestamp_us.length(); // timestamp_us
	length += dsnActive.length(); // dsnActive
	length += deadReckoningX_m.length(); // deadReckoningX_m
	length += deadReckoningY_m.length(); // deadReckoningY_m
	length += heading_rad.length(); // heading_rad
	length += navX_m.length(); // navX_m
	length += navY_m.length(); // navY_m
	length += navHeading_rad.length(); // navHeading_rad
	return length;
}

std::string Position::toString() const
{
	return std::string("Position [0xAAAA]");
}

std::ostream& operator<<(std::ostream& output, const Position& object)
{
    output << object.toString();
    return output;
}

std::ostream& operator<<(std::ostream& output, const Position* object)
{
    output << object->toString();
    return output;
}

std::string Position::toXml(unsigned char ojIndentLevel) const
{
	std::ostringstream prefix;
	for(unsigned char i = 0; i < ojIndentLevel; i++)
	{
		prefix << "\t";
	}

	std::ostringstream oss;
	oss << prefix.str() << "<Message name=\"Position\"";
	oss << " id=\"0xAAAA\" >\n";
	oss << magicNumber.toXml(ojIndentLevel+1); // magicNumber
	oss << versionNumber.toXml(ojIndentLevel+1); // versionNumber
	oss << commandId.toXml(ojIndentLevel+1); // commandId
	oss << dataLength.toXml(ojIndentLevel+1); // dataLength
	oss << timestamp_us.toXml(ojIndentLevel+1); // timestamp_us
	oss << dsnActive.toXml(ojIndentLevel+1); // dsnActive
	oss << deadReckoningX_m.toXml(ojIndentLevel+1); // deadReckoningX_m
	oss << deadReckoningY_m.toXml(ojIndentLevel+1); // deadReckoningY_m
	oss << heading_rad.toXml(ojIndentLevel+1); // heading_rad
	oss << navX_m.toXml(ojIndentLevel+1); // navX_m
	oss << navY_m.toXml(ojIndentLevel+1); // navY_m
	oss << navHeading_rad.toXml(ojIndentLevel+1); // navHeading_rad
	oss << prefix.str() << "</Message>\n";
	return oss.str();
}


// Start of user code for additional methods
// End of user code for additional methods

} // namespace jbt



