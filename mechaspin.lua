local packagePath = package.path
package.path = '../org.openjaus.sdk.cpp/?.lua;' .. packagePath
local ojUtil = require("buildtools.openjaus-util")
local oj = require("buildtools.openjaus")

local solution = {}
local projects = {}

solution.Name = "com.jbt.dsn.messageSet"

projects.msgset = oj.createLibrary(
  "jbt-dsn-msgset",
  { "src/**.cpp", "include/**.h" },
  {"openjaus-system"},
  {"./", "./org.openjaus.system.cpp"})

projects.clientExample = oj.createConsoleApp(
  "ClientExample",
  { "apps/dsn-ClientExample.cpp" },
  { "jbt-dsn-msgset", "openjaus-system"},
  { "./", "./org.openjaus.system.cpp"})

projects.serverExample = oj.createConsoleApp(
  "ServerExample",
  { "apps/dsn-ServerExample.cpp" },
  { "jbt-dsn-msgset", "openjaus-system"},
  { "./","./org.openjaus.system.cpp"})


solution.Projects = projects

return solution