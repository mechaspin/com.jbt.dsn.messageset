
/**
dsn-ServerExample.cpp

Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

Software History
- [2018-01] - Version 1.0

  Generated using MechaSpin's Message Generator.
  Source Model: jbt-dsn.ojss
*/

#include <signal.h>
#include <iostream>
#include <mechaspin.h>
#include <MsgSocket.h>

#include <jbt\Triggers\Position.h>
#include <jbt\Triggers\SensorData.h>
#include <jbt\Triggers\PalletLane.h>
#include <jbt\Triggers\ErrorCorrections.h>
#include <jbt\Triggers\Configuration.h>
#include <jbt\Triggers\Analysis.h>
#include <jbt\Triggers\Debug.h>

using namespace mechaspin;

void signalHandler(int n);
void printMenu();
void mainLoop();
void processMessage(void *);

static bool mainRunning = false;
static MsgSocket *msgSocket;
static system::InetAddress serverAddress;
static uint16 serverPort;

// Start of user code for Default UDP info
#define DEFAULT_SERVER_IP "127.0.0.1"
#define DEFAULT_SERVER_PORT 5250
// End of user code for Default UDP info

// Message Handlers
bool processPosition(const model::Message *msg);
bool processSensorData(const model::Message *msg);
bool processPalletLane(const model::Message *msg);
bool processErrorCorrections(const model::Message *msg);
bool processConfiguration(const model::Message *msg);
bool processAnalysis(const model::Message *msg);
bool processDebug(const model::Message *msg);

int main()
{
	try
	{
		system::Application::setTerminalMode();
		std::cout << "dsn Server Example" << std::endl;
		printMenu();

		// Read Port from config file
		serverPort = system::Application::setting<uint16>("ServerPort", DEFAULT_SERVER_PORT);

		// Create socket
		msgSocket = new MsgSocket(mechaspin::ANY_INTERFACE, serverPort);
		msgSocket->addMessageCallback(&processMessage);

		// Setup our signal handlers
		signal(SIGTERM, signalHandler);
		signal(SIGINT, signalHandler);

		mainRunning = true;

		// Start the MsgSocket's threads
		msgSocket->run();

		// Main Loop
		mainLoop();

		std::cout << "Shutting Down...\n";
		msgSocket->stop();

		return 0;
	}
	catch (openjaus::system::Exception &expn)
	{
		std::cout << expn.toString() << std::endl;
	}

	return -1;
}

void signalHandler(int n)
{
	// Got Signal, shutdown gracefully
	std::cout << "Got Signal, shutdown gracefully" << std::endl;
	mainRunning = false;
}

void printMenu()
{
	std::cout << "Menu:" << std::endl;
	std::cout << "? - Output Menu" << std::endl;
	std::cout << "ESC - Exit Component" << std::endl;
}

void mainLoop()
{
	unsigned char choice = 0;

	while (mainRunning)
	{
		choice = system::Application::getChar();
		switch (choice)
		{
			case system::Application::ASCII_ESC:
				mainRunning = false;
				break;
			case '?':
				printMenu();
				break;
		}
	}
}

void processMessage(void *)
{
	model::Message *msg = msgSocket->getMessage();
	switch (msg->getId())
	{
		case Position::ID:
		{
			processPosition(msg);
			break;
		}
		case SensorData::ID:
		{
			processSensorData(msg);
			break;
		}
		case PalletLane::ID:
		{
			processPalletLane(msg);
			break;
		}
		case ErrorCorrections::ID:
		{
			processErrorCorrections(msg);
			break;
		}
		case Configuration::ID:
		{
			processConfiguration(msg);
			break;
		}
		case Analysis::ID:
		{
			processAnalysis(msg);
			break;
		}
		case Debug::ID:
		{
			processDebug(msg);
			break;
		}
	}

	delete msg->getPayload();
	delete msg;
}


bool processPosition(const model::Message *msg)
{
	Position *position = new Position(); 
	system::Buffer* buffer = dynamic_cast<system::Buffer*>(msg->getPayload());
	system::BufferReader& reader = buffer->getReader();
	reader.reset();
	position->from(reader); // Sets the ID and payload buffer

	std::cout << "Received Msg: Position... Echoing back" << std::endl;

	position->setDestination(msg->getSource());
	position->setDestinationPort(msg->getSourcePort());
	msgSocket->sendMessage(position);

	return true;
}

bool processSensorData(const model::Message *msg)
{
	SensorData *sensorData = new SensorData(); 
	system::Buffer* buffer = dynamic_cast<system::Buffer*>(msg->getPayload());
	system::BufferReader& reader = buffer->getReader();
	reader.reset();
	sensorData->from(reader); // Sets the ID and payload buffer

	std::cout << "Data Length: " << sensorData->getDataLength() << std::endl;
	std::cout << "Count: " << sensorData->getSensorData().size() << std::endl;

	delete sensorData;

	return true;
}

bool processPalletLane(const model::Message *msg)
{
	PalletLane *palletLane = new PalletLane(); 
	system::Buffer* buffer = dynamic_cast<system::Buffer*>(msg->getPayload());
	system::BufferReader& reader = buffer->getReader();
	reader.reset();
	palletLane->from(reader); // Sets the ID and payload buffer

	std::cout << "Received Msg: PalletLane... Echoing back" << std::endl;

	palletLane->setDestination(msg->getSource());
	palletLane->setDestinationPort(msg->getSourcePort());
	msgSocket->sendMessage(palletLane);

	return true;
}

bool processErrorCorrections(const model::Message *msg)
{
	ErrorCorrections *errorCorrections = new ErrorCorrections(); 
	system::Buffer* buffer = dynamic_cast<system::Buffer*>(msg->getPayload());
	system::BufferReader& reader = buffer->getReader();
	reader.reset();
	errorCorrections->from(reader); // Sets the ID and payload buffer

	std::cout << "Received Msg: ErrorCorrections... Echoing back" << std::endl;

	StatusBitField &status = errorCorrections->getStatus();
	status.setInitialCompromiseErrorTooLarge(false);
	status.setInsufficientData(false);
	status.setLaneConfidenceTooLow(false);
	status.setLaneFitHeadingError(false);
	status.setLaneFitLateralError(false);
	status.setNoHostInformation(false);
	status.setNotConfiguredError(false);
	status.setPathHeadingErrorTooLarge(false);
	status.setPathLateralErrorTooLarge(false);


	errorCorrections->setDestination(msg->getSource());
	errorCorrections->setDestinationPort(msg->getSourcePort());
	msgSocket->sendMessage(errorCorrections);

	return true;
}

bool processConfiguration(const model::Message *msg)
{
	Configuration *configuration = new Configuration(); 
	system::Buffer* buffer = dynamic_cast<system::Buffer*>(msg->getPayload());
	system::BufferReader& reader = buffer->getReader();
	reader.reset();
	configuration->from(reader); // Sets the ID and payload buffer

	std::cout << "Received Msg: Configuration... Echoing back" << std::endl;
	//std::cout << configuration->toXml() << std::endl;

	configuration->setDestination(msg->getSource());
	configuration->setDestinationPort(msg->getSourcePort());
	msgSocket->sendMessage(configuration);

	return true;
}

bool processAnalysis(const model::Message *msg)
{
	Analysis *analysisMsg = new Analysis(const_cast<model::Message *>(msg));

	std::cout << "Received Msg: Analysis..." << std::endl;
	std::cout << analysisMsg->toXml() << std::endl;

	delete analysisMsg;
	return true;
}

bool processDebug(const model::Message *msg)
{
	Debug *debugMsg = new Debug(const_cast<model::Message *>(msg));

	std::cout << "Received Msg: Debug..." << std::endl;
	std::cout << debugMsg->toXml() << std::endl;

	delete debugMsg;
	return true;
}



