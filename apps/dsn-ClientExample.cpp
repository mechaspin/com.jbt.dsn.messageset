
/**
dsn-ClientExample.cpp

Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

Software History
- [2018-01] - Version 1.0

  Generated using MechaSpin's Message Generator.
  Source Model: jbt-dsn.ojss
*/

#include <signal.h>
#include <iostream>
#include <mechaspin.h>
#include <MsgSocket.h>

#include <jbt\Triggers\Position.h>
#include <jbt\Triggers\SensorData.h>
#include <jbt\Triggers\PalletLane.h>
#include <jbt\Triggers\ErrorCorrections.h>
#include <jbt\Triggers\Configuration.h>
#include <jbt\Triggers\Analysis.h>
#include <jbt\Triggers\Debug.h>

using namespace mechaspin;

void signalHandler(int n);
void printMenu();
void mainLoop();
void processMessage(void *);

static bool mainRunning = false;
static MsgSocket *msgSocket;
static system::InetAddress serverAddress;
static uint16 serverPort;

// Start of user code for Default UDP info
#define DEFAULT_SERVER_IP "127.0.0.1"
#define DEFAULT_SERVER_PORT 5250
#define DEFAULT_CLIENT_IP "127.0.0.1"
#define DEFAULT_CLIENT_PORT system::InetAddress::ANY_PORT
// End of user code for Default UDP info

// Message Handlers
bool sendPosition();
bool processPosition(const model::Message *msg);
bool sendSensorData();
bool processSensorData(const model::Message *msg);
bool sendPalletLane();
bool processPalletLane(const model::Message *msg);
bool sendErrorCorrections();
bool processErrorCorrections(const model::Message *msg);
bool sendConfiguration();
bool processConfiguration(const model::Message *msg);
bool sendAnalysis();
bool sendDebug();

int main()
{
	try
	{
		system::Application::setTerminalMode();
		std::cout << "dsn Client Example" << std::endl;
		printMenu();

		// Read IP Address and Port from config file
		uint16 clientPort = system::Application::setting<uint16>("ClientPort", DEFAULT_CLIENT_PORT);

		std::string serverAddressStr = system::Application::setting<std::string>("ServerIpAddress", DEFAULT_SERVER_IP);
		serverAddress = system::InetAddress::getByName(serverAddressStr);
		serverPort = system::Application::setting<uint16>("ServerPort", DEFAULT_SERVER_PORT);

		// Create socket
		msgSocket = new MsgSocket(mechaspin::ANY_INTERFACE, clientPort);
		msgSocket->addMessageCallback(&processMessage);

		// Setup our signal handlers
		signal(SIGTERM, signalHandler);
		signal(SIGINT, signalHandler);

		mainRunning = true;

		// Start the MsgSocket's threads
		msgSocket->run();

		// Main Loop
		mainLoop();

		std::cout << "Shutting Down...\n";
		msgSocket->stop();

		return 0;
	}
	catch (openjaus::system::Exception &expn)
	{
		std::cout << expn.toString() << std::endl;
	}

	return -1;
}

void signalHandler(int n)
{
	// Got Signal, shutdown gracefully
	std::cout << "Got Signal, shutdown gracefully" << std::endl;
	mainRunning = false;
}

void printMenu()
{
	std::cout << "Menu:" << std::endl;
	std::cout << "1 - Send Position" << std::endl;
	std::cout << "2 - Send SensorData" << std::endl;
	std::cout << "3 - Send PalletLane" << std::endl;
	std::cout << "4 - Send ErrorCorrections" << std::endl;
	std::cout << "5 - Send Configuration" << std::endl;
	std::cout << "6 - Send Analysis" << std::endl;
	std::cout << "7 - Send Debug" << std::endl;
	std::cout << "? - Output Menu" << std::endl;
	std::cout << "ESC - Exit Component" << std::endl;
}

void mainLoop()
{
	unsigned char choice = 0;

	while (mainRunning)
	{
		choice = system::Application::getChar();
		switch (choice)
		{
			case system::Application::ASCII_ESC:
				mainRunning = false;
				break;
			case '?':
				printMenu();
				break;
			case '1':
				sendPosition();
				break;
			case '2':
				sendSensorData();
				break;
			case '3':
				sendPalletLane();
				break;
			case '4':
				sendErrorCorrections();
				break;
			case '5':
				sendConfiguration();
				break;
			case '6':
				sendAnalysis();
				break;
			case '7':
				sendDebug();
				break;
		}
	}
}

void processMessage(void *)
{
	model::Message *msg = msgSocket->getMessage();
	switch (msg->getId())
	{
		case Position::ID:
		{
			processPosition(msg);
			break;
		}
		case SensorData::ID:
		{
			processSensorData(msg);
			break;
		}
		case PalletLane::ID:
		{
			processPalletLane(msg);
			break;
		}
		case ErrorCorrections::ID:
		{
			processErrorCorrections(msg);
			break;
		}
		case Configuration::ID:
		{
			processConfiguration(msg);
			break;
		}
		case Analysis::ID:
		{
			processConfiguration(msg);
			break;
		}
		case Debug::ID:
		{
			processConfiguration(msg);
			break;
		}
	}
}


bool sendPosition()
{
	Position *msg = new Position();
	msg->setDestination(serverAddress);
	msg->setDestinationPort(serverPort);
	std::cout << "Sending Position to " <<  serverAddress << ":" << serverPort << std::endl;
	return msgSocket->sendMessage(msg);
}

bool processPosition(const model::Message *msg)
{
	std::cout << "Process position..." << std::endl;
	Position position;
	system::Buffer* buffer = dynamic_cast<system::Buffer*>(msg->getPayload());
	system::BufferReader& reader = buffer->getReader();
	reader.reset();
	position.from(reader); // Sets the ID and payload buffer

	std::cout << position << std::endl;

	return true;
}

bool sendSensorData()
{
	for (int i = 0; i < 10; i++)
	{
		SensorData *msg = new SensorData();
		msg->setDestination(serverAddress);
		msg->setDestinationPort(serverPort);

		msg->setLaserId(LaserIdEnumeration::LASER_A);

		SensorDataArray &array = msg->getSensorData();

		for (int j = 0; j < 541; j++)
		{
			LaserPointRecord point;
			point.setX_m(i);
			point.setY_m(j);
			array.add(point);
		}
		
		std::cout << "Sending SensorData to " << serverAddress << ":" << serverPort << std::endl;
 		msgSocket->sendMessage(msg);

		openjaus::system::Time::sleep(500);
	}
	return true;
}

bool processSensorData(const model::Message *msg)
{
	std::cout << "Process sensorData..." << std::endl;
	SensorData sensorData;
	system::Buffer* buffer = dynamic_cast<system::Buffer*>(msg->getPayload());
	system::BufferReader& reader = buffer->getReader();
	reader.reset();
	sensorData.from(reader); // Sets the ID and payload buffer

	std::cout << sensorData << std::endl;

	return true;
}

bool sendPalletLane()
{
	PalletLane *msg = new PalletLane();
	msg->setDestination(serverAddress);
	msg->setDestinationPort(serverPort);
	std::cout << "Sending PalletLane to " <<  serverAddress << ":" << serverPort << std::endl;
	return msgSocket->sendMessage(msg);
}

bool processPalletLane(const model::Message *msg)
{
	std::cout << "Process palletLane..." << std::endl;
	PalletLane palletLane;
	system::Buffer* buffer = dynamic_cast<system::Buffer*>(msg->getPayload());
	system::BufferReader& reader = buffer->getReader();
	reader.reset();
	palletLane.from(reader); // Sets the ID and payload buffer

	std::cout << palletLane << std::endl;

	return true;
}

bool sendErrorCorrections()
{
	ErrorCorrections *errorCorrections = new ErrorCorrections();

	uint16 clientPort = system::Application::setting<uint16>("ClientPort", DEFAULT_CLIENT_PORT);
	std::string clientAddressStr = system::Application::setting<std::string>("ClientIpAddress", DEFAULT_CLIENT_IP);
	system::InetAddress clientAddress = system::InetAddress::getByName(clientAddressStr);

	errorCorrections->setDestination(clientAddress);
	errorCorrections->setDestinationPort(clientPort);
	
	//errorCorrections->setLateralOffsetError_m(float value);
	//errorCorrections->setDepthOffsetError_m(float value);
	//errorCorrections->setHeadingError_rad(float value);
	//errorCorrections->setCorrectionCode(int8_t value);
	//errorCorrections->setSecondaryData1(int8_t value);
	//errorCorrections->setSecondaryData2(int8_t value);
	//StatusBitField& getStatus(void);
	//bool setGlobalErrorX_m(float value);
	//bool setGlobalErrorY_m(float value);
	//bool setGlobalErrorHeading_rad(float value);


	//errorCorrections->setLateralOffsetError_m
	//errorCorrections->setDepthOffsetError_m(0);
	//errorCorrections->setLateralOffsetError_m(0);
	//errorCorrections->setCorrectionCode(0);
	
	
	
	
	std::cout << "Sending ErrorCorrections to " <<  serverAddress << ":" << serverPort << std::endl;
	
	
	
	return msgSocket->sendMessage(errorCorrections);
}

bool processErrorCorrections(const model::Message *msg)
{
	std::cout << "Process errorCorrections..." << std::endl;
	ErrorCorrections errorCorrections;
	system::Buffer* buffer = dynamic_cast<system::Buffer*>(msg->getPayload());
	system::BufferReader& reader = buffer->getReader();
	reader.reset();
	errorCorrections.from(reader); // Sets the ID and payload buffer

	std::cout << errorCorrections << std::endl;

	return true;
}

bool sendConfiguration()
{
	Configuration *msg = new Configuration();
	msg->setDestination(serverAddress);
	msg->setDestinationPort(serverPort);
	std::cout << "Sending Configuration to " <<  serverAddress << ":" << serverPort << std::endl;
	return msgSocket->sendMessage(msg);
}

bool processConfiguration(const model::Message *msg)
{
	std::cout << "Process configuration..." << std::endl;
	Configuration configuration;
	system::Buffer* buffer = dynamic_cast<system::Buffer*>(msg->getPayload());
	system::BufferReader& reader = buffer->getReader();
	reader.reset();
	configuration.from(reader); // Sets the ID and payload buffer

	std::cout << configuration << std::endl;

	return true;
}

bool sendAnalysis()
{
	Analysis *analysisMsg = new Analysis();

	// Get ObjectsArray by reference
	ObjectsArray &objects = analysisMsg->getObjects();

	// Add one object
	SensedObjectRecord object;

	// Reference Line
	object.getReferenceLine().setStartPointX_m(0);
	object.getReferenceLine().setStartPointY_m(0);
	object.getReferenceLine().setEndPointX_m(10);
	object.getReferenceLine().setEndPointY_m(10);

	// Sensed Line
	object.getSensedLine().setStartPointX_m(-0.5);
	object.getSensedLine().setStartPointY_m(.25);
	object.getSensedLine().setEndPointX_m(10.5);
	object.getSensedLine().setEndPointY_m(20.5);

	// Errors
	object.setLateralOffsetError_m(-0.5);
	object.setLongitudinalOffsetError_m(0);
	object.setHeadingError_rad(0.25);

	// Add this object to the array
	objects.add(object);

	// Set destination IP info
	analysisMsg->setDestination(serverAddress);
	analysisMsg->setDestinationPort(serverPort);

	std::cout << "Sending Analysis to " << serverAddress << ":" << serverPort << std::endl;
	return msgSocket->sendMessage(analysisMsg);
}

bool sendDebug()
{
	Debug *debugMsg = new Debug();

	// Set Debug String
	debugMsg->setDebugMessage("This is my Debug Message");

	debugMsg->setDestination(serverAddress);
	debugMsg->setDestinationPort(serverPort);
	std::cout << "Sending Debug to " << serverAddress << ":" << serverPort << std::endl;
	return msgSocket->sendMessage(debugMsg);
}



