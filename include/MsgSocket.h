/*
 * MsgSocket.h
 *
 *  Created on: Feb 2, 2018
 *      Author: danny
 */

#ifndef MECHASPIN_MSGSOCKET_H
#define MECHASPIN_MSGSOCKET_H

#include <model/Message.h>
#include <system/MulticastSocket.h>
#include <system/InetAddress.h>
#include <system/Queue.h>
#include <system/Thread.h>

namespace mechaspin
{
	class MsgSocket;
	static const std::string ANY_INTERFACE = "Auto";
}

template <class CallbackClass, void (CallbackClass::*Func)(void *)>
struct MsgCallbackProxy
{
	static void ProxyFunction(void *object, void *val)
	{
		(static_cast<CallbackClass*>(object)->*Func)(val);
	}
};

#define MESSAGE_CALLBACK(Class, Func) MsgCallbackProxy<Class, &Class::Func>::ProxyFunction

namespace mechaspin
{

class OPENJAUS_EXPORT MsgSocket
{
public:
	MsgSocket(const std::string &interfaceName, uint16 port);
	~MsgSocket();

	void run();
	void stop();

	bool joinMulticastGroup(const std::string &groupAddress);
	bool sendMessage(openjaus::model::Message *msg);
	openjaus::model::Message *getMessage();
	bool hasMessagesWaiting() const;
	bool clearQueue();
	int getQueueSize() const;

	void addMessageCallback(void(*callbackFunc)(void *));
	void addMessageCallback(void(*callbackFunc)(void *object, void *), void *object);

protected:
	class Callback
	{
	public:
		Callback(void(*callback)(void *));
		Callback(void(*callback)(void *object, void *), void* object);
		Callback(const Callback& other);

		void execute();

		Callback& operator=(const Callback& other);

	private:
		void* object;
		void(*funcCallback)(void *);
		void(*objectCallback)(void *object, void *);
	};

	void addMessageCallback(MsgSocket::Callback* callback);

	void *recvThreadMethod();
    void *sendThreadMethod();
	openjaus::system::MulticastSocket socket;
	openjaus::system::Queue<openjaus::model::Message*> recvQueue;
	openjaus::system::Queue<openjaus::model::Message*> sendQueue;
	openjaus::system::Thread recvThread;
	openjaus::system::Thread sendThread;
	openjaus::system::Mutex mutex;

	void sendMessageInternal(openjaus::model::Message *msg);
	openjaus::system::NetworkInterface loadInterface(const std::string &interfaceName);

	MsgSocket::Callback *callback;

};

}

#endif /* MECHASPIN_MSGSOCKET_H */
