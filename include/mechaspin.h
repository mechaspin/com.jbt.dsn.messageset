/*
 * mechaspin.h
 *
 *  Created on: Jan 17, 2019
 *      Author: danny
 */

#ifndef INCLUDE_MECHASPIN_H_
#define INCLUDE_MECHASPIN_H_

#include <system.h>
#include <model/fields.h>
#include <model/Message.h>
using namespace openjaus;

#ifndef OPENJAUS_EXPORT
#define MECHASPIN_EXPORT	__declspec(dllexport)
#else
#define MECHASPIN_EXPORT	OPENJAUS_EXPORT
#endif


#endif /* INCLUDE_MECHASPIN_H_ */
