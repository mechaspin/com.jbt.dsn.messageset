/**
\file Position.h

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/

#ifndef JBT_POSITION_H
#define JBT_POSITION_H

#include <mechaspin.h>

#include "jbt/Triggers/Fields/DsnActiveEnumeration.h"
// Start of user code for additional includes
// End of user code for additional includes

namespace mechaspin
{

/// \class Position Position.h
/// \brief Position Message Implementation.
class OPENJAUS_EXPORT Position : public openjaus::model::Message
{
public:
	Position();
	Position(model::Message *message);
	~Position();

	static const uint16_t ID = 0xAAAA;




	/// \brief Pack this message to the given openjaus::system::Buffer. 
	/// \copybrief
	/// \param[out] dst - The destination openjaus::system::Buffer to which this message will be packed.
	/// \return The number of bytes packed into the destination buffer
	virtual uint64 to(system::BufferWriter& dst) const;	

	/// \brief Unpack this message from the given openjaus::system::Buffer.
	/// \copybrief
	/// \param[in] src - The source openjaus::system::Buffer from which this message will be unpacked.
	/// \return The number of bytes unpacked from the source buffer
	virtual uint64 from(system::BufferReader& src);

	/// \brief Get the number of bytes this message would occupy in a serialized buffer.
	/// \copybrief
	/// \return The number of bytes this message would occupy in a buffer
	virtual uint64 length() const;

	/// \brief Used to serialize the runtime state of the message to an XML format.
	/// \copybrief
	/// \param[in] ojIndentLevel - Used to determine how many tabs are inserted per line for pretty formating.
	/// \return The serialized XML string
	std::string toXml(unsigned char ojIndentLevel=0) const;

	/// \brief Returns a string populated with the Message name and ID
	/// \copybrief
	/// \return The string
	std::string toString() const;

	/// \brief OStream operator for Message object
	/// \copybrief
	OPENJAUS_EXPORT friend std::ostream& operator<<(std::ostream& output, const Position& object);

	/// \brief OStream operator for Message pointer
	/// \copybrief
	OPENJAUS_EXPORT friend std::ostream& operator<<(std::ostream& output, const Position* object);




	uint32_t getMagicNumber(void);
	bool setMagicNumber(uint32_t value);

	int8_t getVersionNumber(void);
	bool setVersionNumber(int8_t value);

	uint32_t getCommandId(void);
	bool setCommandId(uint32_t value);

	uint32_t getDataLength(void);
	bool setDataLength(uint32_t value);

	uint64_t getTimestamp_us(void);
	bool setTimestamp_us(uint64_t value);

	DsnActiveEnumeration::DsnActiveEnum getDsnActive(void);
	bool setDsnActive(DsnActiveEnumeration::DsnActiveEnum value);
	std::string getDsnActiveToString(void);

	float getDeadReckoningX_m(void);
	bool setDeadReckoningX_m(float value);

	float getDeadReckoningY_m(void);
	bool setDeadReckoningY_m(float value);

	float getHeading_rad(void);
	bool setHeading_rad(float value);

	float getNavX_m(void);
	bool setNavX_m(float value);

	float getNavY_m(void);
	bool setNavY_m(float value);

	float getNavHeading_rad(void);
	bool setNavHeading_rad(float value);
	// Start of user code for additional methods
	// End of user code for additional methods

private:
	model::fields::UnsignedInteger magicNumber;
	model::fields::Byte versionNumber;
	model::fields::UnsignedInteger commandId;
	model::fields::UnsignedInteger dataLength;
	model::fields::UnsignedLong timestamp_us;
	DsnActiveEnumeration dsnActive;
	model::fields::Float deadReckoningX_m;
	model::fields::Float deadReckoningY_m;
	model::fields::Float heading_rad;
	model::fields::Float navX_m;
	model::fields::Float navY_m;
	model::fields::Float navHeading_rad;

	// Start of user code for additional members
	// End of user code for additional members
};

} // namespace mechaspin

#endif // JBT_POSITION_H


