/**
\file Configuration.h

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/

#ifndef JBT_CONFIGURATION_H
#define JBT_CONFIGURATION_H

#include <mechaspin.h>

// Start of user code for additional includes
// End of user code for additional includes

namespace mechaspin
{

/// \class Configuration Configuration.h
/// \brief Configuration Message Implementation.
class OPENJAUS_EXPORT Configuration : public openjaus::model::Message
{
public:
	Configuration();
	Configuration(model::Message *message);
	~Configuration();

	static const uint16_t ID = 0xAAF5;




	/// \brief Pack this message to the given openjaus::system::Buffer. 
	/// \copybrief
	/// \param[out] dst - The destination openjaus::system::Buffer to which this message will be packed.
	/// \return The number of bytes packed into the destination buffer
	virtual uint64 to(system::BufferWriter& dst) const;	

	/// \brief Unpack this message from the given openjaus::system::Buffer.
	/// \copybrief
	/// \param[in] src - The source openjaus::system::Buffer from which this message will be unpacked.
	/// \return The number of bytes unpacked from the source buffer
	virtual uint64 from(system::BufferReader& src);

	/// \brief Get the number of bytes this message would occupy in a serialized buffer.
	/// \copybrief
	/// \return The number of bytes this message would occupy in a buffer
	virtual uint64 length() const;

	/// \brief Used to serialize the runtime state of the message to an XML format.
	/// \copybrief
	/// \param[in] ojIndentLevel - Used to determine how many tabs are inserted per line for pretty formating.
	/// \return The serialized XML string
	std::string toXml(unsigned char ojIndentLevel=0) const;

	/// \brief Returns a string populated with the Message name and ID
	/// \copybrief
	/// \return The string
	std::string toString() const;

	/// \brief OStream operator for Message object
	/// \copybrief
	OPENJAUS_EXPORT friend std::ostream& operator<<(std::ostream& output, const Configuration& object);

	/// \brief OStream operator for Message pointer
	/// \copybrief
	OPENJAUS_EXPORT friend std::ostream& operator<<(std::ostream& output, const Configuration* object);




	uint32_t getMagicNumber(void);
	bool setMagicNumber(uint32_t value);

	int8_t getVersionNumber(void);
	bool setVersionNumber(int8_t value);

	uint32_t getCommandId(void);
	bool setCommandId(uint32_t value);

	uint32_t getDataLength(void);
	bool setDataLength(uint32_t value);

	int8_t getScanUseInterval(void);
	bool setScanUseInterval(int8_t value);

	int8_t getNumberScans(void);
	bool setNumberScans(int8_t value);

	int8_t getNumberActiveToStart(void);
	bool setNumberActiveToStart(int8_t value);

	int8_t getOnlyUseSensedEdgeLength(void);
	bool setOnlyUseSensedEdgeLength(int8_t value);

	int8_t getResetLaneBoundaries(void);
	bool setResetLaneBoundaries(int8_t value);

	int8_t getAlignToDetectedScans(void);
	bool setAlignToDetectedScans(int8_t value);

	int8_t getUpdateOnlyIfGood(void);
	bool setUpdateOnlyIfGood(int8_t value);

	int8_t getUseInitialLaneCompromise(void);
	bool setUseInitialLaneCompromise(int8_t value);

	float getLaneMismatchTolerance_m(void);
	bool setLaneMismatchTolerance_m(float value);

	float getLaneAngularMismatchTolerance_deg(void);
	bool setLaneAngularMismatchTolerance_deg(float value);

	float getMinLaneLength_m(void);
	bool setMinLaneLength_m(float value);

	float getInitialLaneMinMatch_percent(void);
	bool setInitialLaneMinMatch_percent(float value);

	float getInitialLateralTolerance_m(void);
	bool setInitialLateralTolerance_m(float value);

	float getInitialHeadingTolerance_deg(void);
	bool setInitialHeadingTolerance_deg(float value);

	float getInitialLateralTolerance2_m(void);
	bool setInitialLateralTolerance2_m(float value);

	float getInitialLaneCompromiseMaxAllowedError_m(void);
	bool setInitialLaneCompromiseMaxAllowedError_m(float value);

	float getMaxLateralDeviation_m(void);
	bool setMaxLateralDeviation_m(float value);

	float getMaxHeadingDeviation_deg(void);
	bool setMaxHeadingDeviation_deg(float value);

	float getLateralErrorMaxStepDelta_m(void);
	bool setLateralErrorMaxStepDelta_m(float value);

	float getYawErrorMaxStepDelta_deg(void);
	bool setYawErrorMaxStepDelta_deg(float value);
	// Start of user code for additional methods
	// End of user code for additional methods

private:
	model::fields::UnsignedInteger magicNumber;
	model::fields::Byte versionNumber;
	model::fields::UnsignedInteger commandId;
	model::fields::UnsignedInteger dataLength;
	model::fields::Byte scanUseInterval;
	model::fields::Byte numberScans;
	model::fields::Byte numberActiveToStart;
	model::fields::Byte onlyUseSensedEdgeLength;
	model::fields::Byte resetLaneBoundaries;
	model::fields::Byte alignToDetectedScans;
	model::fields::Byte updateOnlyIfGood;
	model::fields::Byte useInitialLaneCompromise;
	model::fields::Float laneMismatchTolerance_m;
	model::fields::Float laneAngularMismatchTolerance_deg;
	model::fields::Float minLaneLength_m;
	model::fields::Float initialLaneMinMatch_percent;
	model::fields::Float initialLateralTolerance_m;
	model::fields::Float initialHeadingTolerance_deg;
	model::fields::Float initialLateralTolerance2_m;
	model::fields::Float initialLaneCompromiseMaxAllowedError_m;
	model::fields::Float maxLateralDeviation_m;
	model::fields::Float maxHeadingDeviation_deg;
	model::fields::Float lateralErrorMaxStepDelta_m;
	model::fields::Float yawErrorMaxStepDelta_deg;

	// Start of user code for additional members
	// End of user code for additional members
};

} // namespace mechaspin

#endif // JBT_CONFIGURATION_H


