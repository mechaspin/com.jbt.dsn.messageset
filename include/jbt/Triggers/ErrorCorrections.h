/**
\file ErrorCorrections.h

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/

#ifndef JBT_ERRORCORRECTIONS_H
#define JBT_ERRORCORRECTIONS_H

#include <mechaspin.h>

#include "jbt/Triggers/Fields/StatusBitField.h"
// Start of user code for additional includes
// End of user code for additional includes

namespace mechaspin
{

/// \class ErrorCorrections ErrorCorrections.h
/// \brief ErrorCorrections Message Implementation.
class OPENJAUS_EXPORT ErrorCorrections : public openjaus::model::Message
{
public:
	ErrorCorrections();
	ErrorCorrections(model::Message *message);
	~ErrorCorrections();

	static const uint16_t ID = 0xAACC;




	/// \brief Pack this message to the given openjaus::system::Buffer. 
	/// \copybrief
	/// \param[out] dst - The destination openjaus::system::Buffer to which this message will be packed.
	/// \return The number of bytes packed into the destination buffer
	virtual uint64 to(system::BufferWriter& dst) const;	

	/// \brief Unpack this message from the given openjaus::system::Buffer.
	/// \copybrief
	/// \param[in] src - The source openjaus::system::Buffer from which this message will be unpacked.
	/// \return The number of bytes unpacked from the source buffer
	virtual uint64 from(system::BufferReader& src);

	/// \brief Get the number of bytes this message would occupy in a serialized buffer.
	/// \copybrief
	/// \return The number of bytes this message would occupy in a buffer
	virtual uint64 length() const;

	/// \brief Used to serialize the runtime state of the message to an XML format.
	/// \copybrief
	/// \param[in] ojIndentLevel - Used to determine how many tabs are inserted per line for pretty formating.
	/// \return The serialized XML string
	std::string toXml(unsigned char ojIndentLevel=0) const;

	/// \brief Returns a string populated with the Message name and ID
	/// \copybrief
	/// \return The string
	std::string toString() const;

	/// \brief OStream operator for Message object
	/// \copybrief
	OPENJAUS_EXPORT friend std::ostream& operator<<(std::ostream& output, const ErrorCorrections& object);

	/// \brief OStream operator for Message pointer
	/// \copybrief
	OPENJAUS_EXPORT friend std::ostream& operator<<(std::ostream& output, const ErrorCorrections* object);




	uint32_t getMagicNumber(void);
	bool setMagicNumber(uint32_t value);

	int8_t getVersionNumber(void);
	bool setVersionNumber(int8_t value);

	uint32_t getCommandId(void);
	bool setCommandId(uint32_t value);

	uint32_t getDataLength(void);
	bool setDataLength(uint32_t value);

	float getLateralOffsetError_m(void);
	bool setLateralOffsetError_m(float value);

	float getDepthOffsetError_m(void);
	bool setDepthOffsetError_m(float value);

	float getHeadingError_rad(void);
	bool setHeadingError_rad(float value);

	int8_t getCorrectionCode(void);
	bool setCorrectionCode(int8_t value);

	int8_t getSecondaryData1(void);
	bool setSecondaryData1(int8_t value);

	int8_t getSecondaryData2(void);
	bool setSecondaryData2(int8_t value);

	StatusBitField& getStatus(void);

	float getGlobalErrorX_m(void);
	bool setGlobalErrorX_m(float value);

	float getGlobalErrorY_m(void);
	bool setGlobalErrorY_m(float value);

	float getGlobalErrorHeading_rad(void);
	bool setGlobalErrorHeading_rad(float value);
	// Start of user code for additional methods
	// End of user code for additional methods

private:
	model::fields::UnsignedInteger magicNumber;
	model::fields::Byte versionNumber;
	model::fields::UnsignedInteger commandId;
	model::fields::UnsignedInteger dataLength;
	model::fields::Float lateralOffsetError_m;
	model::fields::Float depthOffsetError_m;
	model::fields::Float headingError_rad;
	model::fields::Byte correctionCode;
	model::fields::Byte secondaryData1;
	model::fields::Byte secondaryData2;
	StatusBitField status;
	model::fields::Float globalErrorX_m;
	model::fields::Float globalErrorY_m;
	model::fields::Float globalErrorHeading_rad;

	// Start of user code for additional members
	// End of user code for additional members
};

} // namespace mechaspin

#endif // JBT_ERRORCORRECTIONS_H


