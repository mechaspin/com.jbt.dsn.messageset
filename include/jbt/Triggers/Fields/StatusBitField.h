/**
\file StatusBitField.h

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/


#ifndef JBT_STATUSBITFIELD_H
#define JBT_STATUSBITFIELD_H

#include <system.h>
#include <model/fields.h>
// Start of user code for additional includes:
// End of user code

using namespace openjaus;
namespace mechaspin
{

class OPENJAUS_EXPORT StatusBitField : public model::fields::BitField
{
public:
	StatusBitField();
	~StatusBitField();


    bool setAllOk(bool value);
    bool getAllOk(void) const;
    bool setPathHeadingErrorTooLarge(bool value);
    bool getPathHeadingErrorTooLarge(void) const;
    bool setPathLateralErrorTooLarge(bool value);
    bool getPathLateralErrorTooLarge(void) const;
    bool setInsufficientData(bool value);
    bool getInsufficientData(void) const;
    bool setLaneFitHeadingError(bool value);
    bool getLaneFitHeadingError(void) const;
    bool setLaneFitLateralError(bool value);
    bool getLaneFitLateralError(void) const;
    bool setInitialCompromiseErrorTooLarge(bool value);
    bool getInitialCompromiseErrorTooLarge(void) const;
    bool setLaneConfidenceTooLow(bool value);
    bool getLaneConfidenceTooLow(void) const;
    bool setNoHostInformation(bool value);
    bool getNoHostInformation(void) const;
    bool setNotConfiguredError(bool value);
    bool getNotConfiguredError(void) const;


	uint32_t getIntegerValue(void) const;
	void setIntegerValue(uint32_t value);
	virtual uint64 to(system::BufferWriter& dst) const;	
	virtual uint64 from(system::BufferReader& src);
	virtual uint64 length() const;
	void copy(StatusBitField& source);
	std::string toXml(unsigned char ojIndentLevel=0) const;
	
protected:
	bool allOk;
	bool pathHeadingErrorTooLarge;
	bool pathLateralErrorTooLarge;
	bool insufficientData;
	bool laneFitHeadingError;
	bool laneFitLateralError;
	bool initialCompromiseErrorTooLarge;
	bool laneConfidenceTooLow;
	bool noHostInformation;
	bool notConfiguredError;

    static const long ALLOK_START_BIT = 0;
    static const long ALLOK_BIT_MASK = 0x1;
    
    static const long PATHHEADINGERRORTOOLARGE_START_BIT = 2;
    static const long PATHHEADINGERRORTOOLARGE_BIT_MASK = 0x1;
    
    static const long PATHLATERALERRORTOOLARGE_START_BIT = 3;
    static const long PATHLATERALERRORTOOLARGE_BIT_MASK = 0x1;
    
    static const long INSUFFICIENTDATA_START_BIT = 4;
    static const long INSUFFICIENTDATA_BIT_MASK = 0x1;
    
    static const long LANEFITHEADINGERROR_START_BIT = 6;
    static const long LANEFITHEADINGERROR_BIT_MASK = 0x1;
    
    static const long LANEFITLATERALERROR_START_BIT = 7;
    static const long LANEFITLATERALERROR_BIT_MASK = 0x1;
    
    static const long INITIALCOMPROMISEERRORTOOLARGE_START_BIT = 8;
    static const long INITIALCOMPROMISEERRORTOOLARGE_BIT_MASK = 0x1;
    
    static const long LANECONFIDENCETOOLOW_START_BIT = 9;
    static const long LANECONFIDENCETOOLOW_BIT_MASK = 0x1;
    
    static const long NOHOSTINFORMATION_START_BIT = 11;
    static const long NOHOSTINFORMATION_BIT_MASK = 0x1;
    
    static const long NOTCONFIGUREDERROR_START_BIT = 12;
    static const long NOTCONFIGUREDERROR_BIT_MASK = 0x1;
    

// Start of user code for additional methods:
// End of user code

};

} // namespace jbt


#endif // JBT_STATUSBITFIELD_H

