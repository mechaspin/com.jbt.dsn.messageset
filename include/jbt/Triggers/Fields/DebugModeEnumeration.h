/**
\file DebugModeEnumeration.h

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/

#ifndef JBT_DEBUGMODEENUMERATION_H
#define JBT_DEBUGMODEENUMERATION_H

#include <system.h>
#include <model/fields.h>

using namespace openjaus;
namespace mechaspin
{

class OPENJAUS_EXPORT DebugModeEnumeration : public openjaus::model::fields::Enumeration
{
public:
	enum DebugModeEnum {	DEBUGMODEINACTIVE = 0,
								DEBUGMODEACTIVE = 1};
	DebugModeEnumeration();
	DebugModeEnumeration(DebugModeEnum value);
	~DebugModeEnumeration();

	
	virtual uint64 to(system::BufferWriter& dst) const;
	virtual uint64 from(system::BufferReader& src);
	virtual uint64 length(void) const;
	std::string toXml(unsigned char ojIndentLevel=0) const;	
	std::string toString() const;
	static std::string toString(DebugModeEnum value);
	
	DebugModeEnum getValue(void) const;
	void setValue(DebugModeEnum value);

protected:
	DebugModeEnum value;
};

} // namespace jbt


#endif // JBT_DEBUGMODEENUMERATION_H

