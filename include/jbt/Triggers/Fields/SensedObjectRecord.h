
/**
\file SensedObjectRecord.h

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/

#ifndef JBT_SENSEDOBJECTRECORD_H
#define JBT_SENSEDOBJECTRECORD_H

#include <system.h>
#include <model/fields.h>
#include "jbt/Triggers/Fields/InfoFlagBitField.h"
#include "jbt/Triggers/Fields/JbtLineRecord.h"
#include "jbt/Triggers/Fields/JbtLineRecord.h"
// Start of user code for additional includes
// End of user code for additional includes

using namespace openjaus;

namespace mechaspin
{

class OPENJAUS_EXPORT SensedObjectRecord : public openjaus::model::fields::Record
{
public:
	SensedObjectRecord();
	SensedObjectRecord(const SensedObjectRecord &source);
	~SensedObjectRecord();


	void copy(SensedObjectRecord& source);
	virtual uint64 to(system::BufferWriter& dst) const;
	virtual uint64 from(system::BufferReader& src);
	virtual uint64 length(void) const;
	std::string toXml(unsigned char ojIndentLevel=0) const;
	

	InfoFlagBitField& getInfoFlag(void);

	JbtLineRecord& getReferenceLine(void);

	JbtLineRecord& getSensedLine(void);

	float getLateralOffsetError_m(void);
	bool setLateralOffsetError_m(float value);

	float getLongitudinalOffsetError_m(void);
	bool setLongitudinalOffsetError_m(float value);

	float getHeadingError_rad(void);
	bool setHeadingError_rad(float value);
	// Start of user code for additional methods
	// End of user code for additional methods

protected:
	InfoFlagBitField infoFlag;
	JbtLineRecord referenceLine;
	JbtLineRecord sensedLine;
	model::fields::Float lateralOffsetError_m;
	model::fields::Float longitudinalOffsetError_m;
	model::fields::Float headingError_rad;

	// Start of user code for additional members
	// End of user code for additional members
};

} // namespace jbt


#endif // JBT_SENSEDOBJECTRECORD_H