/**
\file InfoFlagBitField.h

\par Copyright
Copyright (c) 2010-2018, OpenJAUS LLC (dba MechaSpin)
All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LLC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
Start of user code
- [2018-01] - Version 1.0
End of user code
*/


#ifndef JBT_INFOFLAGBITFIELD_H
#define JBT_INFOFLAGBITFIELD_H

#include <system.h>
#include <model/fields.h>
// Start of user code for additional includes:
// End of user code

using namespace openjaus;
namespace mechaspin
{

class OPENJAUS_EXPORT InfoFlagBitField : public model::fields::BitField
{
public:
	InfoFlagBitField();
	~InfoFlagBitField();


    bool setBit0(bool value);
    bool getBit0(void) const;
    bool setBit1(bool value);
    bool getBit1(void) const;
    bool setBit2(bool value);
    bool getBit2(void) const;
    bool setBit3(bool value);
    bool getBit3(void) const;
    bool setBit4(bool value);
    bool getBit4(void) const;
    bool setBit5(bool value);
    bool getBit5(void) const;
    bool setBit6(bool value);
    bool getBit6(void) const;
    bool setBit7(bool value);
    bool getBit7(void) const;
    bool setBit8(bool value);
    bool getBit8(void) const;
    bool setBit9(bool value);
    bool getBit9(void) const;
    bool setBit10(bool value);
    bool getBit10(void) const;
    bool setBit11(bool value);
    bool getBit11(void) const;
    bool setBit12(bool value);
    bool getBit12(void) const;
    bool setBit13(bool value);
    bool getBit13(void) const;
    bool setBit14(bool value);
    bool getBit14(void) const;
    bool setBit15(bool value);
    bool getBit15(void) const;


	uint16_t getIntegerValue(void) const;
	void setIntegerValue(uint16_t value);
	virtual uint64 to(system::BufferWriter& dst) const;	
	virtual uint64 from(system::BufferReader& src);
	virtual uint64 length() const;
	void copy(InfoFlagBitField& source);
	std::string toXml(unsigned char ojIndentLevel=0) const;
	
protected:
	bool bit0;
	bool bit1;
	bool bit2;
	bool bit3;
	bool bit4;
	bool bit5;
	bool bit6;
	bool bit7;
	bool bit8;
	bool bit9;
	bool bit10;
	bool bit11;
	bool bit12;
	bool bit13;
	bool bit14;
	bool bit15;

    static const long BIT0_START_BIT = 0;
    static const long BIT0_BIT_MASK = 0x1;
    
    static const long BIT1_START_BIT = 1;
    static const long BIT1_BIT_MASK = 0x1;
    
    static const long BIT2_START_BIT = 2;
    static const long BIT2_BIT_MASK = 0x1;
    
    static const long BIT3_START_BIT = 3;
    static const long BIT3_BIT_MASK = 0x1;
    
    static const long BIT4_START_BIT = 4;
    static const long BIT4_BIT_MASK = 0x1;
    
    static const long BIT5_START_BIT = 5;
    static const long BIT5_BIT_MASK = 0x1;
    
    static const long BIT6_START_BIT = 6;
    static const long BIT6_BIT_MASK = 0x1;
    
    static const long BIT7_START_BIT = 7;
    static const long BIT7_BIT_MASK = 0x1;
    
    static const long BIT8_START_BIT = 8;
    static const long BIT8_BIT_MASK = 0x1;
    
    static const long BIT9_START_BIT = 9;
    static const long BIT9_BIT_MASK = 0x1;
    
    static const long BIT10_START_BIT = 10;
    static const long BIT10_BIT_MASK = 0x1;
    
    static const long BIT11_START_BIT = 11;
    static const long BIT11_BIT_MASK = 0x1;
    
    static const long BIT12_START_BIT = 12;
    static const long BIT12_BIT_MASK = 0x1;
    
    static const long BIT13_START_BIT = 13;
    static const long BIT13_BIT_MASK = 0x1;
    
    static const long BIT14_START_BIT = 14;
    static const long BIT14_BIT_MASK = 0x1;
    
    static const long BIT15_START_BIT = 15;
    static const long BIT15_BIT_MASK = 0x1;
    

// Start of user code for additional methods:
// End of user code

};

} // namespace jbt


#endif // JBT_INFOFLAGBITFIELD_H

