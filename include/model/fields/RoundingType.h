/**
\file RoundingType.h

\par Copyright
Copyright (c) 2010-2016, OpenJAUS, LLC
All rights reserved.

This file is part of the OpenJAUS Software Development Kit (SDK). This
software is distributed under one of two licenses, the OpenJAUS SDK
Commercial End User License Agreement or the OpenJAUS SDK Non-Commercial
End User License Agreement. The appropriate licensing details were included
in with your developer credentials and software download. See the LICENSE
file included with this software for full details.
 
THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LCC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
- [2015-08] - Version 4.3 Release
- [2014-12] - Version 4.2 Release
- [2014-01] - Version 4.1 Release
- [2011-06] - Version 4.0 Release

*/
#ifndef ROUNDINGTYPE_H
#define ROUNDINGTYPE_H

#include <istream>
#include <ostream>
#include <string>

namespace openjaus
{
namespace model
{
namespace fields
{

enum RoundingType
{
	FLOOR = 0,
	ROUND = 1,
	CEILING = 2
};

inline std::ostream& operator<<(std::ostream& output, const RoundingType& enumValue)
{
	switch(enumValue)
	{
		case FLOOR:
			output << "FLOOR";
			break;
			
		case ROUND:
			output << "ROUND";
			break;
			
		case CEILING:
			output << "CEILING";
			break;
			
	}
	return output;
}

inline std::istream& operator>>(std::istream& input, RoundingType& enumValue)
{
	std::string enumString;
	input >> enumString;
	if(enumString.compare("FLOOR") == 0)
	{
		enumValue = FLOOR;
	}
	if(enumString.compare("ROUND") == 0)
	{
		enumValue = ROUND;
	}
	if(enumString.compare("CEILING") == 0)
	{
		enumValue = CEILING;
	}
    return input;
}

} // namespace fields
} // namespace model
} // namespace openjaus

#endif // ROUNDINGTYPE_H

