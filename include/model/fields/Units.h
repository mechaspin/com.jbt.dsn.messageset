/**
\file Units.h

\par Copyright
Copyright (c) 2010-2016, OpenJAUS, LLC
All rights reserved.

This file is part of the OpenJAUS Software Development Kit (SDK). This
software is distributed under one of two licenses, the OpenJAUS SDK
Commercial End User License Agreement or the OpenJAUS SDK Non-Commercial
End User License Agreement. The appropriate licensing details were included
in with your developer credentials and software download. See the LICENSE
file included with this software for full details.
 
THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LCC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
- [2015-08] - Version 4.3 Release
- [2014-12] - Version 4.2 Release
- [2014-01] - Version 4.1 Release
- [2011-06] - Version 4.0 Release

*/
#ifndef UNITS_H
#define UNITS_H

#include <istream>
#include <ostream>
#include <string>

namespace openjaus
{
namespace model
{
namespace fields
{

enum Units
{
	UNITS_ONE = 0,
	UNITS_METER = 1,
	UNITS_KILOGRAM = 2,
	UNITS_SECOND = 3,
	UNITS_AMPERE = 4,
	UNITS_KELVIN = 5,
	UNITS_MOLE = 6,
	UNITS_CANDELA = 7,
	UNITS_SQUARE_METER = 8,
	UNITS_CUBIC_METER = 9,
	UNITS_METERS_PER_SECOND = 10,
	UNITS_METERS_PER_SECOND_SQUARED = 11,
	UNITS_RECIPROCAL_METER = 12,
	UNITS_KILOGRAM_PER_CUBIC_METER = 13,
	UNITS_CUBIC_METER_PER_KILOGRAM = 14,
	UNITS_HERTZ = 15,
	UNITS_MINUTE = 16,
	UNITS_DEGREES = 17,
	UNITS_RADIANS = 18,
	UNITS_RADIANS_PER_SECOND = 19,
	UNITS_RADIANS_PER_SECOND_SQUARED = 20,
	UNITS_PERCENT = 21,
	UNITS_NEWTON = 22,
	UNITS_NEWTON_METER = 23,
	UNITS_DEGREE_CELSIUS = 24,
	UNITS_AMPERE_PER_SQUARE_METER = 25,
	UNITS_AMPERE_PER_METER = 26,
	UNITS_MOLE_PER_CUBIC_METER = 27,
	UNITS_CANDELA_PER_SQUARE_METER = 28,
	UNITS_STERADIAN = 29,
	UNITS_PASCAL = 30,
	UNITS_JOULE = 31,
	UNITS_WATT = 32,
	UNITS_COULOMB = 33,
	UNITS_VOLT = 34,
	UNITS_FARAD = 35,
	UNITS_OHM = 36,
	UNITS_SIEMENS = 37,
	UNITS_WEBER = 38,
	UNITS_TESLA = 39,
	UNITS_HENRY = 40,
	UNITS_LUMEN = 41,
	UNITS_LUX = 42,
	UNITS_BECQUEREL = 43,
	UNITS_SIEVERT = 44,
	UNITS_KATAL = 45,
	UNITS_PASCAL_SECOND = 46,
	UNITS_NEWTONS_PER_METER = 47,
	UNITS_WATT_PER_SQUARE_METER = 50,
	UNITS_JOULE_PER_KELVIN = 51,
	UNITS_JOULE_PER_KILOGRAM = 52,
	UNITS_WATT_PER_METER_KELVIN = 53,
	UNITS_JOULE_PER_CUBIC_METER = 54,
	UNITS_VOLT_PER_METER = 55,
	UNITS_COULOMB_PER_CUBIC_METER = 56,
	UNITS_COULOMB_PER_SQUARE_METER = 57,
	UNITS_FARAD_PER_METER = 58,
	UNITS_HENRY_PER_METER = 59,
	UNITS_JOULE_PER_MOLE = 60,
	UNITS_JOULE_PER_MOLE_KELVIN = 61,
	UNITS_COULOMB_PER_KILOGRAM = 62,
	UNITS_GRAY_PER_SECOND = 63,
	UNITS_WATT_PER_SQUARE_METER_STERADIAN = 64,
	UNITS_KATAL_PER_CUBIC_METER = 65,
	UNITS_HOUR = 66,
	UNITS_DAY = 67,
	UNITS_LITER = 69,
	UNITS_METRIC_TON = 70,
	UNITS_NEPER = 71,
	UNITS_BEL = 72,
	UNITS_NAUTICAL_MILE = 73,
	UNITS_KNOT = 74,
	UNITS_ARE = 75,
	UNITS_HECTARE = 76,
	UNITS_BAR = 77,
	UNITS_ANGSTRO = 78,
	UNITS_BARN = 79,
	UNITS_CURIE = 80,
	UNITS_ROENTGEN = 81,
	UNITS_RAD = 82,
	UNITS_REM = 83,
	UNITS_LITERS_PER_SECOND = 84,
	UNITS_LITERS_PER_HOUR = 85,
	UNITS_KILOGRAMS_PER_SECOND = 86,
	UNITS_KILOGRAM_SQUARE_METER = 87,
	UNITS_ROTATIONS_PER_MINUTE = 88,
	UNITS_BITS_PER_SECOND = 89,
	UNITS_FRAMES_PER_SECOND = 90,
	UNITS_AMPERE_HOUR = 91,
	UNITS_PARTS_PER_THOUSAND = 92,
	UNITS_PARTS_PER_MILLION = 93
};

inline std::ostream& operator<<(std::ostream& output, const Units& enumValue)
{
	switch(enumValue)
	{
		case UNITS_ONE:
			output << "UNITS_ONE";
			break;
			
		case UNITS_METER:
			output << "UNITS_METER";
			break;
			
		case UNITS_KILOGRAM:
			output << "UNITS_KILOGRAM";
			break;
			
		case UNITS_SECOND:
			output << "UNITS_SECOND";
			break;
			
		case UNITS_AMPERE:
			output << "UNITS_AMPERE";
			break;
			
		case UNITS_KELVIN:
			output << "UNITS_KELVIN";
			break;
			
		case UNITS_MOLE:
			output << "UNITS_MOLE";
			break;
			
		case UNITS_CANDELA:
			output << "UNITS_CANDELA";
			break;
			
		case UNITS_SQUARE_METER:
			output << "UNITS_SQUARE_METER";
			break;
			
		case UNITS_CUBIC_METER:
			output << "UNITS_CUBIC_METER";
			break;
			
		case UNITS_METERS_PER_SECOND:
			output << "UNITS_METERS_PER_SECOND";
			break;
			
		case UNITS_METERS_PER_SECOND_SQUARED:
			output << "UNITS_METERS_PER_SECOND_SQUARED";
			break;
			
		case UNITS_RECIPROCAL_METER:
			output << "UNITS_RECIPROCAL_METER";
			break;
			
		case UNITS_KILOGRAM_PER_CUBIC_METER:
			output << "UNITS_KILOGRAM_PER_CUBIC_METER";
			break;
			
		case UNITS_CUBIC_METER_PER_KILOGRAM:
			output << "UNITS_CUBIC_METER_PER_KILOGRAM";
			break;
			
		case UNITS_HERTZ:
			output << "UNITS_HERTZ";
			break;
			
		case UNITS_MINUTE:
			output << "UNITS_MINUTE";
			break;
			
		case UNITS_DEGREES:
			output << "UNITS_DEGREES";
			break;
			
		case UNITS_RADIANS:
			output << "UNITS_RADIANS";
			break;
			
		case UNITS_RADIANS_PER_SECOND:
			output << "UNITS_RADIANS_PER_SECOND";
			break;
			
		case UNITS_RADIANS_PER_SECOND_SQUARED:
			output << "UNITS_RADIANS_PER_SECOND_SQUARED";
			break;
			
		case UNITS_PERCENT:
			output << "UNITS_PERCENT";
			break;
			
		case UNITS_NEWTON:
			output << "UNITS_NEWTON";
			break;
			
		case UNITS_NEWTON_METER:
			output << "UNITS_NEWTON_METER";
			break;
			
		case UNITS_DEGREE_CELSIUS:
			output << "UNITS_DEGREE_CELSIUS";
			break;
			
		case UNITS_AMPERE_PER_SQUARE_METER:
			output << "UNITS_AMPERE_PER_SQUARE_METER";
			break;
			
		case UNITS_AMPERE_PER_METER:
			output << "UNITS_AMPERE_PER_METER";
			break;
			
		case UNITS_MOLE_PER_CUBIC_METER:
			output << "UNITS_MOLE_PER_CUBIC_METER";
			break;
			
		case UNITS_CANDELA_PER_SQUARE_METER:
			output << "UNITS_CANDELA_PER_SQUARE_METER";
			break;
			
		case UNITS_STERADIAN:
			output << "UNITS_STERADIAN";
			break;
			
		case UNITS_PASCAL:
			output << "UNITS_PASCAL";
			break;
			
		case UNITS_JOULE:
			output << "UNITS_JOULE";
			break;
			
		case UNITS_WATT:
			output << "UNITS_WATT";
			break;
			
		case UNITS_COULOMB:
			output << "UNITS_COULOMB";
			break;
			
		case UNITS_VOLT:
			output << "UNITS_VOLT";
			break;
			
		case UNITS_FARAD:
			output << "UNITS_FARAD";
			break;
			
		case UNITS_OHM:
			output << "UNITS_OHM";
			break;
			
		case UNITS_SIEMENS:
			output << "UNITS_SIEMENS";
			break;
			
		case UNITS_WEBER:
			output << "UNITS_WEBER";
			break;
			
		case UNITS_TESLA:
			output << "UNITS_TESLA";
			break;
			
		case UNITS_HENRY:
			output << "UNITS_HENRY";
			break;
			
		case UNITS_LUMEN:
			output << "UNITS_LUMEN";
			break;
			
		case UNITS_LUX:
			output << "UNITS_LUX";
			break;
			
		case UNITS_BECQUEREL:
			output << "UNITS_BECQUEREL";
			break;
			
		case UNITS_SIEVERT:
			output << "UNITS_SIEVERT";
			break;
			
		case UNITS_KATAL:
			output << "UNITS_KATAL";
			break;
			
		case UNITS_PASCAL_SECOND:
			output << "UNITS_PASCAL_SECOND";
			break;
			
		case UNITS_NEWTONS_PER_METER:
			output << "UNITS_NEWTONS_PER_METER";
			break;
			
		case UNITS_WATT_PER_SQUARE_METER:
			output << "UNITS_WATT_PER_SQUARE_METER";
			break;
			
		case UNITS_JOULE_PER_KELVIN:
			output << "UNITS_JOULE_PER_KELVIN";
			break;
			
		case UNITS_JOULE_PER_KILOGRAM:
			output << "UNITS_JOULE_PER_KILOGRAM";
			break;
			
		case UNITS_WATT_PER_METER_KELVIN:
			output << "UNITS_WATT_PER_METER_KELVIN";
			break;
			
		case UNITS_JOULE_PER_CUBIC_METER:
			output << "UNITS_JOULE_PER_CUBIC_METER";
			break;
			
		case UNITS_VOLT_PER_METER:
			output << "UNITS_VOLT_PER_METER";
			break;
			
		case UNITS_COULOMB_PER_CUBIC_METER:
			output << "UNITS_COULOMB_PER_CUBIC_METER";
			break;
			
		case UNITS_COULOMB_PER_SQUARE_METER:
			output << "UNITS_COULOMB_PER_SQUARE_METER";
			break;
			
		case UNITS_FARAD_PER_METER:
			output << "UNITS_FARAD_PER_METER";
			break;
			
		case UNITS_HENRY_PER_METER:
			output << "UNITS_HENRY_PER_METER";
			break;
			
		case UNITS_JOULE_PER_MOLE:
			output << "UNITS_JOULE_PER_MOLE";
			break;
			
		case UNITS_JOULE_PER_MOLE_KELVIN:
			output << "UNITS_JOULE_PER_MOLE_KELVIN";
			break;
			
		case UNITS_COULOMB_PER_KILOGRAM:
			output << "UNITS_COULOMB_PER_KILOGRAM";
			break;
			
		case UNITS_GRAY_PER_SECOND:
			output << "UNITS_GRAY_PER_SECOND";
			break;
			
		case UNITS_WATT_PER_SQUARE_METER_STERADIAN:
			output << "UNITS_WATT_PER_SQUARE_METER_STERADIAN";
			break;
			
		case UNITS_KATAL_PER_CUBIC_METER:
			output << "UNITS_KATAL_PER_CUBIC_METER";
			break;
			
		case UNITS_HOUR:
			output << "UNITS_HOUR";
			break;
			
		case UNITS_DAY:
			output << "UNITS_DAY";
			break;
			
		case UNITS_LITER:
			output << "UNITS_LITER";
			break;
			
		case UNITS_METRIC_TON:
			output << "UNITS_METRIC_TON";
			break;
			
		case UNITS_NEPER:
			output << "UNITS_NEPER";
			break;
			
		case UNITS_BEL:
			output << "UNITS_BEL";
			break;
			
		case UNITS_NAUTICAL_MILE:
			output << "UNITS_NAUTICAL_MILE";
			break;
			
		case UNITS_KNOT:
			output << "UNITS_KNOT";
			break;
			
		case UNITS_ARE:
			output << "UNITS_ARE";
			break;
			
		case UNITS_HECTARE:
			output << "UNITS_HECTARE";
			break;
			
		case UNITS_BAR:
			output << "UNITS_BAR";
			break;
			
		case UNITS_ANGSTRO:
			output << "UNITS_ANGSTRO";
			break;
			
		case UNITS_BARN:
			output << "UNITS_BARN";
			break;
			
		case UNITS_CURIE:
			output << "UNITS_CURIE";
			break;
			
		case UNITS_ROENTGEN:
			output << "UNITS_ROENTGEN";
			break;
			
		case UNITS_RAD:
			output << "UNITS_RAD";
			break;
			
		case UNITS_REM:
			output << "UNITS_REM";
			break;
			
		case UNITS_LITERS_PER_SECOND:
			output << "UNITS_LITERS_PER_SECOND";
			break;
			
		case UNITS_LITERS_PER_HOUR:
			output << "UNITS_LITERS_PER_HOUR";
			break;
			
		case UNITS_KILOGRAMS_PER_SECOND:
			output << "UNITS_KILOGRAMS_PER_SECOND";
			break;
			
		case UNITS_KILOGRAM_SQUARE_METER:
			output << "UNITS_KILOGRAM_SQUARE_METER";
			break;
			
		case UNITS_ROTATIONS_PER_MINUTE:
			output << "UNITS_ROTATIONS_PER_MINUTE";
			break;
			
		case UNITS_BITS_PER_SECOND:
			output << "UNITS_BITS_PER_SECOND";
			break;
			
		case UNITS_FRAMES_PER_SECOND:
			output << "UNITS_FRAMES_PER_SECOND";
			break;
			
		case UNITS_AMPERE_HOUR:
			output << "UNITS_AMPERE_HOUR";
			break;
			
		case UNITS_PARTS_PER_THOUSAND:
			output << "UNITS_PARTS_PER_THOUSAND";
			break;
			
		case UNITS_PARTS_PER_MILLION:
			output << "UNITS_PARTS_PER_MILLION";
			break;
			
	}
	return output;
}

inline std::istream& operator>>(std::istream& input, Units& enumValue)
{
	std::string enumString;
	input >> enumString;
	if(enumString.compare("UNITS_ONE") == 0)
	{
		enumValue = UNITS_ONE;
	}
	if(enumString.compare("UNITS_METER") == 0)
	{
		enumValue = UNITS_METER;
	}
	if(enumString.compare("UNITS_KILOGRAM") == 0)
	{
		enumValue = UNITS_KILOGRAM;
	}
	if(enumString.compare("UNITS_SECOND") == 0)
	{
		enumValue = UNITS_SECOND;
	}
	if(enumString.compare("UNITS_AMPERE") == 0)
	{
		enumValue = UNITS_AMPERE;
	}
	if(enumString.compare("UNITS_KELVIN") == 0)
	{
		enumValue = UNITS_KELVIN;
	}
	if(enumString.compare("UNITS_MOLE") == 0)
	{
		enumValue = UNITS_MOLE;
	}
	if(enumString.compare("UNITS_CANDELA") == 0)
	{
		enumValue = UNITS_CANDELA;
	}
	if(enumString.compare("UNITS_SQUARE_METER") == 0)
	{
		enumValue = UNITS_SQUARE_METER;
	}
	if(enumString.compare("UNITS_CUBIC_METER") == 0)
	{
		enumValue = UNITS_CUBIC_METER;
	}
	if(enumString.compare("UNITS_METERS_PER_SECOND") == 0)
	{
		enumValue = UNITS_METERS_PER_SECOND;
	}
	if(enumString.compare("UNITS_METERS_PER_SECOND_SQUARED") == 0)
	{
		enumValue = UNITS_METERS_PER_SECOND_SQUARED;
	}
	if(enumString.compare("UNITS_RECIPROCAL_METER") == 0)
	{
		enumValue = UNITS_RECIPROCAL_METER;
	}
	if(enumString.compare("UNITS_KILOGRAM_PER_CUBIC_METER") == 0)
	{
		enumValue = UNITS_KILOGRAM_PER_CUBIC_METER;
	}
	if(enumString.compare("UNITS_CUBIC_METER_PER_KILOGRAM") == 0)
	{
		enumValue = UNITS_CUBIC_METER_PER_KILOGRAM;
	}
	if(enumString.compare("UNITS_HERTZ") == 0)
	{
		enumValue = UNITS_HERTZ;
	}
	if(enumString.compare("UNITS_MINUTE") == 0)
	{
		enumValue = UNITS_MINUTE;
	}
	if(enumString.compare("UNITS_DEGREES") == 0)
	{
		enumValue = UNITS_DEGREES;
	}
	if(enumString.compare("UNITS_RADIANS") == 0)
	{
		enumValue = UNITS_RADIANS;
	}
	if(enumString.compare("UNITS_RADIANS_PER_SECOND") == 0)
	{
		enumValue = UNITS_RADIANS_PER_SECOND;
	}
	if(enumString.compare("UNITS_RADIANS_PER_SECOND_SQUARED") == 0)
	{
		enumValue = UNITS_RADIANS_PER_SECOND_SQUARED;
	}
	if(enumString.compare("UNITS_PERCENT") == 0)
	{
		enumValue = UNITS_PERCENT;
	}
	if(enumString.compare("UNITS_NEWTON") == 0)
	{
		enumValue = UNITS_NEWTON;
	}
	if(enumString.compare("UNITS_NEWTON_METER") == 0)
	{
		enumValue = UNITS_NEWTON_METER;
	}
	if(enumString.compare("UNITS_DEGREE_CELSIUS") == 0)
	{
		enumValue = UNITS_DEGREE_CELSIUS;
	}
	if(enumString.compare("UNITS_AMPERE_PER_SQUARE_METER") == 0)
	{
		enumValue = UNITS_AMPERE_PER_SQUARE_METER;
	}
	if(enumString.compare("UNITS_AMPERE_PER_METER") == 0)
	{
		enumValue = UNITS_AMPERE_PER_METER;
	}
	if(enumString.compare("UNITS_MOLE_PER_CUBIC_METER") == 0)
	{
		enumValue = UNITS_MOLE_PER_CUBIC_METER;
	}
	if(enumString.compare("UNITS_CANDELA_PER_SQUARE_METER") == 0)
	{
		enumValue = UNITS_CANDELA_PER_SQUARE_METER;
	}
	if(enumString.compare("UNITS_STERADIAN") == 0)
	{
		enumValue = UNITS_STERADIAN;
	}
	if(enumString.compare("UNITS_PASCAL") == 0)
	{
		enumValue = UNITS_PASCAL;
	}
	if(enumString.compare("UNITS_JOULE") == 0)
	{
		enumValue = UNITS_JOULE;
	}
	if(enumString.compare("UNITS_WATT") == 0)
	{
		enumValue = UNITS_WATT;
	}
	if(enumString.compare("UNITS_COULOMB") == 0)
	{
		enumValue = UNITS_COULOMB;
	}
	if(enumString.compare("UNITS_VOLT") == 0)
	{
		enumValue = UNITS_VOLT;
	}
	if(enumString.compare("UNITS_FARAD") == 0)
	{
		enumValue = UNITS_FARAD;
	}
	if(enumString.compare("UNITS_OHM") == 0)
	{
		enumValue = UNITS_OHM;
	}
	if(enumString.compare("UNITS_SIEMENS") == 0)
	{
		enumValue = UNITS_SIEMENS;
	}
	if(enumString.compare("UNITS_WEBER") == 0)
	{
		enumValue = UNITS_WEBER;
	}
	if(enumString.compare("UNITS_TESLA") == 0)
	{
		enumValue = UNITS_TESLA;
	}
	if(enumString.compare("UNITS_HENRY") == 0)
	{
		enumValue = UNITS_HENRY;
	}
	if(enumString.compare("UNITS_LUMEN") == 0)
	{
		enumValue = UNITS_LUMEN;
	}
	if(enumString.compare("UNITS_LUX") == 0)
	{
		enumValue = UNITS_LUX;
	}
	if(enumString.compare("UNITS_BECQUEREL") == 0)
	{
		enumValue = UNITS_BECQUEREL;
	}
	if(enumString.compare("UNITS_SIEVERT") == 0)
	{
		enumValue = UNITS_SIEVERT;
	}
	if(enumString.compare("UNITS_KATAL") == 0)
	{
		enumValue = UNITS_KATAL;
	}
	if(enumString.compare("UNITS_PASCAL_SECOND") == 0)
	{
		enumValue = UNITS_PASCAL_SECOND;
	}
	if(enumString.compare("UNITS_NEWTONS_PER_METER") == 0)
	{
		enumValue = UNITS_NEWTONS_PER_METER;
	}
	if(enumString.compare("UNITS_WATT_PER_SQUARE_METER") == 0)
	{
		enumValue = UNITS_WATT_PER_SQUARE_METER;
	}
	if(enumString.compare("UNITS_JOULE_PER_KELVIN") == 0)
	{
		enumValue = UNITS_JOULE_PER_KELVIN;
	}
	if(enumString.compare("UNITS_JOULE_PER_KILOGRAM") == 0)
	{
		enumValue = UNITS_JOULE_PER_KILOGRAM;
	}
	if(enumString.compare("UNITS_WATT_PER_METER_KELVIN") == 0)
	{
		enumValue = UNITS_WATT_PER_METER_KELVIN;
	}
	if(enumString.compare("UNITS_JOULE_PER_CUBIC_METER") == 0)
	{
		enumValue = UNITS_JOULE_PER_CUBIC_METER;
	}
	if(enumString.compare("UNITS_VOLT_PER_METER") == 0)
	{
		enumValue = UNITS_VOLT_PER_METER;
	}
	if(enumString.compare("UNITS_COULOMB_PER_CUBIC_METER") == 0)
	{
		enumValue = UNITS_COULOMB_PER_CUBIC_METER;
	}
	if(enumString.compare("UNITS_COULOMB_PER_SQUARE_METER") == 0)
	{
		enumValue = UNITS_COULOMB_PER_SQUARE_METER;
	}
	if(enumString.compare("UNITS_FARAD_PER_METER") == 0)
	{
		enumValue = UNITS_FARAD_PER_METER;
	}
	if(enumString.compare("UNITS_HENRY_PER_METER") == 0)
	{
		enumValue = UNITS_HENRY_PER_METER;
	}
	if(enumString.compare("UNITS_JOULE_PER_MOLE") == 0)
	{
		enumValue = UNITS_JOULE_PER_MOLE;
	}
	if(enumString.compare("UNITS_JOULE_PER_MOLE_KELVIN") == 0)
	{
		enumValue = UNITS_JOULE_PER_MOLE_KELVIN;
	}
	if(enumString.compare("UNITS_COULOMB_PER_KILOGRAM") == 0)
	{
		enumValue = UNITS_COULOMB_PER_KILOGRAM;
	}
	if(enumString.compare("UNITS_GRAY_PER_SECOND") == 0)
	{
		enumValue = UNITS_GRAY_PER_SECOND;
	}
	if(enumString.compare("UNITS_WATT_PER_SQUARE_METER_STERADIAN") == 0)
	{
		enumValue = UNITS_WATT_PER_SQUARE_METER_STERADIAN;
	}
	if(enumString.compare("UNITS_KATAL_PER_CUBIC_METER") == 0)
	{
		enumValue = UNITS_KATAL_PER_CUBIC_METER;
	}
	if(enumString.compare("UNITS_HOUR") == 0)
	{
		enumValue = UNITS_HOUR;
	}
	if(enumString.compare("UNITS_DAY") == 0)
	{
		enumValue = UNITS_DAY;
	}
	if(enumString.compare("UNITS_LITER") == 0)
	{
		enumValue = UNITS_LITER;
	}
	if(enumString.compare("UNITS_METRIC_TON") == 0)
	{
		enumValue = UNITS_METRIC_TON;
	}
	if(enumString.compare("UNITS_NEPER") == 0)
	{
		enumValue = UNITS_NEPER;
	}
	if(enumString.compare("UNITS_BEL") == 0)
	{
		enumValue = UNITS_BEL;
	}
	if(enumString.compare("UNITS_NAUTICAL_MILE") == 0)
	{
		enumValue = UNITS_NAUTICAL_MILE;
	}
	if(enumString.compare("UNITS_KNOT") == 0)
	{
		enumValue = UNITS_KNOT;
	}
	if(enumString.compare("UNITS_ARE") == 0)
	{
		enumValue = UNITS_ARE;
	}
	if(enumString.compare("UNITS_HECTARE") == 0)
	{
		enumValue = UNITS_HECTARE;
	}
	if(enumString.compare("UNITS_BAR") == 0)
	{
		enumValue = UNITS_BAR;
	}
	if(enumString.compare("UNITS_ANGSTRO") == 0)
	{
		enumValue = UNITS_ANGSTRO;
	}
	if(enumString.compare("UNITS_BARN") == 0)
	{
		enumValue = UNITS_BARN;
	}
	if(enumString.compare("UNITS_CURIE") == 0)
	{
		enumValue = UNITS_CURIE;
	}
	if(enumString.compare("UNITS_ROENTGEN") == 0)
	{
		enumValue = UNITS_ROENTGEN;
	}
	if(enumString.compare("UNITS_RAD") == 0)
	{
		enumValue = UNITS_RAD;
	}
	if(enumString.compare("UNITS_REM") == 0)
	{
		enumValue = UNITS_REM;
	}
	if(enumString.compare("UNITS_LITERS_PER_SECOND") == 0)
	{
		enumValue = UNITS_LITERS_PER_SECOND;
	}
	if(enumString.compare("UNITS_LITERS_PER_HOUR") == 0)
	{
		enumValue = UNITS_LITERS_PER_HOUR;
	}
	if(enumString.compare("UNITS_KILOGRAMS_PER_SECOND") == 0)
	{
		enumValue = UNITS_KILOGRAMS_PER_SECOND;
	}
	if(enumString.compare("UNITS_KILOGRAM_SQUARE_METER") == 0)
	{
		enumValue = UNITS_KILOGRAM_SQUARE_METER;
	}
	if(enumString.compare("UNITS_ROTATIONS_PER_MINUTE") == 0)
	{
		enumValue = UNITS_ROTATIONS_PER_MINUTE;
	}
	if(enumString.compare("UNITS_BITS_PER_SECOND") == 0)
	{
		enumValue = UNITS_BITS_PER_SECOND;
	}
	if(enumString.compare("UNITS_FRAMES_PER_SECOND") == 0)
	{
		enumValue = UNITS_FRAMES_PER_SECOND;
	}
	if(enumString.compare("UNITS_AMPERE_HOUR") == 0)
	{
		enumValue = UNITS_AMPERE_HOUR;
	}
	if(enumString.compare("UNITS_PARTS_PER_THOUSAND") == 0)
	{
		enumValue = UNITS_PARTS_PER_THOUSAND;
	}
	if(enumString.compare("UNITS_PARTS_PER_MILLION") == 0)
	{
		enumValue = UNITS_PARTS_PER_MILLION;
	}
    return input;
}

} // namespace fields
} // namespace model
} // namespace openjaus

#endif // UNITS_H

