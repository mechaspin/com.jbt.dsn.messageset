/**
\file TimeStamp.h

\par Copyright
Copyright (c) 2010-2016, OpenJAUS, LLC
All rights reserved.

This file is part of the OpenJAUS Software Development Kit (SDK). This
software is distributed under one of two licenses, the OpenJAUS SDK
Commercial End User License Agreement or the OpenJAUS SDK Non-Commercial
End User License Agreement. The appropriate licensing details were included
in with your developer credentials and software download. See the LICENSE
file included with this software for full details.
 
THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LCC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
- [2015-08] - Version 4.3 Release
- [2014-12] - Version 4.2 Release
- [2014-01] - Version 4.1 Release
- [2011-06] - Version 4.0 Release

*/
#ifndef FIELDS_TIMESTAMP_H
#define FIELDS_TIMESTAMP_H

#include "model/fields/Field.h"
#include "system/Transportable.h"
#include <string>
#include "types.h"
#include <ostream>

#include "system/Buffer.h"
// Start of user code for additional includes
// End of user code

namespace openjaus
{
namespace model
{
namespace fields
{
class TimeStamp;

/// \class TimeStamp TimeStamp.h

class OPENJAUS_EXPORT TimeStamp : public Field, public system::Transportable
{
public:
	TimeStamp(); 
	virtual ~TimeStamp();
	// Start of user code for additional constructors
	// End of user code
	/// Accessor to get the value of day.
	uint8_t getDay() const;

	/// Accessor to set value of day.
	/// \param day The value of the new day.
	bool setDay(uint8_t day);

	/// Accessor to get the value of hour.
	uint8_t getHour() const;

	/// Accessor to set value of hour.
	/// \param hour The value of the new hour.
	bool setHour(uint8_t hour);

	/// Accessor to get the value of minutes.
	uint8_t getMinutes() const;

	/// Accessor to set value of minutes.
	/// \param minutes The value of the new minutes.
	bool setMinutes(uint8_t minutes);

	/// Accessor to get the value of seconds.
	uint8_t getSeconds() const;

	/// Accessor to set value of seconds.
	/// \param seconds The value of the new seconds.
	bool setSeconds(uint8_t seconds);

	/// Accessor to get the value of milliseconds.
	uint16_t getMilliseconds() const;

	/// Accessor to set value of milliseconds.
	/// \param milliseconds The value of the new milliseconds.
	bool setMilliseconds(uint16_t milliseconds);


	/// \param level 
	 std::string toXml(uint8_t level) const;

	/// Operation setCurrentTime.
	 void setCurrentTime();


	/// \param format 
	 std::string toString(std::string format) const;


	 uint32_t toIntegerValue() const;

	/// Operation fromIntegerValue.
	/// \param intValue 
	 void fromIntegerValue(uint32_t intValue);

	/// Operation copy.
	/// \param source 
	 void copy(TimeStamp &source);


	/// \param hours 
	 TimeStamp addHours(int32_t hours) const;


	/// \param days 
	 TimeStamp addDays(int32_t days) const;


	/// \param minutes 
	 TimeStamp addMinutes(int32_t minutes) const;


	/// \param seconds 
	 TimeStamp addSeconds(int32_t seconds) const;


	/// \param milliseconds 
	 TimeStamp addMilliseconds(int32_t milliseconds) const;

	/// Operation setTime.
	/// \param days 
	/// \param hours 
	/// \param minutes 
	/// \param seconds 
	/// \param milliseconds 
	 bool setTime(int8_t days, int8_t hours, int8_t minutes, int8_t seconds, int16_t milliseconds);

	// Inherited pure virtuals from Transportable that need to be implemented
    virtual uint64 to(system::BufferWriter& dst) const;
    virtual uint64 from(system::BufferReader& src);
    virtual uint64 length() const;	

	virtual std::string toString() const;
	OPENJAUS_EXPORT friend std::ostream& operator<<(std::ostream& output, const TimeStamp& object);
	OPENJAUS_EXPORT friend std::ostream& operator<<(std::ostream& output, const TimeStamp* object);

protected:
	// Member attributes & references
	uint8_t day;
	uint8_t hour;
	uint8_t minutes;
	uint8_t seconds;
	uint16_t milliseconds;

// Start of user code for additional member data
    static const long MILLISECOND_START_BIT = 0;
    static const long MILLISECOND_BIT_MASK = 0x3FF;

    static const long SECOND_START_BIT = 10;
    static const long SECOND_BIT_MASK = 0x3F;

    static const long MINUTE_START_BIT = 16;
    static const long MINUTE_BIT_MASK = 0x3F;

    static const long HOUR_START_BIT = 22;
    static const long HOUR_BIT_MASK = 0x1F;

    static const long DAY_START_BIT = 27;
    static const long DAY_BIT_MASK = 0x1F;

    static const long MILLISECOND_MIN_VALUE = 0;
    static const long MILLISECOND_MAX_VALUE = 999;

    static const long SECOND_MIN_VALUE = 0;
    static const long SECOND_MAX_VALUE = 59;

    static const long MINUTE_MIN_VALUE = 0;
    static const long MINUTE_MAX_VALUE = 59;

    static const long HOUR_MIN_VALUE = 0;
    static const long HOUR_MAX_VALUE = 23;

    static const long DAY_MIN_VALUE = 1;
    static const long DAY_MAX_VALUE = 31;

public:
    inline static TimeStamp currentTimeStamp()
    {
    	TimeStamp current;
    	current.setCurrentTime();
    	return current;
    }

    // Utility functions
    struct tm convertToTm( ) const;

    TimeStamp till( TimeStamp &futureTime ) const;
    
    void setTime(const openjaus::system::Time& time);
// End of user code

private:
	

// Start of user code for additional private member data
// End of user code

}; // class TimeStamp

// Start of user code for inline functions
// End of user code



} // namespace fields
} // namespace model
} // namespace openjaus

#endif // FIELDS_TIMESTAMP_H


