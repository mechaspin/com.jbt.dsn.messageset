/**
\file Byte.h

\par Copyright
Copyright (c) 2010-2016, OpenJAUS, LLC
All rights reserved.

This file is part of the OpenJAUS Software Development Kit (SDK). This
software is distributed under one of two licenses, the OpenJAUS SDK
Commercial End User License Agreement or the OpenJAUS SDK Non-Commercial
End User License Agreement. The appropriate licensing details were included
in with your developer credentials and software download. See the LICENSE
file included with this software for full details.
 
THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LCC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
- [2015-08] - Version 4.3 Release
- [2014-12] - Version 4.2 Release
- [2014-01] - Version 4.1 Release
- [2011-06] - Version 4.0 Release

*/
#ifndef FIELDS_BYTE_H
#define FIELDS_BYTE_H

#include "model/fields/Signed.h"
#include "model/fields/Field.h"
#include "system/Transportable.h"
#include <string>
#include "types.h"
#include <ostream>

#include "system/Buffer.h"
// Start of user code for additional includes
// End of user code

namespace openjaus
{
namespace model
{
namespace fields
{

/// \class Byte Byte.h
/// \brief This is a brief description.
/// Detailed description.
/// \author Name (name@email.com)
class OPENJAUS_EXPORT Byte : public Signed, public Field, public system::Transportable
{
public:
	Byte(); 
	virtual ~Byte();
	// Start of user code for additional constructors
	// End of user code
	/// Accessor to get the value of value.
	int8_t getValue() const;

	/// Accessor to set value of value.
	/// \param value The value of the new value.
	bool setValue(int8_t value);


	// Inherited pure virtuals from Transportable that need to be implemented
	virtual uint64 to(system::BufferWriter& dst) const;
	virtual uint64 from(system::BufferReader& src);
	virtual uint64 length() const;

	virtual std::string toString() const;
	OPENJAUS_EXPORT friend std::ostream& operator<<(std::ostream& output, const Byte& object);
	OPENJAUS_EXPORT friend std::ostream& operator<<(std::ostream& output, const Byte* object);

protected:
	// Member attributes & references
	int8_t value;

// Start of user code for additional member data
public:
	std::string toXml(unsigned char level=0) const;
// End of user code

private:
	

// Start of user code for additional private member data
// End of user code

}; // class Byte

// Start of user code for inline functions
// End of user code



} // namespace fields
} // namespace model
} // namespace openjaus

#endif // FIELDS_BYTE_H


