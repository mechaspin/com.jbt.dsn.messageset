/**
\file DateStamp.h

\par Copyright
Copyright (c) 2010-2016, OpenJAUS, LLC
All rights reserved.

This file is part of the OpenJAUS Software Development Kit (SDK). This
software is distributed under one of two licenses, the OpenJAUS SDK
Commercial End User License Agreement or the OpenJAUS SDK Non-Commercial
End User License Agreement. The appropriate licensing details were included
in with your developer credentials and software download. See the LICENSE
file included with this software for full details.
 
THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LCC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
- [2015-08] - Version 4.3 Release
- [2014-12] - Version 4.2 Release
- [2014-01] - Version 4.1 Release
- [2011-06] - Version 4.0 Release

*/
#ifndef FIELDS_DATESTAMP_H
#define FIELDS_DATESTAMP_H

#include "model/fields/Field.h"
#include "system/Transportable.h"
#include <string>
#include "types.h"
#include <ostream>

#include "system/Buffer.h"
// Start of user code for additional includes
// End of user code

namespace openjaus
{
namespace model
{
namespace fields
{
class DateStamp;

/// \class DateStamp DateStamp.h

class OPENJAUS_EXPORT DateStamp : public Field, public system::Transportable
{
public:
	DateStamp(); 
	virtual ~DateStamp();
	// Start of user code for additional constructors
	// End of user code
	/// Accessor to get the value of day.
	uint8_t getDay() const;

	/// Accessor to set value of day.
	/// \param day The value of the new day.
	bool setDay(uint8_t day);

	/// Accessor to get the value of month.
	uint8_t getMonth() const;

	/// Accessor to set value of month.
	/// \param month The value of the new month.
	bool setMonth(uint8_t month);

	/// Accessor to get the value of year.
	uint16_t getYear() const;

	/// Accessor to set value of year.
	/// \param year The value of the new year.
	bool setYear(uint16_t year);


	/// \param level 
	 std::string toXml(uint8_t level) const;

	/// Operation setCurrentDate.
	 void setCurrentDate();


	/// \param format 
	 std::string toString(std::string format) const;


	 uint16_t toIntegerValue() const;

	/// Operation fromIntegerValue.
	/// \param intValue 
	 void fromIntegerValue(uint16_t intValue);

	/// Operation copy.
	/// \param source 
	 void copy(DateStamp &source);

	/// Operation setDate.
	/// \param year 
	/// \param month 
	/// \param day 
	 bool setDate(int16_t year, int8_t month, int8_t day);


	/// \param years 
	 DateStamp addYears(int32_t years) const;


	/// \param months 
	 DateStamp addMonths(int32_t months) const;


	/// \param days 
	 DateStamp addDays(int32_t days) const;

	// Inherited pure virtuals from Transportable that need to be implemented
	virtual uint64 to(system::BufferWriter& dst) const;
	virtual uint64 from(system::BufferReader& src);
	virtual uint64 length() const;

	virtual std::string toString() const;
	OPENJAUS_EXPORT friend std::ostream& operator<<(std::ostream& output, const DateStamp& object);
	OPENJAUS_EXPORT friend std::ostream& operator<<(std::ostream& output, const DateStamp* object);

protected:
	// Member attributes & references
	uint8_t day;
	uint8_t month;
	uint16_t year;

// Start of user code for additional member data
    static const long DAY_START_BIT = 0;
    static const long DAY_BIT_MASK = 0x1F;

    static const long MONTH_START_BIT = 5;
    static const long MONTH_BIT_MASK = 0xF;

    static const long YEAR_START_BIT = 9;
    static const long YEAR_BIT_MASK = 0x7F;

    static const long DAY_MIN_VALUE = 1;
    static const long DAY_MAX_VALUE = 31;

    static const long MONTH_MIN_VALUE = 1;
    static const long MONTH_MAX_VALUE = 12;

    static const long YEAR_MIN_VALUE = 2000;
    static const long YEAR_MAX_VALUE = 2127;

public:
    inline static DateStamp currentDateStamp()
    {
    	DateStamp current;
    	current.setCurrentDate();
    	return current;
    }

    // Utility functions
    struct tm convertToTm( ) const;

    void setDate(const openjaus::system::Time& time);
// End of user code

private:
	

// Start of user code for additional private member data
// End of user code

}; // class DateStamp

// Start of user code for inline functions
// End of user code



} // namespace fields
} // namespace model
} // namespace openjaus

#endif // FIELDS_DATESTAMP_H


