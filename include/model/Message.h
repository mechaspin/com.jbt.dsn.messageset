/**
\file Message.h

\par Copyright
Copyright (c) 2010-2016, OpenJAUS, LLC
All rights reserved.

This file is part of the OpenJAUS Software Development Kit (SDK). This
software is distributed under one of two licenses, the OpenJAUS SDK
Commercial End User License Agreement or the OpenJAUS SDK Non-Commercial
End User License Agreement. The appropriate licensing details were included
in with your developer credentials and software download. See the LICENSE
file included with this software for full details.
 
THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LCC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
- [2015-08] - Version 4.3 Release
- [2014-12] - Version 4.2 Release
- [2014-01] - Version 4.1 Release
- [2011-06] - Version 4.0 Release

*/
#ifndef MODEL_MESSAGE_H
#define MODEL_MESSAGE_H

#include "system/Buffer.h"
#include "system/InetAddress.h"
#include "model/Trigger.h"
#include <string>
#include "types.h"
#include <ostream>

namespace openjaus
{
namespace model
{

/// \class Message Message.h
/// \brief This is a brief description.
/// Detailed description.
/// \author Name (name@email.com)
class OPENJAUS_EXPORT Message : public Trigger
{
public:
	Message(); 
	virtual ~Message();
	Message(Message *message);

	/// Accessor to get the value of payload.
	system::Transportable* getPayload() const;

	/// Accessor to set value of payload.
	/// \param payload The value of the new payload.
	bool setPayload(system::Transportable* payload);

	/// Operation to.
	/// \param dst 
	virtual uint64 to(system::BufferWriter& dst) const;

	/// Operation from.
	/// \param src 
	virtual uint64 from(system::BufferReader& src);

	/// \brief Serializes object to internal transport buffer.
	virtual uint64 length() const;

	system::InetAddress getSource() const;
	void setSource(system::InetAddress ipAddress);

	system::InetAddress getDestination() const;
	void setDestination(system::InetAddress ipAddress);

	uint16 getSourcePort() const;
	void setSourcePort(uint16 port);

	uint16 getDestinationPort() const;
	void setDestinationPort(uint16 port);
	

	std::string toString() const;
	OPENJAUS_EXPORT friend std::ostream& operator<<(std::ostream& output, const Message& object);
	OPENJAUS_EXPORT friend std::ostream& operator<<(std::ostream& output, const Message* object);

protected:
	// Member attributes & references

private:
    system::Transportable *payload;
	system::InetAddress source;
	uint16 sourcePort;
	system::InetAddress destination;
	uint16 destinationPort;

}; // class Message

} // namespace model
} // namespace openjaus

#endif // MODEL_MESSAGE_H


