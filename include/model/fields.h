/**
\file fields.h

\par Copyright
Copyright (c) 2010-2016, OpenJAUS, LLC
All rights reserved.

This file is part of the OpenJAUS Software Development Kit (SDK). This
software is distributed under one of two licenses, the OpenJAUS SDK
Commercial End User License Agreement or the OpenJAUS SDK Non-Commercial
End User License Agreement. The appropriate licensing details were included
in with your developer credentials and software download. See the LICENSE
file included with this software for full details.
 
THIS SOFTWARE IS PROVIDED BY THE LICENSOR (OPENJAUS LCC) "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE LICENSOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THE SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE. THE LICENSOR DOES NOT WARRANT THAT THE LICENSED SOFTWARE WILL MEET
LICENSEE'S REQUIREMENTS OR THAT THE OPERATION OF THE LICENSED SOFTWARE
WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ERRORS IN THE LICENSED
SOFTWARE WILL BE CORRECTED.

\ Software History
- [2015-08] - Version 4.3 Release
- [2014-12] - Version 4.2 Release
- [2014-01] - Version 4.1 Release
- [2011-06] - Version 4.0 Release

*/

#ifndef FIELDS_H
#define FIELDS_H

// Start of user code for additional include files
#include "types.h"
// End of user code


// Type Definitions

// Enumerations
#include "model/fields/TypesUnsigned.h"
#include "model/fields/Units.h"
#include "model/fields/LimitType.h"
#include "model/fields/RoundingType.h"

// Classes
#include "model/fields/Field.h"
#include "model/fields/ComplexField.h"
#include "model/fields/FixedPoint.h"
#include "model/fields/VariablePoint.h"
#include "model/fields/Boolean.h"
#include "model/fields/Signed.h"
#include "model/fields/Unsigned.h"
#include "model/fields/Byte.h"
#include "model/fields/Short.h"
#include "model/fields/Integer.h"
#include "model/fields/Long.h"
#include "model/fields/UnsignedByte.h"
#include "model/fields/UnsignedShort.h"
#include "model/fields/UnsignedInteger.h"
#include "model/fields/UnsignedLong.h"
#include "model/fields/Float.h"
#include "model/fields/Double.h"
#include "model/fields/ScaledInteger.h"
#include "model/fields/Enumeration.h"
#include "model/fields/EnumerationItem.h"
#include "model/fields/EnumerationLiteral.h"
#include "model/fields/EnumerationRange.h"
#include "model/fields/Variant.h"
#include "model/fields/BitField.h"
#include "model/fields/BitFieldItem.h"
#include "model/fields/BitFieldRange.h"
#include "model/fields/BitFieldEnumeration.h"
#include "model/fields/BitFieldEnumerationValue.h"
#include "model/fields/FixedLengthString.h"
#include "model/fields/VariableLengthString.h"
#include "model/fields/Blob.h"
#include "model/fields/BlobType.h"
#include "model/fields/Array.h"
#include "model/fields/ArrayDimension.h"
#include "model/fields/ArrayType.h"
#include "model/fields/Record.h"
#include "model/fields/FieldReference.h"
#include "model/fields/VariablePointReference.h"
#include "model/fields/Unitized.h"
#include "model/fields/TimeStamp.h"
#include "model/fields/DateStamp.h"

// Start of user code for additional functions and headers
#include <time.h>

#define OJ_SECS_PER_MIN         60
#define OJ_SECS_PER_HOUR      3600
#define OJ_SECS_PER_DAY      86400
#define OJ_SECS_PER_YEAR  31536000


namespace openjaus
{
namespace model
{
namespace fields
{

inline struct tm toLocalTime(DateStamp date, TimeStamp time)
{
	struct tm returnResult = date.convertToTm();

	// Add date information
	struct tm timeResult = time.convertToTm();
	returnResult.tm_hour = timeResult.tm_hour;
	returnResult.tm_min  = timeResult.tm_min;
	returnResult.tm_sec  = timeResult.tm_sec;

	return returnResult;
}

inline float secondsSinceUnixEpoch(DateStamp date, TimeStamp time)
{
	float secondsResult;

	// Get struct tm for this Date/Time combo
	struct tm stamp = toLocalTime( date, time );

	// Normalize (updates the tm_yday field)
	mktime( &stamp );

	// Get POSIX-SPECIFIC seconds. If we were certain this was a POSIX system, we could just
	//  use the answer from gmtime, but it's impossible to be certain, and not being certain
	//  introduces issues of differing system epochs and the inclusion of leap seconds.
	secondsResult = ( stamp.tm_sec +                                      // seconds
					  (stamp.tm_min  * OJ_SECS_PER_MIN ) +                // minutes
					  (stamp.tm_hour * OJ_SECS_PER_HOUR) +                // hours
					  (stamp.tm_yday * OJ_SECS_PER_DAY ) +                // days
					  ((stamp.tm_year - 70)        * OJ_SECS_PER_YEAR) +  // years
					  (((stamp.tm_year - 69)/4)    * OJ_SECS_PER_DAY) -   // leap-days
					  (((stamp.tm_year - 1)/100)   * OJ_SECS_PER_DAY) +   // except divis by 100
					  (((stamp.tm_year + 299)/400) * OJ_SECS_PER_DAY) );  // except divis by 400

	return secondsResult;
}

} // namespace fields
} // namespace model
} // namespace openjaus
// End of user code



#endif // FIELDS_H
