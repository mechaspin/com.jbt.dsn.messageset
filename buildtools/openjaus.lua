local ojUtil = require("buildtools.openjaus-util")
local ojInternal = require("buildtools.openjaus-internal")

local M = {}

local function removeLibJsonDependency(incoming)
  local updated = {}
  updated.Dependencies = {}
  updated.Directories = {}
  updated.Messages = incoming.Messages
 
  local removeLibjsonDirectory = false

  for i, dep in ipairs(incoming.Dependencies) do
    if (dep == "json") then
      local message = "Removing dependency on libjson. JSON support is now included in the openjaus library"
      table.insert(updated.Messages, message)
      removeLibjsonDirectory = true
    else
      table.insert(updated.Dependencies, dep)
    end
  end

  if (removeLibjsonDirectory) then
    for i, dir in ipairs(incoming.Directories) do
      if (not (dir == "../libjson")) then
        table.insert(updated.Directories, dir)
      end
    end
  else
    updated.Directories = incoming.Directories
  end
  
  return updated
end

local function updateDependencyNames(incoming)
  local updated = {}
  updated.Dependencies = {}
  updated.Directories = incoming.Directories
  updated.Messages = incoming.Messages
  
  local updatedDep
  
  for i, dep in ipairs(incoming.Dependencies) do
    if (dep == "openjaus-core") then
      updatedDep = dep .. "_v1_1"
    elseif (dep == "openjaus-environment") then
      updatedDep = dep .. "_v1_0"
    elseif (dep == "openjaus-mobility") then
      updatedDep = dep .. "_v1_0"
    elseif (dep == "openjaus-manipulator") then
      updatedDep = dep .. "_v1_0"
    elseif (dep == "openjaus-ugv") then
      updatedDep = dep .. "_v1_0"
    else
      updatedDep = dep
    end

    if (not (dep == updatedDep)) then
      local message = "Updating " .. dep .. " to reference " .. updatedDep
      table.insert(updated.Messages, message)
    end
    
    table.insert(updated.Dependencies, updatedDep) 
  end
  
  return updated
end

local function updateDependencies(nameOJ, depsOJ, dirsOJ)
  local updated = {}
  updated.Dependencies = depsOJ
  updated.Directories = dirsOJ
  updated.Messages = {}

  updated = removeLibJsonDependency(updated)
  updated = updateDependencyNames(updated)
  
  if (not (table.getn(updated.Messages) == 0)) then
    print("--- " .. nameOJ .. " ---")
    for i, message in ipairs(updated.Messages) do
      print("  INFO: " .. message)
    end
    print("")
  end
  
  return updated
end

local function createProject(typeOJ, nameOJ, filesOJ, depsOJ, dirsOJ)
  local proj = {}

  proj.Type = typeOJ
  proj.Name = nameOJ
  proj.Files = filesOJ
  
  local resolvedDependencies = updateDependencies(nameOJ, depsOJ, dirsOJ)
  proj.Dependencies = resolvedDependencies.Dependencies
  proj.Directories = resolvedDependencies.Directories
  
  proj.AddPostBuildCommands = (_OPTIONS["parallelbuild"] == nil)

  return proj
end

local function createConsoleApp(nameOJ, filesOJ, depsOJ, dirsOJ)
  return createProject(ojInternal.ProjectTypes.CONSOLEAPP, nameOJ, filesOJ, depsOJ, dirsOJ)
end

local function createLibrary(nameOJ, filesOJ, depsOJ, dirsOJ)
  return createProject(ojInternal.ProjectTypes.SHAREDLIB, nameOJ, filesOJ, depsOJ, dirsOJ)
end

local function addProjects(projectsOJ)
  for i, project in pairs(projectsOJ) do
    ojInternal.addProject(project)
  end
end

local function includeDependencies(projectsOJ)
  for i, project in pairs(projectsOJ) do
    ojInternal.includeDependencies(project)
  end
end

local function resolvename(originalName)
  local name = _OPTIONS["name"]
  if (name == nil) then
    return originalName
  else
    return name
  end
end

local function createSolution(nameOJ, projectsOJ)
  if (not _OPTIONS["solution"]) then
    _OPTIONS["solution"] = "standalone"
  end

  if ((_OPTIONS["solution"] == "full") and (not _OPTIONS["name"])) then
    _OPTIONS["projects"] = "libs"
    _OPTIONS["name"] = nameOJ
  end

  local name = resolvename(nameOJ)
  ojInternal.configureSolution(name)
  addProjects(projectsOJ)
  if (_OPTIONS["solution"] == "full") then
    includeDependencies(projectsOJ)
  end
  
  if (not(_OPTIONS["parallelbuild"] == nil)) then
    if (not (_ACTION == nil) and not (string.match(_ACTION, "vs..") == nil)) then
      print("INFO: Parallel build support requested; adding _PostBuild project.")
      ojInternal.addMSVSPostBuildProject(projectsOJ)
    end
  end
  
end

M.createSolution = createSolution
M.createLibrary = createLibrary
M.createConsoleApp = createConsoleApp

newoption {
  trigger = "name",
  value = "VALUE",
  description = "Specify the solution name",
}

newoption {
  trigger = "solution",
  value= "",
  description = "(For internal OpenJAUS use only) Specify the type of solution to be generated.",
  allowed = {
    { "standalone", "Generated solution will only contain the top-level projects. [Default]" },
    { "full", "Generated solution will contain the top-level projects and the projects for all the dependencies." }
  }
}

newoption {
  trigger = "parallelbuild",
  description = "(For internal OpenJAUS use only) Specify that the parallel build support should be enabled."
}

return M