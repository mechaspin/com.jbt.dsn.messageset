local ojUtil = require("buildtools.openjaus-util")

_PROJECT_DIRECTORIES = {}
local M = {}

local ProjectTypes = {
  SHAREDLIB = "sharedlibrary",
  CONSOLEAPP = "consoleapp"
}

local function addDefines(projectDefines, dependencies)
  defines { projectDefines }
  
  for i, dep in ipairs(dependencies) do
    if (dep == "openjaus") then
      defines { "JSON_DLL" }
    end
  end
end

local function addIncludeDirectories(directories)
  for i, inc in ipairs(directories) do
    includedirs { inc .. "/include"}
  end
end

local function addLibraryDirectories(directories, subfolderName)
  for i, lib in ipairs(directories) do
    libdirs { lib .. "/" .. subfolderName}
  end
end

local function addPostbuildCommands(directories, osSeparator, directoryPrefix)
  for i, dir in ipairs(directories) do
    local cmd = "copy " .. directoryPrefix .. "/" .. dir .. "/lib/*.dll " .. directoryPrefix .. "/bin"
    local newcmd = string.gsub(cmd, "/", osSeparator)
    postbuildcommands { newcmd }
  end
end

local function addConsoleApplication(ojProject)
  project(ojProject.Name)
    kind "ConsoleApp"
    if (not (_ACTION == nil)) then
      location(".build/" .. _ACTION)
      objdir (".build/" .. _ACTION)
    end
    targetdir "bin"
    files { ojProject.Files }

    addIncludeDirectories(ojProject.Directories)
    addDefines(ojProject.Defines, ojProject.Dependencies)

    configuration {"gmake", "Arm"}
      links { ojProject.Dependencies }
      addLibraryDirectories(ojProject.Directories, "lib/arm")
      targetdir "bin/arm"
      linkoptions { "-static -static-libgcc -static-libstdc++" }

    configuration { "windows", "gmake", "Not Arm" }
      links { ojProject.Dependencies }
      addLibraryDirectories(ojProject.Directories, "lib")
      buildoptions { "-fPIC" }
      targetextension ""

    configuration {"gmake", "Not Arm"}
      links { ojProject.Dependencies }
      addLibraryDirectories(ojProject.Directories, "lib")

    configuration { "vs2008 or vs2010" }
      if (ojProject.AddPostBuildCommands) then
        addPostbuildCommands(ojProject.Directories, "\\", "../../")
      end
      addLibraryDirectories(ojProject.Directories, "lib")

    configuration { "vs2008 or vs2010", "Release" }
      links { ojProject.Dependencies }

    configuration { "vs2008 or vs2010", "Debug" }
      for k, v in ipairs(ojProject.Dependencies) do
        links{ v .."d"}
      end

    -- rt and pthread need to be at the end of the dependency list
    configuration { "gmake" }
      links { "rt", "pthread" }
end

local function addSharedLibrary(ojProject)
  project(ojProject.Name)
    kind "SharedLib"
    if (not (_ACTION == nil)) then
      location(".build/" .. _ACTION)
      objdir (".build/" .. _ACTION)
    end
    targetdir "lib"
    files { ojProject.Files }

    addIncludeDirectories(ojProject.Directories)
    addDefines(ojProject.Defines, ojProject.Dependencies)

    configuration { "windows", "gmake", "Not Arm" }
      targetextension ".so"
      buildoptions { "-fPIC" }

    configuration {"gmake", "Not Arm"}
      links { ojProject.Dependencies }
      addLibraryDirectories(ojProject.Directories, "lib")

    configuration {"gmake", "Arm"}
      links { ojProject.Dependencies }
      addLibraryDirectories(ojProject.Directories, "lib/arm")
      targetdir "lib/arm"
      kind "StaticLib"

    configuration { "vs2008 or vs2010" }
      addLibraryDirectories(ojProject.Directories, "lib")

    configuration { "vs2008 or vs2010", "Release" }
      links { ojProject.Dependencies }

    configuration { "vs2008 or vs2010", "Debug" }
      targetsuffix "d"
      for k, v in ipairs(ojProject.Dependencies) do
        links{ v .."d"}
      end

    -- rt and pthread need to be at the end of the dependency list
    configuration { "gmake" }
      links { "rt", "pthread" }
end

local function addMSVSPostBuildProject(projectsOJ)
  project("_PostBuild")
    kind "ConsoleApp"
    if (not (_ACTION == nil)) then
      location(".build/" .. _ACTION)
      objdir (".build/" .. _ACTION)
    end
    targetdir "bin"
    files { }

  configuration { "vs2008 or vs2010" }
    for i, proj in pairs(projectsOJ) do
      addPostbuildCommands(proj.Directories, "\\", "../../")
    end
end

local function addProject(ojProject)
--  print("adding project " .. ojProject.Name)

  if (ojProject.Type == ProjectTypes.SHAREDLIB) then
    addSharedLibrary(ojProject)
  elseif (ojProject.Type == ProjectTypes.CONSOLEAPP) then
    addConsoleApplication(ojProject)
  else
    print(ojProject.Name .. " is of unknown Project type: " .. ojProject.Type )
  end
end

local function includeDependencies(ojProject)
  for i, directory in pairs(ojProject.Directories) do
      if (not (ojUtil.table_contains(_PROJECT_DIRECTORIES, directory))) then
        if (not (directory == "./")) then
          table.insert(_PROJECT_DIRECTORIES, directory)
--          print("including directory " .. directory)
          include(directory)
        end
      end
  end
end

local function configureSolution(solutionName)
  solution(solutionName)
    configurations { "Debug", "Release" }
    platforms { "Native", "Arm"}
    language "C++"

    os.mkdir("bin")

    configuration { "windows", "gmake" }
      flags { "NoImportLib" }

    configuration {"gmake", "Arm"}

      if (not _OPTIONS["prefix"]) then
        ojPrefix = "arm-none-linux-gnueabi-"
      else
        ojPrefix = _OPTIONS["prefix"]
      end

      premake.gcc.platforms.Arm.cc = ojPrefix .. "gcc"
      premake.gcc.platforms.Arm.cxx = ojPrefix .. "g++"
      premake.gcc.platforms.Arm.ar= ojPrefix .. "ar"

    configuration "Debug"
      defines { "DEBUG" }
      flags { "Symbols", "ExtraWarnings" }

    configuration "Release"
      defines { "NDEBUG" }
      flags { "Optimize" }

    configuration "vs2008 or vs2010"
      local ver = os.getversion()
      if(os.getversion().majorversion < 6) then
        defines { "_WIN32_WINNT=0x0501", "WIN32", "_CRT_SECURE_NO_WARNINGS" }
      else
        defines { "_WIN32_WINNT=0x0600", "WIN32", "_CRT_SECURE_NO_WARNINGS", "_ALLOW_RTCc_IN_STL" }
      end
      flags { "NoMinimalRebuild" }
      -- 2015/08 DK: Added "/wd4589" to this to address the bug discussed here: https://connect.microsoft.com/VisualStudio/feedback/details/1570496/vs-2015-generates-a-copy-constructor-and-then-complains-about-it
      buildoptions { "/wd4100", "/wd4800", "/wd4244", "/wd4251", "/wd4127", "/wd4250", "/wd4482", "/wd4458", "/wd4589", "/wd4512", "/MP" }
end

newoption {
  trigger = "prefix",
  value= "VALUE",
  description = "Specify the prefix for your cross-compiling tools. If not specified a default prefix of 'arm-none-linux-gnueabi-' will be used."
}

M.addIncludeDirectories = addIncludeDirectories
M.addLibraryDirectories = addLibraryDirectories
M.addPostbuildCommands = addPostbuildCommands
M.configureSolution = configureSolution
M.addProject = addProject
M.includeDependencies = includeDependencies
M.addMSVSPostBuildProject = addMSVSPostBuildProject

M.ProjectTypes = ProjectTypes


return M